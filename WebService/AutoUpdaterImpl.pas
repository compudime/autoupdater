{ Invokable implementation File for TAutoUpdater which implements IAutoUpdater }

unit AutoUpdaterImpl;

interface

uses Soap.InvokeRegistry, System.Types, Soap.XSBuiltIns, AutoUpdaterIntf,
  System.SysUtils, System.Classes, FireDAC.Phys.ADSDef, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.ADS, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, IOUtils,
  FireDAC.Comp.DataSet, FireDAC.VCLUI.Login, FireDAC.Comp.UI,
  FireDAC.Phys.ADSWrapper, System.JSON, WinApi.Windows, IniFiles, uUpdateChecker;

type
  { TAutoUpdater }
  TAutoUpdater = class(TInvokableClass, IAutoUpdater)
  private
    procedure Log (s: string);
    procedure CreateLinkConn (var ADTLink: TFDPhysADSDriverLink; var Conn: TFDConnection);
    function NewQuery (ConnectionName: string): TFDQuery;
    function NewTable (ConnectionName: string): TFDTable;
    procedure ExtractVersionSegments (s: string; var Major, Miner, Release, Build: integer);
    function LogOperationDone (const JSON, Operation: UnicodeString): UnicodeString; //overload;
    function GetComponentVersion (s: UnicodeString): string;
    function GetUpgradeVersion_1_0_1 (const Info: UnicodeString): UnicodeString; stdcall;
    function LogDownloadDone_1_0_1 (const Info: UnicodeString): UnicodeString; stdcall;
    function LogUpdateDone_1_0_1 (const Info: UnicodeString): UnicodeString; stdcall;
  public
    function echoStr(const Value: UnicodeString): UnicodeString; stdcall;
    function GetUpdateInfo (const Info: UnicodeString): UnicodeString; stdcall;
    function ConfirmDownload (const Info: UnicodeString): UnicodeString; stdcall;
    function ConfirmUpdate (const Info: UnicodeString): UnicodeString; stdcall;
  end;

implementation
const
  LogDirectory  = 'C:\AutoUpdater\Log';
  ConfigIniFile = 'C:\AutoUpdater\WSConfig\WSAutoUpdater.ini';

function Nvl (b1, b2: TBookmark): TBookmark;
begin
  if Assigned (b1) then
    result := b1
  else
    result := b2;
end;

procedure TAutoUpdater.ExtractVersionSegments (s: string; var Major, Miner, Release, Build: integer);
var
  A: TArray <string>;
begin
  Major   := 0;
  Miner   := 0;
  Release := 0;
  Build   := 0;
  A       := s.Split(['.']);

  if Length (A) >= 1 then TryStrToInt(A [0], Major);
  if Length (A) >= 2 then TryStrToInt(A [1], Miner);
  if Length (A) >= 3 then TryStrToInt(A [2], Release);
  if Length (A) >= 4 then TryStrToInt(A [3], Build);
end;

function TAutoUpdater.echoStr(const Value: UnicodeString): UnicodeString; stdcall;
begin
  result := Format ('Echo: [%s]', [Value]);
end;

procedure TAutoUpdater.Log (s: string);
var
  FileName: string;
  T: TextFile;
begin
  FileName := LogDirectory + '\WSAutoUpdate_' + FormatDateTime('yyyy"_"mm"_"dd', Now) + '.log';
  if FileExists(FileName) then
  begin
    AssignFile (T, FileName);
    Append (T);
  end
  else
  begin
    AssignFile (T, FileName);
    Rewrite (T);
  end;
  WriteLn (T, FormatDateTime ('yyyy/mm/dd hh:nn:ss', Now) + ' ' + s);
  CloseFile (T);
end;

function ComputerName:String;
var
  ComputerName: Array [0 .. 256] of char;
  Size: DWORD;
begin
  Size := 256;
  GetComputerName(ComputerName, Size);
  Result := ComputerName;
end;

procedure TAutoUpdater.CreateLinkConn (var ADTLink: TFDPhysADSDriverLink; var Conn: TFDConnection);
var
  IniFile: TIniFile;
  Driver, Alias, TableType, ServerTypes: string;
begin
  IniFile := TIniFile.Create(ConfigIniFile);
  try
    ADTLink             := TFDPhysADSDriverLink.Create(nil);
    ADTLink.VendorLib   := 'C:\AutoUpdater\Redistribute\ace32.dll';
    Conn                := TFDConnection.Create(nil);
    Conn.ConnectionName := 'AutoUpdater';
    Conn.DriverName     := 'ADS';
    Conn.LoginPrompt    := False;
    Driver              := IniFile.ReadString('Config', 'DriverID',    'ADS');
    Alias               := IniFile.ReadString('Config', 'Alias',       'AutoUpdater');
    TableType           := IniFile.ReadString('Config', 'TableType',   'ADT');
    ServerTypes         := IniFile.ReadString('Config', 'ServerTypes', 'Remote');

    Conn.Params.Clear;
    Conn.Params.Add('DriverID='    + Driver);
    Conn.Params.Add('Alias='       + Alias);
    Conn.Params.Add('TableType='   + TableType);
    Conn.Params.Add('ServerTypes=' + ServerTypes);

    if      ServerTypes.ToUpper = 'REMOTE' then
      TFDPhysADSConnectionDefParams(Conn.Params).ServerTypes := stRemote
    else if ServerTypes.ToUpper = 'LOCAL' then
      TFDPhysADSConnectionDefParams(Conn.Params).ServerTypes := stLocal
    else if ServerTypes.ToUpper = 'INTERNET' then
      TFDPhysADSConnectionDefParams(Conn.Params).ServerTypes := stInternet;

    try
      Conn.Params.UserName := 'AdsSys';
      Conn.Params.Password := 'lezaM12345$$';
      Conn.Connected       := True;
    except
      on E: Exception do
        Log ('TAutoUpdater.CreateLinkConn. Error: ' + E.Message);
    end;
  finally
    IniFile.Free;
  end;
end;

function TAutoUpdater.NewQuery (ConnectionName: string): TFDQuery;
begin
  result                   := TFDQuery.Create(nil);
  result.ConnectionName    := ConnectionName;
  result.FetchOptions.Mode := fmAll;
end;

function TAutoUpdater.NewTable (ConnectionName: string): TFDTable;
begin
  result                := TFDTable.Create(nil);
  result.ConnectionName := ConnectionName;
end;

function TAutoUpdater.LogOperationDone (const JSON, Operation: UnicodeString): UnicodeString;
var
  Link:  TFDPhysADSDriverLink;
  Conn:  TFDConnection;
  Query: TFDQuery;
  Value: TJSONValue;
  CurrMajor, CurrMiner, CurrRelease, CurrBuild: integer;
  DBMajor,   DBMiner,   DBRelease,   DBBuild:   integer;

  inApplication, inVersion, inDBVersion, inComputerID, inEnterprise,
  inStore, inMachineNumber, inExternalIP, inLocalIP, inComponentVer: string;
begin
  result := 'OK';
  Link   := nil;
  Conn   := nil;
  Query  := nil;
  try
    Value := TJSONObject.ParseJSONValue(JSON);
    try
      inApplication   := UpperCase (Value.GetValue<string>('Application'));
      inVersion       := Value.GetValue<string>('Version');
      inDBVersion     := Value.GetValue<string>('DBVersion');
      inEnterprise    := Value.GetValue<string>('Enterprise');
      inStore         := Value.GetValue<string>('Store');
      inMachineNumber := Value.GetValue<string>('MachineNumber');
      inComputerID    := Value.GetValue<string>('ComputerID');
      inExternalIP    := Value.GetValue<string>('ExternalIP');
      inLocalIP       := Value.GetValue<string>('LocalIP');
      inComponentVer  := Value.GetValue<string>('ComponentVer');
    finally
      Value.Free;
    end;
  except
    on E: Exception do
    begin
      Log ('LogOperationDone. Error parsing JSON');
      Log (E.Message);
      result := E.Message;
      exit;
    end;
  end;

  ExtractVersionSegments(inVersion,   CurrMajor, CurrMiner, CurrRelease, CurrBuild);
  ExtractVersionSegments(inDBVersion, DBMajor,   DBMiner,   DBRelease,   DBBuild);
  try
    try
      CreateLinkConn(Link, Conn);
      Query := NewQuery (Conn.ConnectionName);
    except
      on E: Exception do
      begin
        Log (Format ('LogOperationDone [%s], Error creating link and connection', [Operation]));
        Log (E.Message);
        result := E.Message;
        exit;
      end;
    end;

    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('Merge Status_Machine_Updates On (    Upper (Application) = Upper (:App)');
    Query.SQL.Add('                                 And Upper (Computer_ID) = Upper (:Computer_ID))');
    Query.SQL.Add('When Matched Then');
    Query.SQL.Add('  Update Set Last_Op           = :Last_Op,');
    Query.SQL.Add('             Date_op           = Now(),');
    Query.SQL.Add('             Current_Ver_Maj   = :Major,');
    Query.SQL.Add('             Current_Ver_Min   = :Miner,');
    Query.SQL.Add('             Current_Ver_Rel   = :Release,');
    Query.SQL.Add('             Enterprise        = :Enterprise,');
    Query.SQL.Add('             Store             = :Store,');
    Query.SQL.Add('             Machine_Number    = :Machine_Number,');
    Query.SQL.Add('             External_IP       = :External_IP,');
    Query.SQL.Add('             Local_IP          = :Local_IP,');
    Query.SQL.Add('             Component_Version = :CompVer');
    Query.SQL.Add('When Not Matched Then');
    Query.SQL.Add('  Insert (Application, Current_Ver_Maj, Current_Ver_Min, Current_Ver_Rel, Enterprise, Store, Machine_Number, Last_Op, Date_Op, Computer_ID, External_IP, Local_IP, Component_Version)');
    Query.SQL.Add('    Values (Upper (:App), :Major, :Miner, :Release, Upper (:Enterprise), Upper (:Store), Upper (:Machine_Number), :Last_Op, Now(), :Computer_ID, :External_IP, :Local_IP, :CompVer)');
    Query.ParamByName('App').AsString            := inApplication;
    Query.ParamByName('Major').AsInteger         := CurrMajor;
    Query.ParamByName('Miner').AsInteger         := CurrMiner;
    Query.ParamByName('Release').AsInteger       := CurrRelease;
    Query.ParamByName('Enterprise').AsString     := inEnterprise;
    Query.ParamByName('Store').AsString          := inStore;
    Query.ParamByName('Machine_Number').AsString := inMachineNumber;
    Query.ParamByName('Last_Op').AsString        := Operation;
    Query.ParamByName('Computer_ID').AsString    := inComputerID;
    Query.ParamByName('External_IP').AsString    := inExternalIP;
    Query.ParamByName('Local_IP').AsString       := inLocalIP;
    Query.ParamByName('CompVer').AsString        := inComponentVer;
    try
      Query.ExecSQL;

      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add('Select Install_Ver');
      Query.SQL.Add('  From Status_Machine_Updates');
      Query.SQL.Add(' Where Upper (Application) = Upper (:App)');
      Query.SQL.Add('   And Upper (Computer_ID) = Upper (:Computer_ID)');
      Query.ParamByName('App').AsString         := inApplication;
      Query.ParamByName('Computer_ID').AsString := inComputerID;
      Query.Open;

      if CompareVersion (Query.FieldByName ('Install_Ver').AsString, Format ('%d.%d.%d', [CurrMajor, CurrMiner, CurrRelease])) = coEqual then
      begin
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('Update Status_Machine_Updates Set Install_Ver = Null');
        Query.SQL.Add(' Where Upper (Application) = Upper (:App)');
        Query.SQL.Add('   And Upper (Computer_ID) = Upper (:Computer_ID)');
        Query.ParamByName('App').AsString         := inApplication;
        Query.ParamByName('Computer_ID').AsString := inComputerID;
        Query.ExecSQL;
      end;

      try
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('Insert Into Log_Machine_Updates (Enterprise, Store, Machine_Number, Application, Current_Ver_Maj, Current_Ver_Min, Current_Ver_Rel,');
        Query.SQL.Add('                                 Running_DB_Ver_Maj, Running_DB_Ver_Min, Running_DB_Ver_Rel, Desc_Op, Date_Op, Computer_ID, External_IP, Local_IP, Component_Version)');
        Query.SQL.Add('  Values (Upper (:Enterprise), Upper (:Store), Upper (:Machine), Upper (:App), :Curr_Maj, :Curr_Min, :Curr_Rel, :DB_Maj, :DB_Min, :DB_Rel,');
        Query.SQL.Add('          :Last_Op, Now(), :Computer_ID, :External_IP, :Local_IP, :CompVer);');
        Query.ParamByName('App').AsString         := inApplication;
        Query.ParamByName('Enterprise').AsString  := inEnterprise;
        Query.ParamByName('Store').AsString       := inStore;
        Query.ParamByName('MACHINE').AsString     := inMachineNumber;
        Query.ParamByName('DB_MAJ').AsInteger     := DBMajor;
        Query.ParamByName('DB_MIN').AsInteger     := DBMiner;
        Query.ParamByName('DB_REL').AsInteger     := DBRelease;
        Query.ParamByName('CURR_MAJ').AsInteger   := CurrMajor;
        Query.ParamByName('CURR_MIN').AsInteger   := CurrMiner;
        Query.ParamByName('CURR_REL').AsInteger   := CurrRelease;
        Query.ParamByName('Last_Op').AsString     := Operation;
        Query.ParamByName('Computer_ID').AsString := inComputerID;
        Query.ParamByName('External_IP').AsString := inExternalIP;
        Query.ParamByName('Local_IP').AsString    := inLocalIP;
        Query.ParamByName('CompVer').AsString     := inComponentVer;
        Query.ExecSQL;
      except
        on E: Exception do
        begin
          Log (Format ('LogOperationDone [%s], Inserting Log_Machine_Updates', [Operation]));
          Log (E.Message);
        end;
      end;
    except
      on E: Exception do
      begin
        result := E.Message;
        Log (Format ('LogOperationDone [%s], Updating Status_Machine_Updates', [Operation]));
        Log (E.Message);
      end;
    end;

  finally
    if Assigned (Query) then
    begin
      Query.Close;
      Query.Free;
    end;

    if Assigned (Conn) then
    begin
      Conn.Close;
      Conn.Free;
    end;

    if Assigned (Link) then
      Link.Free;
  end;
end;

function TAutoUpdater.GetComponentVersion (s: UnicodeString): string;
var
  JSON: TJSONValue;
begin
  result := '';
  try
    JSON   := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(s), 0);
    result := JSON.GetValue<string>('ComponentVer');
  except
    on E: Exception do
      result := 'ERROR: ' + E.Message;
  end;
end;

function TAutoUpdater.GetUpgradeVersion_1_0_1 (const Info: UnicodeString): UnicodeString;
var
  inApplication, inEnterprise, inStore, inMachineNumber,
  inComputerID, inExternalIP, inLocalIP, inComponentVersion: string;
  inCurrMajor, inCurrMiner, inCurrRelease, inDBMajor, inDBMiner, inDBRelease: integer;

  outErrorMsg, outApplication, outUpdatePriority, outIdleTime, outVersion, outURL,
  outReleaseNotes, outTargetFileName, outPathToInstall, outInstallerType: string;

var
  Link:           TFDPhysADSDriverLink;
  Conn:           TFDConnection;
  Query:          TFDQuery;
  bm,
  bmMaj,
  bmMin,
  bmRel:          TBookmark;
  UpdateType:     string;
  Duplicated:     boolean;
  Obj:            TJSONObject;
  JSONValue:      TJSONValue;
  InstalledVer,
  NewSpecificVer,
  NewGlobalVer,
  SelVersion, s:  string;
  Found:          boolean;
begin
  result             := '';
  JSONValue          := TJSONObject.Create;
  JSONValue          := TJSONObject.ParseJSONValue(Info);
  Link               := nil;
  Conn               := nil;
  Query              := nil;
  bmMaj              := nil;
  bmMin              := nil;
  bmRel              := nil;
  inApplication      := '';
  inEnterprise       := '';
  inStore            := '';
  inMachineNumber    := '';
  inComputerID       := '';
  inExternalIP       := '';
  inLocalIP          := '';
  inComponentVersion := '';
  inCurrMajor        := 0;
  inCurrMiner        := 0;
  inCurrRelease      := 0;
  inDBMajor          := 0;
  inDBMiner          := 0;
  inDBRelease        := 0;
  outErrorMsg        := '';
  outApplication     := '';
  outUpdatePriority  := '';
  outIdleTime        := '';
  outVersion         := '';
  outURL             := '';
  outReleaseNotes    := '';
  outTargetFileName  := '';
  outPathToInstall   := '';
  outInstallerType   := '';
  try
    try
      inApplication      := JSONValue.GetValue<string>('Application');
      inComputerID       := JSONValue.GetValue<string>('ComputerID');
      inEnterprise       := JSONValue.GetValue<string>('Enterprise');
      inStore            := JSONValue.GetValue<string>('Store');
      inMachineNumber    := JSONValue.GetValue<string>('MachineNumber');
      inExternalIP       := JSONValue.GetValue<string>('ExternalIP');
      inLocalIP          := JSONValue.GetValue<string>('LocalIP');
      inCurrMajor        := JSONValue.GetValue<integer>('CurrMajor');
      inCurrMiner        := JSONValue.GetValue<integer>('CurrMiner');
      inCurrRelease      := JSONValue.GetValue<integer>('CurrRelease');
      inDBMajor          := JSONValue.GetValue<integer>('DBMajor');
      inDBMiner          := JSONValue.GetValue<integer>('DBMiner');
      inDBRelease        := JSONValue.GetValue<integer>('DBRelease');
      inComponentVersion := JSONValue.GetValue<string>('ComponentVer');
    except
      on E: Exception do
      begin
        Log ('GetUpgradeVersion, Error parsing JSON');
        Log (E.Message);
        outErrorMsg := Format ('GetUpgradeVersion, Error parsing JSON. Error Msg [%s]', [E.Message]);
      end;
    end;

    try
      CreateLinkConn(Link, Conn);
      Query := NewQuery (Conn.ConnectionName);
    except
      on E: Exception do
      begin
        Log ('GetUpgradeVersion, Error creating link and connection');
        Log (E.Message);
        outErrorMsg := Format ('GetUpgradeVersion, Error creating link and connection. Error Msg [%s]', [E.Message]);
      end;
    end;

    try
      //try to insert the current status of the machine by first time
      try
        Duplicated := False;
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('Insert Into Status_Machine_Updates (Computer_ID, Enterprise, Store, Machine_Number, Application, Current_Ver_Maj, Current_Ver_Min, Current_Ver_Rel,');
        Query.SQL.Add('                                    Running_DB_Ver_Maj, Running_DB_Ver_Min, Running_DB_Ver_Rel, Last_Op, Date_Op, External_IP, Local_IP, Component_Version)');
        Query.SQL.Add('  Values (Upper (:ComputerID), Upper (:Enterprise), Upper (:Store), Upper (:Machine), Upper (:App), :Curr_Maj, :Curr_Min, :Curr_Rel, :DB_Maj, :DB_Min, :DB_Rel,');
        Query.SQL.Add('          ''INITIALIZED'', Now(), :ExternalIP, :LocalIP, :CompVer);');
        Query.ParamByName('COMPUTERID').AsString := inComputerID;
        Query.ParamByName('APP').AsString        := inApplication;
        Query.ParamByName('ENTERPRISE').AsString := inEnterprise;
        Query.ParamByName('STORE').AsString      := inStore;
        Query.ParamByName('MACHINE').AsString    := inMachineNumber;
        Query.ParamByName('DB_MAJ').AsInteger    := inDBMajor;
        Query.ParamByName('DB_MIN').AsInteger    := inDBMiner;
        Query.ParamByName('DB_REL').AsInteger    := inDBRelease;
        Query.ParamByName('CURR_MAJ').AsInteger  := inCurrMajor;
        Query.ParamByName('CURR_MIN').AsInteger  := inCurrMiner;
        Query.ParamByName('CURR_REL').AsInteger  := inCurrRelease;
        Query.ParamByName('EXTERNALIP').AsString := inExternalIP;
        Query.ParamByName('LOCALIP').AsString    := inLocalIP;
        Query.ParamByName('COMPVER').AsString    := inComponentVersion;
        Query.ExecSQL;
      except
        on E: EADSNativeException do
        begin
          //Status_Machine_Updates already exists, just update the information
          if Pos ('NativeError = 7057;', E.Message) > 0 then
          begin
            Duplicated := True;
            Query.Close;
            Query.SQL.Clear;
            Query.SQL.Add('Update Status_Machine_Updates');
            Query.SQL.Add('   Set Current_Ver_Maj     = :Curr_Maj,');
            Query.SQL.Add('       Current_Ver_Min     = :Curr_Min,');
            Query.SQL.Add('       Current_Ver_Rel     = :Curr_Rel,');
            Query.SQL.Add('       Running_DB_Ver_Maj  = :DB_Maj,');
            Query.SQL.Add('       Running_DB_Ver_Min  = :DB_Min,');
            Query.SQL.Add('       Running_DB_Ver_Rel  = :DB_Rel,');
            Query.SQL.Add('       Date_Op             = Now(),');
            Query.SQL.Add('       Computer_ID         = Upper (:ComputerID),');
            Query.SQL.Add('       External_IP         = :ExternalIP,');
            Query.SQL.Add('       Local_IP            = :LocalIP,');
            Query.SQL.Add('       Component_Version   = :CompVer,');
            Query.SQL.Add('       Enterprise          = :Enterprise,');
            Query.SQL.Add('       Store               = :Store,');
            Query.SQL.Add('       Machine_Number      = :Machine');
            Query.SQL.Add(' Where Upper (Application) = Upper (:App)');
            Query.SQL.Add('   And Upper (Computer_ID) = Upper (:ComputerID)');
            Query.SQL.Add('   And (Current_Ver_Maj     <> :Curr_Maj           Or');
            Query.SQL.Add('        Current_Ver_Min     <> :Curr_Min           Or');
            Query.SQL.Add('        Current_Ver_Rel     <> :Curr_Rel           Or');
            Query.SQL.Add('        Running_DB_Ver_Maj  <> :DB_Maj             Or');
            Query.SQL.Add('        Running_DB_Ver_Min  <> :DB_Min             Or');
            Query.SQL.Add('        Running_DB_Ver_Rel  <> :DB_Rel             Or');
            Query.SQL.Add('        External_IP         <> :ExternalIP         Or');
            Query.SQL.Add('        Enterprise          <> Upper (:Enterprise) Or');
            Query.SQL.Add('        Store               <> Upper (:Store)      Or');
            Query.SQL.Add('        Machine_Number      <> Upper (:Machine)    Or');
            Query.SQL.Add('        Local_IP            <> :LocalIP            Or');
            Query.SQL.Add('        Component_Version   <> :CompVer)');
            Query.ParamByName('APP').AsString        := inApplication;
            Query.ParamByName('COMPUTERID').AsString := inComputerID;
            Query.ParamByName('ENTERPRISE').AsString := inEnterprise;
            Query.ParamByName('STORE').AsString      := inStore;
            Query.ParamByName('MACHINE').AsString    := inMachineNumber;
            Query.ParamByName('DB_MAJ').AsInteger    := inDBMajor;
            Query.ParamByName('DB_MIN').AsInteger    := inDBMiner;
            Query.ParamByName('DB_REL').AsInteger    := inDBRelease;
            Query.ParamByName('CURR_MAJ').AsInteger  := inCurrMajor;
            Query.ParamByName('CURR_MIN').AsInteger  := inCurrMiner;
            Query.ParamByName('CURR_REL').AsInteger  := inCurrRelease;
            Query.ParamByName('EXTERNALIP').AsString := inExternalIP;
            Query.ParamByName('LOCALIP').AsString    := inLocalIP;
            Query.ParamByName('COMPVER').AsString    := inComponentVersion;
            Query.ExecSQL;
          end
          else
          begin
            Log ('GetUpgradeVersion, Error on Insert Into Status_Machine_Updates');
            Log (E.Message);
            outErrorMsg := Format ('GetUpgradeVersion, Error on Insert Into Status_Machine_Updates. Error Msg [%s]', [E.Message]);
          end;
        end;
      end;

      if not Duplicated then
      begin
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('Insert Into Log_Machine_Updates (Enterprise, Store, Machine_Number, Application, Current_Ver_Maj, Current_Ver_Min, Current_Ver_Rel,');
        Query.SQL.Add('                                 Running_DB_Ver_Maj, Running_DB_Ver_Min, Running_DB_Ver_Rel, Desc_Op, Date_Op, Computer_ID, External_IP, Local_IP, Component_Version)');
        Query.SQL.Add('  Values (Upper (:Enterprise), Upper (:Store), Upper (:Machine), Upper (:App), :Curr_Maj, :Curr_Min, :Curr_Rel, :DB_Maj, :DB_Min, :DB_Rel,');
        Query.SQL.Add('          ''INITIALIZED'', Now(), Upper (:ComputerID), :ExternalIP, :LocalIP, :CompVer);');
        Query.ParamByName('APP').AsString        := inApplication;
        Query.ParamByName('ENTERPRISE').AsString := inEnterprise;
        Query.ParamByName('STORE').AsString      := inStore;
        Query.ParamByName('MACHINE').AsString    := inMachineNumber;
        Query.ParamByName('DB_MAJ').AsInteger    := inDBMajor;
        Query.ParamByName('DB_MIN').AsInteger    := inDBMiner;
        Query.ParamByName('DB_REL').AsInteger    := inDBRelease;
        Query.ParamByName('CURR_MAJ').AsInteger  := inCurrMajor;
        Query.ParamByName('CURR_MIN').AsInteger  := inCurrMiner;
        Query.ParamByName('CURR_REL').AsInteger  := inCurrRelease;
        Query.ParamByName('COMPUTERID').AsString := inComputerID;
        Query.ParamByName('EXTERNALIP').AsString := inExternalIP;
        Query.ParamByName('LOCALIP').AsString    := inLocalIP;
        Query.ParamByName('COMPVER').AsString    := inComponentVersion;
        Query.ExecSQL;
      end;
    except
      //Do nothing
    end;

    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('Select C.Enterprise, C.Store, C.Machine_Number, C.Computer_ID, C.Application,');
    Query.SQL.Add('       C.Current_Ver_Maj, C.Current_Ver_Min, C.Current_Ver_Rel, C.Install_Ver,');
    Query.SQL.Add('       U.Lastest_Ver_Maj, U.Lastest_Ver_Min, U.Lastest_Ver_Rel');
    Query.SQL.Add('  From            Status_Machine_Updates C');
    Query.SQL.Add('  Left Outer Join Updates U On Upper (C.Application) = Upper (U.Application)');
    Query.SQL.Add('                               And ((     U.Enterprise  Is Not Null');
    Query.SQL.Add('                                      And U.Store       Is Null');
    Query.SQL.Add('                                      And (   U.Computer_ID Is Null And C.Computer_ID Is Null');
    Query.SQL.Add('                                           Or');
    Query.SQL.Add('                                              U.Computer_ID Is Not Null And Upper (U.Computer_ID) = Upper (C.Computer_ID)');
    Query.SQL.Add('                                          )');
    Query.SQL.Add('                                      And Upper (C.Enterprise) = Upper (U.Enterprise)');
    Query.SQL.Add('                                    )');
    Query.SQL.Add('                                    Or');
    Query.SQL.Add('                                    (     U.Enterprise Is Not Null');
    Query.SQL.Add('                                      And U.Store      Is Not Null');
    Query.SQL.Add('                                      And (   U.Computer_ID Is Null And C.Computer_ID Is Null');
    Query.SQL.Add('                                           Or');
    Query.SQL.Add('                                              U.Computer_ID Is Not Null And Upper (U.Computer_ID) = Upper (C.Computer_ID)');
    Query.SQL.Add('                                          )');
    Query.SQL.Add('                                      And Upper (C.Enterprise) = Upper (U.Enterprise)');
    Query.SQL.Add('                                      And Upper (C.Store)      = Upper (U.Store)');
    Query.SQL.Add('                                    )');
    Query.SQL.Add('                                    Or');
    Query.SQL.Add('                                    (    U.Enterprise Is Null');
    Query.SQL.Add('                                     And U.Store      Is Null');
    Query.SQL.Add('                                     And Upper (U.Computer_ID) = Upper (C.Computer_ID)');
    Query.SQL.Add('                                    )');
    Query.SQL.Add('                                   )');
    Query.SQL.Add('  Where Upper (C.Enterprise)     = Upper (:Enterprise)');
    Query.SQL.Add('    And Upper (C.Store)          = Upper (:Store)');
    Query.SQL.Add('    And Upper (C.Machine_Number) = Upper (:Machine_Number)');
    Query.SQL.Add('    And Upper (C.Application)    = Upper (:App)');
    Query.ParamByName('App').AsString            := inApplication;
    Query.ParamByName('Enterprise').AsString     := inEnterprise;
    Query.ParamByName('Store').AsString          := inStore;
    Query.ParamByName('Machine_Number').AsString := inMachineNumber;
    Query.Open;

    SelVersion     := '';
    InstalledVer   := Format ('%d.%d.%d', [Query.FieldByName('Current_Ver_Maj').AsInteger, Query.FieldByName('Current_Ver_Min').AsInteger, Query.FieldByName('Current_Ver_Rel').AsInteger]);
    NewSpecificVer := Query.FieldByName('Install_Ver').AsString;
    NewGlobalVer   := '';
    if not Query.FieldByName('Lastest_Ver_Maj').IsNull and
       not Query.FieldByName('Lastest_Ver_Min').IsNull and
       not Query.FieldByName('Lastest_Ver_Rel').IsNull then
       NewGlobalVer := Format ('%d.%d.%d', [Query.FieldByName('Lastest_Ver_Maj').AsInteger, Query.FieldByName('Lastest_Ver_Min').AsInteger, Query.FieldByName('Lastest_Ver_Rel').AsInteger]);

    if not NewSpecificVer.IsEmpty and (CompareVersion(NewSpecificVer, InstalledVer) = coGreater) then
      SelVersion := NewSpecificVer
    else if not NewGlobalVer.IsEmpty and (CompareVersion(NewGlobalVer, InstalledVer) = coGreater) then
      SelVersion := NewGlobalVer;

    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('Select U.Application, U.Lastest_Ver_Maj, U.Lastest_Ver_Min, U.Lastest_Ver_Rel,');
    Query.SQL.Add('       Case');
    Query.SQL.Add('         When     U.Lastest_Ver_Maj > :Curr_Maj Then ''Major''');
    Query.SQL.Add('         When     U.Lastest_Ver_Maj = :Curr_Maj');
    Query.SQL.Add('              And U.Lastest_Ver_Min > :Curr_Min Then ''Miner''');
    Query.SQL.Add('         When     U.Lastest_Ver_Maj = :Curr_Maj');
    Query.SQL.Add('              And U.Lastest_Ver_Min = :Curr_Min');
    Query.SQL.Add('              And U.Lastest_Ver_Rel > :Curr_Rel Then ''Release''');
    Query.SQL.Add('       End                                                        Version_Update_Type,');
    Query.SQL.Add('       IfNull (A.Update_Type_ID, 0)                               Update_Type_ID,');
    Query.SQL.Add('       Coalesce (S.Update_To_ID, A.Update_Type_ID, 0)             User_Update_To_ID,');
    Query.SQL.Add('       P.Update_Priority_ID                                       User_Priority_ID,');
    Query.SQL.Add('       P.Update_Priority_Desc,');
    Query.SQL.Add('       IfNull (S.Idle_Time,    U.Idle_Time)                       User_Idle_Time,');
    Query.SQL.Add('       UT.Update_Desc,');
    Query.SQL.Add('       U.URL_Download, U.TargetFileName,');
    Query.SQL.Add('       U.Release_Notes, U.Path_To_Install,');
    Query.SQL.Add('       U.Min_DB_Req_Maj, U.Min_DB_Req_Min, U.Min_DB_Req_Rel,');
    Query.SQL.Add('	   I.Installer_Desc,');
    Query.SQL.Add('	   Case When J.Application Is Null Then 0 Else 1 End As Apply_Major_Update');
    Query.SQL.Add('  From Updates      U');
    Query.SQL.Add('  Join Applications A On Upper (U.Application) = Upper (A.Application)');
    Query.SQL.Add('  Left Outer Join Status_Machine_Updates S On     Upper (U.Application)    = Upper (S.Application)');
    Query.SQL.Add('                                              And Upper (S.Enterprise)     = Upper (:Enterprise)');
    Query.SQL.Add('                                              And Upper (S.Store)          = Upper (:Store)');
    Query.SQL.Add('                                              And Upper (S.Machine_Number) = Upper (:Machine)');
    Query.SQL.Add('  Left Outer Join Update_Type UT On UT.Update_Type_ID     = Case When S.Update_To_ID Is Null Or');
    Query.SQL.Add('                                                                      S.Update_To_ID = 0 Then A.Update_Type_ID');
    Query.SQL.Add('                                                                 Else S.Update_To_ID');
    Query.SQL.Add('                                                            End');
    Query.SQL.Add('  Left Outer Join Update_Priority       P  On P.Update_Priority_ID  = Coalesce (S.Priority_ID, U.Priority_ID, A.Priority_ID, 0)');
    Query.SQL.Add('  Left Outer Join Update_Installer_Type I  On U.Installer_Type      = I.Installer_Type');
    Query.SQL.Add('  Left Outer Join Just_Major_Updates    J  On     Upper (U.Application) = Upper (J.Application)');
    Query.SQL.Add('                                              And Upper (J.Enterprise)  = Upper (:Enterprise)');
    Query.SQL.Add('                                              And (J.Store Is Null Or Upper (J.Store) = Upper (:Store))');
    Query.SQL.Add(' Where Upper (U.Application) = Upper (:App)');
    Query.SQL.Add('   And U.Active      = True');
    Query.SQL.Add('   And (    IfNull (U.Min_DB_Req_Maj, 0) < :DB_Maj');
    Query.SQL.Add('        Or');
    Query.SQL.Add('           (    IfNull (U.Min_DB_Req_Maj, 0) = :DB_Maj');
    Query.SQL.Add('            And IfNull (U.Min_DB_Req_Min, 0) < :DB_Min');
    Query.SQL.Add('           )');
    Query.SQL.Add('        Or (    IfNull (U.Min_DB_Req_Maj, 0) =  :DB_Maj');
    Query.SQL.Add('            And IfNull (U.Min_DB_Req_Min, 0) =  :DB_Min');
    Query.SQL.Add('            And IfNull (U.Min_DB_Req_Rel, 0) <= :DB_Rel');
    Query.SQL.Add('           )');
    Query.SQL.Add('       )');
    Query.SQL.Add('   And (   U.Lastest_Ver_Maj > :Curr_Maj');
    Query.SQL.Add('        Or (    U.Lastest_Ver_Maj = :Curr_Maj');
    Query.SQL.Add('            And U.Lastest_Ver_Min > :Curr_Min');
    Query.SQL.Add('           )');
    Query.SQL.Add('        Or (    U.Lastest_Ver_Maj = :Curr_Maj');
    Query.SQL.Add('            And U.Lastest_Ver_Min = :Curr_Min');
    Query.SQL.Add('            And U.Lastest_Ver_Rel > :Curr_Rel)');
    Query.SQL.Add('       )');
    Query.SQL.Add(' Order By U.Lastest_Ver_Maj Desc, U.Lastest_Ver_Min Desc, U.Lastest_Ver_Rel Desc');
    Query.ParamByName('APP').AsString        := inApplication;
    Query.ParamByName('ENTERPRISE').AsString := inEnterprise;
    Query.ParamByName('STORE').AsString      := inStore;
    Query.ParamByName('MACHINE').AsString    := inMachineNumber;
    Query.ParamByName('DB_MAJ').AsInteger    := inDBMajor;
    Query.ParamByName('DB_MIN').AsInteger    := inDBMiner;
    Query.ParamByName('DB_REL').AsInteger    := inDBRelease;
    Query.ParamByName('CURR_MAJ').AsInteger  := inCurrMajor;
    Query.ParamByName('CURR_MIN').AsInteger  := inCurrMiner;
    Query.ParamByName('CURR_REL').AsInteger  := inCurrRelease;
    Query.Open;

    if Query.Eof then exit;

    if not SelVersion.IsEmpty then
    begin
      Query.First;
      Found := False;
      while not Query.Eof and not Found do
      begin
        s := Format ('%d.%d.%d', [Query.FieldByName('Lastest_Ver_Maj').AsInteger, Query.FieldByName('Lastest_Ver_Min').AsInteger, Query.FieldByName('Lastest_Ver_Rel').AsInteger]);
        if CompareVersion (s, SelVersion) = coEqual then
        begin
          Found             := True;
          outApplication    := Query.FieldByName('Application').AsString;
          outUpdatePriority := Query.FieldByName('Update_Priority_Desc').AsString;
          outIdleTime       := Query.FieldByName('User_Idle_Time').AsString;
          outVersion        := Query.FieldByName('Lastest_Ver_Maj').AsString + '.' +
                               Query.FieldByName('Lastest_Ver_Min').AsString + '.' +
                               Query.FieldByName('Lastest_Ver_Rel').AsString;
          outURL            := Query.FieldByName('URL_Download').AsString;
          outReleaseNotes   := Query.FieldByName('Release_Notes').AsString;
          outTargetFileName := Query.FieldByName('TargetFileName').AsString;
          outPathToInstall  := Query.FieldByName('Path_To_Install').AsString;
          outInstallerType  := Query.FieldByName('Installer_Desc').AsString;
        end;

        Query.Next;
      end;

      if Found then Exit;
    end;

    Query.First;
    if Query.FieldByName ('Update_Desc').IsNull or (Query.FieldByName ('Update_Desc').AsString = 'NoUpdate') then exit;

    if Query.FieldByName ('Apply_Major_Update').AsInteger = 1 then
    begin
      //If it applies major update, it checks if the highest is major update and returns it,
      //otherwise it does not return anything
      if Query.FieldByName('Lastest_Ver_Maj').AsInteger > inCurrMajor then
      begin
        outApplication    := Query.FieldByName('Application').AsString;
        outUpdatePriority := Query.FieldByName('Update_Priority_Desc').AsString;
        outIdleTime       := Query.FieldByName('User_Idle_Time').AsString;
        outVersion        := Query.FieldByName('Lastest_Ver_Maj').AsString + '.' +
                             Query.FieldByName('Lastest_Ver_Min').AsString + '.' +
                             Query.FieldByName('Lastest_Ver_Rel').AsString;
        outURL            := Query.FieldByName('URL_Download').AsString;
        outReleaseNotes   := Query.FieldByName('Release_Notes').AsString;
        outTargetFileName := Query.FieldByName('TargetFileName').AsString;
        outPathToInstall  := Query.FieldByName('Path_To_Install').AsString;
        outInstallerType  := Query.FieldByName('Installer_Desc').AsString;
      end;

      exit;
    end;

    UpdateType := '';
    while not Query.Eof do
    begin
      outApplication    := Query.FieldByName('Application').AsString;
      outUpdatePriority := Query.FieldByName('Update_Priority_Desc').AsString;
      outIdleTime       := Query.FieldByName('User_Idle_Time').AsString;
      UpdateType        := Query.FieldByName('Update_Desc').AsString;

      if not Assigned (bmMaj) and (Query.FieldByName ('Version_Update_Type').AsString = 'Major') then
        bmMaj := Query.GetBookmark;

      if not Assigned (bmMin) and (Query.FieldByName ('Version_Update_Type').AsString = 'Miner') then
        bmMin := Query.GetBookmark;

      if not Assigned (bmRel) and (Query.FieldByName ('Version_Update_Type').AsString = 'Release') then
        bmRel := Query.GetBookmark;

      Query.Next;
    end;

    if Assigned (bmRel) and (UpdateType = 'LastestRelease') then
    begin
      Query.GotoBookmark(bmRel);
      outVersion        := Query.FieldByName('Lastest_Ver_Maj').AsString + '.' +
                           Query.FieldByName('Lastest_Ver_Min').AsString + '.' +
                           Query.FieldByName('Lastest_Ver_Rel').AsString;
      outURL            := Query.FieldByName('URL_Download').AsString;
      outReleaseNotes   := Query.FieldByName('Release_Notes').AsString;
      outTargetFileName := Query.FieldByName('TargetFileName').AsString;
      outPathToInstall  := Query.FieldByName('Path_To_Install').AsString;
      outInstallerType  := Query.FieldByName('Installer_Desc').AsString;
    end;

    bm := Nvl (bmMin, bmRel);
    if Assigned (bm) and (UpdateType = 'LastestMiner') then
    begin
      Query.GotoBookmark(bm);

      outVersion        := Query.FieldByName('Lastest_Ver_Maj').AsString + '.' +
                           Query.FieldByName('Lastest_Ver_Min').AsString + '.' +
                           Query.FieldByName('Lastest_Ver_Rel').AsString;
      outURL            := Query.FieldByName('URL_Download').AsString;
      outReleaseNotes   := Query.FieldByName('Release_Notes').AsString;
      outTargetFileName := Query.FieldByName('TargetFileName').AsString;
      outPathToInstall  := Query.FieldByName('Path_To_Install').AsString;
      outInstallerType  := Query.FieldByName('Installer_Desc').AsString;
    end;

    bm := Nvl (bmMaj, Nvl (bmMin, bmRel));
    if Assigned (bm) and (UpdateType = 'LastestMajor') then
    begin
      Query.GotoBookmark(bm);

      outVersion        := Query.FieldByName('Lastest_Ver_Maj').AsString + '.' +
                           Query.FieldByName('Lastest_Ver_Min').AsString + '.' +
                           Query.FieldByName('Lastest_Ver_Rel').AsString;
      outURL            := Query.FieldByName('URL_Download').AsString;
      outReleaseNotes   := Query.FieldByName('Release_Notes').AsString;
      outTargetFileName := Query.FieldByName('TargetFileName').AsString;
      outPathToInstall  := Query.FieldByName('Path_To_Install').AsString;
      outInstallerType  := Query.FieldByName('Installer_Desc').AsString;
    end;
  finally
    if Assigned (Query) then
    begin
      if Assigned (bmMaj)   then Query.FreeBookmark(bmMaj);
      if Assigned (bmMin)   then Query.FreeBookmark(bmMin);
      if Assigned (bmRel)   then Query.FreeBookmark(bmRel);

      Query.Close;
      Query.Free;
    end;

    if Assigned (Conn) then
    begin
      Conn.Close;
      Conn.Free;
    end;

    if Assigned (Link) then
      Link.Free;

    Obj := TJSONObject.Create;
    try
      Obj.AddPair(TJSONPair.Create('Application',    outApplication));
      Obj.AddPair(TJSONPair.Create('UpdatePriority', outUpdatePriority));
      Obj.AddPair(TJSONPair.Create('IdleTime',       outIdleTime));
      Obj.AddPair(TJSONPair.Create('Version',        outVersion));
      Obj.AddPair(TJSONPair.Create('URL',            outURL));
      Obj.AddPair(TJSONPair.Create('ReleaseNotes',   outReleaseNotes));
      Obj.AddPair(TJSONPair.Create('TargetFileName', outTargetFileName));
      Obj.AddPair(TJSONPair.Create('PathToInstall',  outPathToInstall));
      Obj.AddPair(TJSONPair.Create('InstallerType',  outInstallerType));
      Obj.AddPair(TJSONPair.Create('ErrorMsg',       outErrorMsg));
      result := Obj.ToJSON;
    finally
      Obj.Free;
    end;
  end;
end;

function TAutoUpdater.LogDownloadDone_1_0_1 (const Info: UnicodeString): UnicodeString;
begin
  result := LogOperationDone (Info, 'DOWNLOADED');
end;

function TAutoUpdater.LogUpdateDone_1_0_1 (const Info: UnicodeString): UnicodeString;
begin
  result := LogOperationDone (Info, 'UPDATED');
end;

function TAutoUpdater.GetUpdateInfo (const Info: UnicodeString): UnicodeString;
var
  Ver: string;
begin
  Ver    := GetComponentVersion(Info);
  result := 'ERROR: Invalid TUpdateChecker version';

  if Ver = '1.0.1' then
    result := GetUpgradeVersion_1_0_1 (Info);
end;

function TAutoUpdater.ConfirmDownload (const Info: UnicodeString): UnicodeString; stdcall;
var
  Ver: string;
begin
  Ver    := GetComponentVersion(Info);
  result := 'ERROR: Invalid TUpdateChecker version';

  if Ver = '1.0.1' then
    result := LogDownloadDone_1_0_1 (Info);
end;

function TAutoUpdater.ConfirmUpdate (const Info: UnicodeString): UnicodeString; stdcall;
var
  Ver: string;
begin
  Ver    := GetComponentVersion(Info);
  result := 'ERROR: Invalid TUpdateChecker version';

  if Ver = '1.0.1' then
    result := LogUpdateDone_1_0_1 (Info);
end;

initialization
{ Invokable classes must be registered }
   InvRegistry.RegisterInvokableClass(TAutoUpdater);
end.

