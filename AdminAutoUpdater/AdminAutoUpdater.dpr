program AdminAutoUpdater;

uses
  Vcl.Forms,
  uMainAdmin in 'uMainAdmin.pas' {FMainAdmin},
  uUploadDlg in 'uUploadDlg.pas' {FUploadDlg},
  uAddAppDlg in 'uAddAppDlg.pas' {FAddAppDlg},
  uProgramInfo in 'uProgramInfo.pas',
  uValidateDeleteDlg in 'uValidateDeleteDlg.pas' {FValidateDeleteDlg};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMainAdmin, FMainAdmin);
  Application.CreateForm(TFUploadDlg, FUploadDlg);
  Application.CreateForm(TFAddAppDlg, FAddAppDlg);
  Application.CreateForm(TFValidateDeleteDlg, FValidateDeleteDlg);
  Application.Run;
end.
