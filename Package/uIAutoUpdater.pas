// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://connect.compudime.com:8080/WSAutoUpdater/WSAutoUpdater.dll/wsdl/IAutoUpdater
//  >Import : http://connect.compudime.com:8080/WSAutoUpdater/WSAutoUpdater.dll/wsdl/IAutoUpdater>0
// Version  : 1.0
// (25/09/2020 12:22:03 a.m. - - $Rev: 96726 $)
// ************************************************************************ //

unit uIAutoUpdater;

interface

uses Soap.InvokeRegistry, Soap.SOAPHTTPClient, System.Types, Soap.XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:int             - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]

  TVersionInfoResponse = class;                 { "urn:AutoUpdaterIntf"[GblCplx] }
  TVersionInfoRequest  = class;                 { "urn:AutoUpdaterIntf"[GblCplx] }



  // ************************************************************************ //
  // XML       : TVersionInfoResponse, global, <complexType>
  // Namespace : urn:AutoUpdaterIntf
  // ************************************************************************ //
  TVersionInfoResponse = class(TRemotable)
  private
    FApplication_: string;
    FVersion: string;
    FURL: string;
    FReleaseNotes: string;
    FTargetFileName: string;
    FPathToInstall: string;
    FUpdatePriority: string;
    FIdleTime: string;
    FInstallerType: string;
  published
    property Application_:   string  read FApplication_ write FApplication_;
    property Version:        string  read FVersion write FVersion;
    property URL:            string  read FURL write FURL;
    property ReleaseNotes:   string  read FReleaseNotes write FReleaseNotes;
    property TargetFileName: string  read FTargetFileName write FTargetFileName;
    property PathToInstall:  string  read FPathToInstall write FPathToInstall;
    property UpdatePriority: string  read FUpdatePriority write FUpdatePriority;
    property IdleTime:       string  read FIdleTime write FIdleTime;
    property InstallerType:  string  read FInstallerType write FInstallerType;
  end;



  // ************************************************************************ //
  // XML       : TVersionInfoRequest, global, <complexType>
  // Namespace : urn:AutoUpdaterIntf
  // ************************************************************************ //
  TVersionInfoRequest = class(TRemotable)
  private
    FApplication_: string;
    FEnterprise: string;
    FStore: string;
    FMachineNumber: string;
    FDBMajor: Integer;
    FDBMiner: Integer;
    FDBRelease: Integer;
    FCurrMajor: Integer;
    FCurrMiner: Integer;
    FCurrRelease: Integer;
  published
    property Application_:  string   read FApplication_ write FApplication_;
    property Enterprise:    string   read FEnterprise write FEnterprise;
    property Store:         string   read FStore write FStore;
    property MachineNumber: string   read FMachineNumber write FMachineNumber;
    property DBMajor:       Integer  read FDBMajor write FDBMajor;
    property DBMiner:       Integer  read FDBMiner write FDBMiner;
    property DBRelease:     Integer  read FDBRelease write FDBRelease;
    property CurrMajor:     Integer  read FCurrMajor write FCurrMajor;
    property CurrMiner:     Integer  read FCurrMiner write FCurrMiner;
    property CurrRelease:   Integer  read FCurrRelease write FCurrRelease;
  end;


  // ************************************************************************ //
  // Namespace : urn:AutoUpdaterIntf-IAutoUpdater
  // soapAction: urn:AutoUpdaterIntf-IAutoUpdater#%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // use       : encoded
  // binding   : IAutoUpdaterbinding
  // service   : IAutoUpdaterservice
  // port      : IAutoUpdaterPort
  // URL       : http://localhost/WSAutoUpdater/WSAutoUpdater.dll/soap/IAutoUpdater
  // ************************************************************************ //
  IAutoUpdater = interface(IInvokable)
  ['{049D8EB9-8AD4-BF09-3CA1-284C04638D84}']
    function  echoStr(const Value: string): string; stdcall;
    function  GetUpgradeVersion(const Request: TVersionInfoRequest): TVersionInfoResponse; stdcall;
    function  LogDownloadDone(const ProgramName: string; const Version: string; const DBVersion: string; const Enterprise: string; const Store: string; const MachineNumber: string
                              ): string; stdcall;
    function  LogUpdateDone(const ProgramName: string; const Version: string; const DBVersion: string; const Enterprise: string; const Store: string; const MachineNumber: string
                            ): string; stdcall;
    function  GetUpdateInfo(const Info: string): string; stdcall;
    function  ConfirmDownload(const Info: string): string; stdcall;
    function  ConfirmUpdate(const Info: string): string; stdcall;
  end;

function GetIAutoUpdater(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IAutoUpdater;


implementation
  uses System.SysUtils;

function GetIAutoUpdater(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IAutoUpdater;
const
  defWSDL = 'http://localhost/WSAutoUpdater/WSAutoUpdater.dll/wsdl/IAutoUpdater';
  defURL  = 'http://localhost/WSAutoUpdater/WSAutoUpdater.dll/soap/IAutoUpdater';
  defSvc  = 'IAutoUpdaterservice';
  defPrt  = 'IAutoUpdaterPort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IAutoUpdater);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  { IAutoUpdater }
  InvRegistry.RegisterInterface(TypeInfo(IAutoUpdater), 'urn:AutoUpdaterIntf-IAutoUpdater', '');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IAutoUpdater), 'urn:AutoUpdaterIntf-IAutoUpdater#%operationName%');
  RemClassRegistry.RegisterXSClass(TVersionInfoResponse, 'urn:AutoUpdaterIntf', 'TVersionInfoResponse');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TVersionInfoResponse), 'Application_', '[ExtName="Application"]');
  RemClassRegistry.RegisterXSClass(TVersionInfoRequest, 'urn:AutoUpdaterIntf', 'TVersionInfoRequest');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(TVersionInfoRequest), 'Application_', '[ExtName="Application"]');

end.
