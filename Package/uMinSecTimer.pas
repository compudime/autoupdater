unit uMinSecTimer;

interface
uses System.SysUtils, System.Classes, Vcl.ExtCtrls, DateUtils, Math;

const
  ErrMsgInvalidFormat = 'Invalid interval format, must be hh:mm:ss or mm:ss';
  
type
  TMinSecTimer = class (TComponent)
  private
    FTimer:           TTimer;
    FInterval:        string;
    FOnTimer:         TNotifyEvent;
    FStartInterval:   TDateTime;
    FSecondsInterval: Int64;
    procedure  SetInterval (Value: string);
    function GetEnabled: boolean;
    procedure SetEnabled(Value: boolean);
    procedure TimerEvent (Sender: TObject);
    procedure UpdateInterval;
    procedure ResetStartTime;
    function IsIntervalFulfilled: boolean;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
    function DecodeHourMinSec (Data: string; var Hour, Min, Sec: integer): boolean;
    function GetSeconds (Hour, Min, Sec: integer): integer; overload;
    function GetSeconds (Data: string): integer; overload;
    function ValidateIntervalFormat (s: string): boolean;
  published
    property Interval: string read FInterval write SetInterval;
    property Enabled: boolean read GetEnabled write SetEnabled default False;
    property OnTimer: TNotifyEvent read FOnTimer write FOnTimer;
  end;

procedure Register;

implementation

constructor TMinSecTimer.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FTimer          := TTimer.Create(Self);
  FTimer.Interval := 500;
  FTimer.OnTimer  := TimerEvent;
  FInterval       := '';
  FOnTimer        := nil;
end;

destructor TMinSecTimer.Destroy;
begin
  FTimer.Enabled := False;
  FTimer.Free;

  inherited Destroy;
end;

procedure TMinSecTimer.SetInterval (Value: string);
begin
  if FInterval <> Value then
  begin
    if not ValidateIntervalFormat (Value) then
      raise Exception.Create(ErrMsgInvalidFormat);

    FTimer.Enabled := False;
    FInterval      := Value;

    UpdateInterval;
  end;
end;

function TMinSecTimer.GetEnabled: boolean;
begin
  result := FTimer.Enabled;
end;

procedure TMinSecTimer.SetEnabled (Value: boolean);
begin
  if FTimer.Enabled <> Value then
  begin
    if Value then
    begin
      if FInterval = '' then
        raise Exception.Create('Interval property must have value');

      if FSecondsInterval = 0 then
        raise Exception.Create('Interval must be greater than 0');
        
      ResetStartTime;
    end;

    FTimer.Enabled := Value;
  end;
end;

procedure TMinSecTimer.TimerEvent (Sender: TObject);
begin
  if not IsIntervalFulfilled then exit;

  ResetStartTime;

  if Assigned (FOnTimer) then
    FOnTimer (Sender);
end;

function TMinSecTimer.DecodeHourMinSec (Data: string; var Hour, Min, Sec: integer): boolean;
var
  s, tmp: string;
begin
  Hour   := 0;
  Min    := 0;
  Sec    := 0;
  tmp    := Data;
  result := Length (Data) in [5, 8];
  if not result then exit;

  //Extract seconds
  s      := Copy (tmp, Length (tmp) - 1, 2);
  result := TryStrToInt (s, Sec);

  if not result or not Math.InRange (Sec, 0, 59) then
  begin
    result := False;
    exit;
  end;

  Delete (tmp, Length (tmp) - 2, 3);

  //Extract minutes
  s      := Copy (tmp, Length (tmp) - 1, 2);
  result :=  TryStrToInt(s, Min);

  if not result or not Math.InRange (Min, 0, 59) then
  begin
    result := False;
    exit;
  end;

  if Length (tmp) = 2 then exit;

  //Extract hours
  s      := Copy (tmp, 1, 2);
  result := TryStrToInt(s, Hour);
  if not result or not Math.InRange (Hour, 0, 24) then
    result := False;
end;

function TMinSecTimer.GetSeconds (Hour, Min, Sec: integer): integer;
begin
  result := Sec + (Min * 60) +  (Hour * 3600);
end;

function TMinSecTimer.GetSeconds (Data: string): integer;
var
  H, M, S: integer;
begin
  if not ValidateIntervalFormat (Data) then
    raise Exception.Create(ErrMsgInvalidFormat);

  DecodeHourMinSec (Data, H, M, S);
  result := GetSeconds (H, M, S);
end;


function TMinSecTimer.ValidateIntervalFormat (s: string): boolean;
var
  Hour, Min, Sec: integer;
begin
  result := DecodeHourMinSec(s, Hour, Min, Sec);
end;

procedure TMinSecTimer.UpdateInterval;
var
  Hour, Min, Sec: integer;
begin
  FSecondsInterval := 0;
  if not DecodeHourMinSec(FInterval, Hour, Min, Sec) then exit;

  FSecondsInterval := GetSeconds (Hour, Min, Sec);
end;

procedure TMinSecTimer.ResetStartTime;
begin
  FStartInterval := Now;
end;
                                  
function TMinSecTimer.IsIntervalFulfilled: boolean;
var
  Seconds: Int64;
begin
  Seconds := SecondsBetween(Now, FStartInterval);
  result  := Seconds >= FSecondsInterval;
end;

procedure Register;
begin
  RegisterComponents('Compudime', [TMinSecTimer]);
end;

end.
