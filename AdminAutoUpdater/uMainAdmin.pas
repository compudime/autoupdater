unit uMainAdmin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IniFiles, System.IOUtils, Vcl.ComCtrls,
  Vcl.ExtCtrls, Vcl.StdCtrls, System.Actions, Vcl.ActnList, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.ADS, FireDAC.Phys.ADSDef, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, System.ImageList, Vcl.ImgList, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  dxSkinsDefaultPainters, cxButtons, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, cxControls, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxDBLookupComboBox, Math, cxTextEdit, cxDBNavigator, cxMemo,
  cxContainer, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  Vcl.Grids, Vcl.DBGrids, uUploadDlg, JvBaseDlg, JvBrowseFolder,
  FireDAC.Phys.ADSWrapper, uValidateDeleteDlg, uUpdateChecker;

const
  ErrMsgInvalidFormat = 'Invalid interval format, must be hh:mm:ss or mm:ss';

const
  PNL_App_Version = 0;
  PNL_DB_Status   = 1;

type
  TFMainAdmin = class(TForm)
    PageControl1: TPageControl;
    pagParamsApps: TTabSheet;
    pagUpdates: TTabSheet;
    pagComputerStatus: TTabSheet;
    pagLog: TTabSheet;
    Shape1: TShape;
    Shape2: TShape;
    Shape4: TShape;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    actDBConnect: TAction;
    actDBDisconnect: TAction;
    ImageList1: TImageList;
    Conn: TFDConnection;
    FDPhysADSDriverLink1: TFDPhysADSDriverLink;
    tabApplications: TFDTable;
    tabUpdates: TFDTable;
    tabComputer: TFDTable;
    qryLog: TFDQuery;
    tabLog: TFDTable;
    dsApplications: TDataSource;
    dsUpdates: TDataSource;
    dsComputer: TDataSource;
    dsLog: TDataSource;
    dsLogDetail: TDataSource;
    GridLogDBTableView1: TcxGridDBTableView;
    GridLogLevel1: TcxGridLevel;
    GridLog: TcxGrid;
    tabUpdateType: TFDTable;
    dsUpdateType: TDataSource;
    tabPriority: TFDTable;
    dsPriority: TDataSource;
    qryAppUpdateType: TFDQuery;
    dsAppUpdateType: TDataSource;
    tabInstallers: TFDTable;
    dsInstallers: TDataSource;
    TimerLog: TTimer;
    TimerMachine: TTimer;
    GridLogDBTableView1Enterprise: TcxGridDBColumn;
    GridLogDBTableView1Store: TcxGridDBColumn;
    GridLogDBTableView1Machine_Number: TcxGridDBColumn;
    GridLogDBTableView1Application: TcxGridDBColumn;
    GridLogDBTableView1Desc_Op: TcxGridDBColumn;
    GridLogDBTableView1Date_Op: TcxGridDBColumn;
    Panel1: TPanel;
    Shape5: TShape;
    GridUpdates: TcxGrid;
    GridUpdatesDBTableView1: TcxGridDBTableView;
    GridUpdatesDBTableView1Application: TcxGridDBColumn;
    GridUpdatesDBTableView1URL_Download: TcxGridDBColumn;
    GridUpdatesDBTableView1TargetFileName: TcxGridDBColumn;
    GridUpdatesDBTableView1Release_Notes: TcxGridDBColumn;
    GridUpdatesDBTableView1Path_To_Install: TcxGridDBColumn;
    GridUpdatesDBTableView1Installer_Type: TcxGridDBColumn;
    GridUpdatesDBTableView1Priority_ID: TcxGridDBColumn;
    GridUpdatesDBTableView1Idle_Time: TcxGridDBColumn;
    GridUpdatesDBTableView1Active: TcxGridDBColumn;
    GridUpdatesLevel1: TcxGridLevel;
    cxDBNavigator2: TcxDBNavigator;
    Panel2: TPanel;
    Shape6: TShape;
    Splitter1: TSplitter;
    cbApplications: TcxLookupComboBox;
    Label5: TLabel;
    qryUpdatesByApp: TFDQuery;
    dsUpdatesByApp: TDataSource;
    GridUpdatesByAppDBTableView1: TcxGridDBTableView;
    GridUpdatesByAppLevel1: TcxGridLevel;
    GridUpdatesByApp: TcxGrid;
    GridUpdatesByAppDBTableView1URL_Download: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1TargetFileName: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1Release_Notes: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1Path_To_Install: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1Priority_ID: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1Idle_Time: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1Installer_Type: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1Active: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    RowEven: TcxStyle;
    RowOdd: TcxStyle;
    Header: TcxStyle;
    Selection: TcxStyle;
    ReadOnly: TcxStyle;
    tabApplicationsApplication: TStringField;
    tabApplicationsUpdate_Type_ID: TIntegerField;
    tabApplicationsPriority_ID: TIntegerField;
    tabApplicationsIdle_Time: TStringField;
    tabApplicationsApp_Directory: TStringField;
    tabApplicationsTotalVersions: TIntegerField;
    tabApplicationsTotalDownloads: TIntegerField;
    tabApplicationsLastDownload: TDateTimeField;
    qryCalcAppTotVersions: TFDQuery;
    qryCalcAppDownladsSummary: TFDQuery;
    actUpload: TAction;
    tabUpdatesApplication: TStringField;
    tabUpdatesLastest_Ver_Maj: TIntegerField;
    tabUpdatesLastest_Ver_Min: TIntegerField;
    tabUpdatesLastest_Ver_Rel: TIntegerField;
    tabUpdatesMin_DB_Req_Maj: TIntegerField;
    tabUpdatesMin_DB_Req_Min: TIntegerField;
    tabUpdatesMin_DB_Req_Rel: TIntegerField;
    tabUpdatesURL_Download: TStringField;
    tabUpdatesTargetFileName: TStringField;
    tabUpdatesRelease_Notes: TStringField;
    tabUpdatesPath_To_Install: TStringField;
    tabUpdatesPriority_ID: TIntegerField;
    tabUpdatesIdle_Time: TStringField;
    tabUpdatesInstaller_Type: TIntegerField;
    tabUpdatesActive: TBooleanField;
    tabUpdatesVersion: TStringField;
    pagSetup: TTabSheet;
    Shape7: TShape;
    Label1: TLabel;
    edAlias: TEdit;
    Label2: TLabel;
    cbServerType: TComboBox;
    Label3: TLabel;
    edUserName: TEdit;
    Label4: TLabel;
    edPassword: TEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label6: TLabel;
    edRepositoryURL: TEdit;
    actAppPost: TAction;
    actAppCancel: TAction;
    Label7: TLabel;
    edRootDirectory: TEdit;
    btnSelectFile: TcxButton;
    SelectDirDlg: TJvBrowseForFolderDialog;
    GridUpdatesDBTableView1Version: TcxGridDBColumn;
    tabUpdatesDBVersion: TStringField;
    GridUpdatesDBTableView1DBVersion: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1Version: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1DBVersion: TcxGridDBColumn;
    tabComputerEnterprise: TStringField;
    tabComputerStore: TStringField;
    tabComputerMachine_Number: TStringField;
    tabComputerApplication: TStringField;
    tabComputerCurrent_Ver_Maj: TIntegerField;
    tabComputerCurrent_Ver_Min: TIntegerField;
    tabComputerCurrent_Ver_Rel: TIntegerField;
    tabComputerRunning_DB_Ver_Maj: TIntegerField;
    tabComputerRunning_DB_Ver_Min: TIntegerField;
    tabComputerRunning_DB_Ver_Rel: TIntegerField;
    tabComputerUpdate_To_ID: TIntegerField;
    tabComputerPriority_ID: TIntegerField;
    tabComputerIdle_Time: TStringField;
    tabComputerLast_Op: TStringField;
    tabComputerDate_Op: TSQLTimeStampField;
    tabComputerVersion: TStringField;
    tabComputerDBVersion: TStringField;
    tabLogEnterprise: TStringField;
    tabLogStore: TStringField;
    tabLogMachine_Number: TStringField;
    tabLogApplication: TStringField;
    tabLogCurrent_Ver_Maj: TIntegerField;
    tabLogCurrent_Ver_Min: TIntegerField;
    tabLogCurrent_Ver_Rel: TIntegerField;
    tabLogRunning_DB_Ver_Maj: TIntegerField;
    tabLogRunning_DB_Ver_Min: TIntegerField;
    tabLogRunning_DB_Ver_Rel: TIntegerField;
    tabLogDesc_Op: TStringField;
    tabLogDate_Op: TSQLTimeStampField;
    tabLogVersion: TStringField;
    tabLogDBVersion: TStringField;
    GridLogDBTableView1Version: TcxGridDBColumn;
    GridLogDBTableView1DBVersion: TcxGridDBColumn;
    tabComputerComputer_ID: TStringField;
    tabComputerExternal_IP: TStringField;
    tabComputerLocal_IP: TStringField;
    tabComputerComponent_Version: TStringField;
    tabLogComputer_ID: TStringField;
    tabLogExternal_IP: TStringField;
    tabLogLocal_IP: TStringField;
    tabLogComponent_Version: TStringField;
    GridLogDBTableView1Computer_ID: TcxGridDBColumn;
    GridLogDBTableView1External_IP: TcxGridDBColumn;
    GridLogDBTableView1Local_IP: TcxGridDBColumn;
    GridLogDBTableView1Component_Version: TcxGridDBColumn;
    actAppDelete: TAction;
    pnlComputerTop: TPanel;
    Splitter2: TSplitter;
    pnlComputerBottom: TPanel;
    Shape3: TShape;
    Shape8: TShape;
    GridComputer: TcxGrid;
    GridComputerDBTableView1: TcxGridDBTableView;
    GridComputerDBTableView1Computer_ID: TcxGridDBColumn;
    GridComputerDBTableView1Enterprise: TcxGridDBColumn;
    GridComputerDBTableView1Store: TcxGridDBColumn;
    GridComputerDBTableView1Machine_Number: TcxGridDBColumn;
    GridComputerDBTableView1Application: TcxGridDBColumn;
    GridComputerDBTableView1Version: TcxGridDBColumn;
    GridComputerDBTableView1DBVersion: TcxGridDBColumn;
    GridComputerDBTableView1Update_To_ID: TcxGridDBColumn;
    GridComputerDBTableView1Priority_ID: TcxGridDBColumn;
    GridComputerDBTableView1Idle_Time: TcxGridDBColumn;
    GridComputerDBTableView1Last_Op: TcxGridDBColumn;
    GridComputerDBTableView1Date_Op: TcxGridDBColumn;
    GridComputerDBTableView1External_IP: TcxGridDBColumn;
    GridComputerDBTableView1Local_IP: TcxGridDBColumn;
    GridComputerDBTableView1Component_Version: TcxGridDBColumn;
    GridComputerLevel1: TcxGridLevel;
    cxDBNavigator3: TcxDBNavigator;
    GridLogDetail: TcxGrid;
    GridLogDetailDBTableView1: TcxGridDBTableView;
    GridLogDetailDBTableView1Version: TcxGridDBColumn;
    GridLogDetailDBTableView1DBVersion: TcxGridDBColumn;
    GridLogDetailDBTableView1Desc_Op: TcxGridDBColumn;
    GridLogDetailDBTableView1Date_Op: TcxGridDBColumn;
    GridLogDetailLevel1: TcxGridLevel;
    pnlAppApp: TPanel;
    Shape9: TShape;
    GridApps: TcxGrid;
    GridAppsDBTableView1: TcxGridDBTableView;
    GridAppsDBTableView1Application: TcxGridDBColumn;
    GridAppsDBTableView1App_Directory: TcxGridDBColumn;
    GridAppsDBTableView1Update_Type_ID: TcxGridDBColumn;
    GridAppsDBTableView1Priority_ID: TcxGridDBColumn;
    GridAppsDBTableView1Idle_Time: TcxGridDBColumn;
    GridAppsDBTableView1TotalVersions: TcxGridDBColumn;
    GridAppsDBTableView1TotalDownloads: TcxGridDBColumn;
    GridAppsDBTableView1LastDownload: TcxGridDBColumn;
    GridAppsLevel1: TcxGridLevel;
    cxButton4: TcxButton;
    cxButton6: TcxButton;
    Splitter3: TSplitter;
    pnlAppBottom: TPanel;
    Shape10: TShape;
    cxButton3: TcxButton;
    cxButton5: TcxButton;
    pnlAppDetail: TPanel;
    GridUpdatesByAppMainDBTableView1: TcxGridDBTableView;
    GridUpdatesByAppMainLevel1: TcxGridLevel;
    GridUpdatesByAppMain: TcxGrid;
    qryUpdatesByAppMain: TFDQuery;
    qryUpdatesByAppMainLastest_Ver_Maj: TIntegerField;
    qryUpdatesByAppMainLastest_Ver_Min: TIntegerField;
    qryUpdatesByAppMainLastest_Ver_Rel: TIntegerField;
    qryUpdatesByAppMainMin_DB_Req_Maj: TIntegerField;
    qryUpdatesByAppMainMin_DB_Req_Min: TIntegerField;
    qryUpdatesByAppMainMin_DB_Req_Rel: TIntegerField;
    qryUpdatesByAppMainTargetFileName: TStringField;
    qryUpdatesByAppMainPath_To_Install: TStringField;
    qryUpdatesByAppMainUpdate_Priority_Desc: TStringField;
    qryUpdatesByAppMainIdle_Time: TStringField;
    qryUpdatesByAppMainInstaller_Desc: TStringField;
    qryUpdatesByAppMainActive: TBooleanField;
    qryUpdatesByAppMainTotal_Updates: TIntegerField;
    qryUpdatesByAppMainVersion: TStringField;
    qryUpdatesByAppMainDBVersion: TStringField;
    dsUpdatesByAppMain: TDataSource;
    GridUpdatesByAppMainDBTableView1TargetFileName: TcxGridDBColumn;
    GridUpdatesByAppMainDBTableView1Path_To_Install: TcxGridDBColumn;
    GridUpdatesByAppMainDBTableView1Update_Priority_Desc: TcxGridDBColumn;
    GridUpdatesByAppMainDBTableView1Idle_Time: TcxGridDBColumn;
    GridUpdatesByAppMainDBTableView1Installer_Desc: TcxGridDBColumn;
    GridUpdatesByAppMainDBTableView1Active: TcxGridDBColumn;
    GridUpdatesByAppMainDBTableView1Total_Updates: TcxGridDBColumn;
    GridUpdatesByAppMainDBTableView1Version: TcxGridDBColumn;
    GridUpdatesByAppMainDBTableView1DBVersion: TcxGridDBColumn;
    tabUpdatesEnterprise: TStringField;
    tabUpdatesStore: TStringField;
    tabUpdatesComputer_ID: TStringField;
    qryUpdatesByAppMainEnterprise: TStringField;
    qryUpdatesByAppMainStore: TStringField;
    qryUpdatesByAppMainComputer_ID: TStringField;
    GridUpdatesByAppMainDBTableView1Enterprise: TcxGridDBColumn;
    GridUpdatesByAppMainDBTableView1Store: TcxGridDBColumn;
    GridUpdatesByAppMainDBTableView1Computer_ID: TcxGridDBColumn;
    GridUpdatesDBTableView1Enterprise: TcxGridDBColumn;
    GridUpdatesDBTableView1Store: TcxGridDBColumn;
    GridUpdatesDBTableView1Computer_ID: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1Enterprise: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1Store: TcxGridDBColumn;
    GridUpdatesByAppDBTableView1Computer_ID: TcxGridDBColumn;
    tabComputerInstall_Ver: TStringField;
    GridComputerDBTableView1Install_Ver: TcxGridDBColumn;
    tabComputerGlobalUpdateVersion: TStringField;
    GridComputerDBTableView1GlobalUpdateVersion: TcxGridDBColumn;
    pagJustMajorUpdates: TTabSheet;
    Shape11: TShape;
    tabJustMajorUpdates: TFDTable;
    GridJustMajorUpdatesDBTableView1: TcxGridDBTableView;
    GridJustMajorUpdatesLevel1: TcxGridLevel;
    GridJustMajorUpdates: TcxGrid;
    dsJustMajorUpdates: TDataSource;
    GridJustMajorUpdatesDBTableView1Enterprise: TcxGridDBColumn;
    GridJustMajorUpdatesDBTableView1Store: TcxGridDBColumn;
    GridJustMajorUpdatesDBTableView1Application: TcxGridDBColumn;
    navJustMajorUpdates: TcxDBNavigator;
    AlerterMachineInsert: TFDEventAlerter;
    AlerterMachineUpdate: TFDEventAlerter;
    AlerterLog: TFDEventAlerter;
    AlerterMachineDelete: TFDEventAlerter;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actDBConnectExecute(Sender: TObject);
    procedure actDBDisconnectExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ConnAfterConnect(Sender: TObject);
    procedure ConnAfterDisconnect(Sender: TObject);
    procedure GridAppsDBTableView1Idle_TimePropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure TimerDtlLogTimer(Sender: TObject);
    procedure TimerLogTimer(Sender: TObject);
    procedure TimerMachineTimer(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject; var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure cbApplicationsPropertiesChange(Sender: TObject);
    procedure TimerAppTimer(Sender: TObject);
    procedure tabApplicationsCalcFields(DataSet: TDataSet);
    procedure actUploadExecute(Sender: TObject);
    procedure tabUpdatesCalcFields(DataSet: TDataSet);
    procedure actAppPostExecute(Sender: TObject);
    procedure actAppCancelExecute(Sender: TObject);
    procedure btnSelectFileClick(Sender: TObject);
    procedure tabComputerCalcFields(DataSet: TDataSet);
    procedure actAppDeleteExecute(Sender: TObject);
    procedure dsApplicationsDataChange(Sender: TObject; Field: TField);
    procedure qryUpdatesByAppMainCalcFields(DataSet: TDataSet);
    procedure GridComputerDBTableView1InitEdit(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit);
    procedure GridJustMajorUpdatesDBTableView1InitEdit(
      Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
      AEdit: TcxCustomEdit);
    procedure ConnBeforeDisconnect(Sender: TObject);
    procedure AlerterMachineInsertAlert(ASender: TFDCustomEventAlerter;
      const AEventName: string; const AArgument: Variant);
    procedure AlerterLogAlert(ASender: TFDCustomEventAlerter;
      const AEventName: string; const AArgument: Variant);
  private
    { Private declarations }
    FLoading:     boolean;
    FIniFileName: string;
    procedure LoadFromIniFile (FileName: string);
    procedure SaveToIniFile (FileName: string);
    procedure OpenConn;
    function DecodeHourMinSec (Data: string; var Hour, Min, Sec: integer): boolean;
    function ValidateIntervalFormat (s: string): boolean;
    procedure ExtractAppParams (var App, URL, WorkSpace, RepoSlug, UserName, Password: string);
  public
    { Public declarations }
    function AppIsRegistered (AppName: string): boolean;
    function NewQuery: TFDQuery;
  end;

var
  FMainAdmin: TFMainAdmin;

implementation

{$R *.dfm}

function GetAppVersionStr: string;
type
  TBytes = array of byte;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d',
    [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
     LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
     LongRec(FixedPtr.dwFileVersionLS).Hi,  //release
     LongRec(FixedPtr.dwFileVersionLS).Lo]) //build
end;

function TFMainAdmin.DecodeHourMinSec (Data: string; var Hour, Min, Sec: integer): boolean;
var
  s, tmp: string;
begin
  Hour   := 0;
  Min    := 0;
  Sec    := 0;
  tmp    := Data;
  result := Length (Data) in [5, 8];
  if not result then exit;

  //Extract seconds
  s      := Copy (tmp, Length (tmp) - 1, 2);
  result := TryStrToInt (s, Sec);

  if not result or not Math.InRange (Sec, 0, 59) then
  begin
    result := False;
    exit;
  end;

  Delete (tmp, Length (tmp) - 2, 3);

  //Extract minutes
  s      := Copy (tmp, Length (tmp) - 1, 2);
  result :=  TryStrToInt(s, Min);

  if not result or not Math.InRange (Min, 0, 59) then
  begin
    result := False;
    exit;
  end;

  if Length (tmp) = 2 then exit;

  //Extract hours
  s      := Copy (tmp, 1, 2);
  result := TryStrToInt(s, Hour);
  if not result or not Math.InRange (Hour, 0, 24) then
    result := False;
end;

procedure TFMainAdmin.dsApplicationsDataChange(Sender: TObject; Field: TField);
begin
  if Conn.Connected and (tabApplications.State = dsBrowse) then
  begin
    qryUpdatesByAppMain.Close;
    qryUpdatesByAppMain.ParamByName('App').AsString := tabApplications.FieldByName('Application').AsString;
    qryUpdatesByAppMain.Open;
  end;
end;

function TFMainAdmin.ValidateIntervalFormat (s: string): boolean;
var
  Hour, Min, Sec: integer;
begin
  result := DecodeHourMinSec(s, Hour, Min, Sec);
end;

procedure TFMainAdmin.actAppCancelExecute(Sender: TObject);
begin
  tabApplications.Cancel;
end;

procedure TFMainAdmin.actAppDeleteExecute(Sender: TObject);
var
  RootDir, AppDir, Msg, Git: string;
  Qry: TFDQuery;
begin
  if MessageDlg (Format ('Are you sure you want to delete the application "%s", its updates and all its history? This operation cannot be undone.', [tabApplications.FieldByName('Application').AsString]), mtConfirmation, mbYesNo, 0) <> mrYes then exit;

  FValidateDeleteDlg.Caption  := Format ('Confirm delete "%s"', [tabApplications.FieldByName('Application').AsString]);
  FValidateDeleteDlg.Password := '9999';
  if FValidateDeleteDlg.ShowModal = mrOK then
  begin
    RootDir := edRootDirectory.Text;
    if not RootDir.EndsWith ('\') then
      RootDir := RootDir + '\';

    AppDir := RootDir + tabApplications.FieldByName('App_Directory').AsString;

    //Recursively remove the directory
    TDirectory.Delete (AppDir, True);

    //Delete app info from tables
    Qry := TFDQuery.Create(Self);
    try
      Qry.Connection := Conn;
      Qry.Close;
      Qry.SQL.Clear;
      Qry.SQL.Add('Delete From Applications           Where Upper (Application) = :App;');
      Qry.SQL.Add('Delete From Updates                Where Upper (Application) = :App;');
      Qry.SQL.Add('Delete From Status_Machine_Updates Where Upper (Application) = :App;');
      Qry.SQL.Add('Delete From Log_Machine_Updates    Where Upper (Application) = :App;');
      Qry.SQL.Add('Delete From Just_Major_Updates     Where Upper (Application) = :App;');
      Qry.ParamByName('App').AsString := tabApplications.FieldByName('Application').AsString.ToUpper;
      Qry.ExecSQL;
    finally
      Qry.Free;
    end;

    //Performes push to git
    Msg := 'Delete App: ' + tabApplications.FieldByName('Application').AsString;
    Git := Format ('"%s\gitBatNoWait.bat %s"', [RootDir, Msg]);
    WinExecAndWait32('CMD', Format ('/c %s', [Git]), SW_HIDE);

    //Refresh data
    tabApplications.Refresh;
    tabUpdates.Refresh;
    tabComputer.Refresh;
    tabLog.Refresh;
    qryLog.Refresh;
    qryAppUpdateType.Refresh;
    qryUpdatesByApp.Refresh;
    qryCalcAppTotVersions.Refresh;
    qryCalcAppDownladsSummary.Refresh;

    MessageDlg ('Application deleted successfully!', mtInformation, [mbOK], 0);
  end;
end;

procedure TFMainAdmin.actAppPostExecute(Sender: TObject);
begin
  tabApplications.Post;
end;

procedure TFMainAdmin.actDBConnectExecute(Sender: TObject);
begin
  OpenConn;
end;

procedure TFMainAdmin.actDBDisconnectExecute(Sender: TObject);
begin
  Conn.Close;
end;

procedure TFMainAdmin.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  actDBConnect.Enabled    := not Conn.Connected and (edAlias.Text <> '') and (edUserName.Text <> '');
  actDBDisconnect.Enabled :=     Conn.Connected;
  actUpload.Enabled       := tabApplications.Active and tabUpdates.Active and (tabApplications.State = dsBrowse);
  actAppPost.Enabled      := tabApplications.State = dsEdit;
  actAppCancel.Enabled    := tabApplications.State = dsEdit;
  actAppDelete.Enabled    := (tabApplications.State = dsBrowse) and (tabApplications.RecordCount > 0);
end;

procedure TFMainAdmin.actUploadExecute(Sender: TObject);
begin
  FUploadDlg.ShowModal;
  tabApplications.Refresh;
  tabUpdates.Refresh;
end;

procedure TFMainAdmin.btnSelectFileClick(Sender: TObject);
begin
  SelectDirDlg.Directory := edRootDirectory.Text;
  if SelectDirDlg.Execute (Handle) then
    edRootDirectory.Text := SelectDirDlg.Directory;
end;

procedure TFMainAdmin.cbApplicationsPropertiesChange(Sender: TObject);
begin
  qryUpdatesByApp.Close;
  qryUpdatesByApp.SQL.Clear;
  qryUpdatesByApp.SQL.Add ('Select Application, URL_Download, TargetFileName, Release_Notes, Path_To_Install, Priority_ID, Idle_Time, Installer_Type, Active,');
  qryUpdatesByApp.SQL.Add ('       Lastest_Ver_Maj, Lastest_Ver_Min, Lastest_Ver_Rel,');
  qryUpdatesByApp.SQL.Add ('       Case When Lastest_Ver_Maj Is Null And Lastest_Ver_Min Is Null And Lastest_Ver_Rel Then ''''');
  qryUpdatesByApp.SQL.Add ('            Else Cast (Lastest_Ver_Maj As SQL_Varchar) + ''.'' + Cast (Lastest_Ver_Min As SQL_Varchar) + ''.'' + Cast (Lastest_Ver_Rel As SQL_Varchar)');
  qryUpdatesByApp.SQL.Add ('       End Version,');
  qryUpdatesByApp.SQL.Add ('       Case When Min_DB_Req_Maj Is Null And Min_DB_Req_Min Is Null And Min_DB_Req_Rel Then ''''');
  qryUpdatesByApp.SQL.Add ('            Else Cast (Min_DB_Req_Maj As SQL_Varchar) + ''.'' + Cast (Min_DB_Req_Min As SQL_Varchar) + ''.'' + Cast (Min_DB_Req_Rel As SQL_Varchar)');
  qryUpdatesByApp.SQL.Add ('       End DBVersion,');
  qryUpdatesByApp.SQL.Add ('       Enterprise, Store, Computer_ID');
  qryUpdatesByApp.SQL.Add ('  From Updates');

  if cbApplications.Text <> '' then
  begin
    qryUpdatesByApp.SQL.Add (' Where Upper (Application) = Upper (:App)');
    qryUpdatesByApp.ParamByName('App').AsString := cbApplications.Text;
  end;

  qryUpdatesByApp.SQL.Add ('Order By Lastest_Ver_Maj, Lastest_Ver_Min, Lastest_Ver_Rel');
  qryUpdatesByApp.Open;
end;

procedure TFMainAdmin.ConnAfterConnect(Sender: TObject);
begin
  StatusBar1.Panels [1].Text := 'Connected';
end;

procedure TFMainAdmin.ConnAfterDisconnect(Sender: TObject);
begin
  StatusBar1.Panels [1].Text := 'Disconnected';
end;

procedure TFMainAdmin.ConnBeforeDisconnect(Sender: TObject);
begin
  AlerterMachineInsert.Active := False;
  AlerterMachineUpdate.Active := False;
  AlerterMachineDelete.Active := False;
  AlerterLog.Active           := False;
end;

procedure TFMainAdmin.ExtractAppParams (var App, URL, WorkSpace, RepoSlug, UserName, Password: string);
var
  bm:    TBookmark;
  Found: boolean;
  A:     TArray <string>;
begin
  App       := '';
  URL       := '';
  WorkSpace := '';
  RepoSlug  := '';
  UserName  := '';
  Password  := '';

  if    not tabUpdates.Active
     or not tabApplications.Active
     or (tabUpdates.RecordCount = 0)
     or (tabApplications.RecordCount = 0)
     or (tabUpdates.FieldByName('Application').AsString = '') then exit;

  App := UpperCase (tabUpdates.FieldByName('Application').AsString);
  bm  := tabApplications.GetBookmark;
  tabApplications.DisableControls;
  try
    Found := False;
    tabApplications.First;
    while not tabApplications.Eof and not Found do
    begin
      if UpperCase (tabApplications.FieldByName ('Application').AsString) = App then
      begin
        Found    := True;
        URL      := tabApplications.FieldByName ('Repository_URL').AsString;
        UserName := tabApplications.FieldByName ('Repository_User').AsString;
        Password := tabApplications.FieldByName ('Repository_Pass').AsString;
      end;

      tabApplications.Next;
    end;

    if not Found then
    begin
      App := '';
      exit;
    end;

    if URL.EndsWith ('/') then
      Delete (URL, Length (URL), 1);

    A := URL.Split(['/']);
    if Length (A) > 2 then
    begin
      WorkSpace := A [Length (A) - 2];
      RepoSlug  := A [Length (A) - 1];
    end;
  finally
    tabApplications.GotoBookmark(bm);
    tabApplications.FreeBookmark(bm);
    tabApplications.EnableControls;
  end;
end;

procedure TFMainAdmin.AlerterLogAlert(ASender: TFDCustomEventAlerter;
  const AEventName: string; const AArgument: Variant);
begin
  TimerLog.Interval := 10;
  TimerLog.Enabled  := True;
end;

procedure TFMainAdmin.AlerterMachineInsertAlert(ASender: TFDCustomEventAlerter;
  const AEventName: string; const AArgument: Variant);
begin
  TimerMachine.Interval := 10;
  TimerMachine.Enabled  := True;
end;

function TFMainAdmin.AppIsRegistered (AppName: string): boolean;
var
  bm: TBookmark;
begin
  result := False;
  if not Conn.Connected or not tabApplications.Active or (tabApplications.State <> dsBrowse) or
     (tabApplications.RecordCount = 0) then exit;

  bm := tabApplications.GetBookmark;
  tabApplications.DisableControls;
  try
    AppName := UpperCase (AppName);
    tabApplications.First;
    while not tabApplications.Eof do
    begin
      if UpperCase (tabApplications.FieldByName('Application').AsString) = AppName then
      begin
        result := True;
        exit;
      end;

      tabApplications.Next;
    end;
  finally
    tabApplications.GotoBookmark(bm);
    tabApplications.FreeBookmark(bm);
    tabApplications.EnableControls;
  end;
end;

procedure TFMainAdmin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveToIniFile(FIniFileName);
  Conn.Close;
end;

procedure TFMainAdmin.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (tabApplications.State     in [dsEdit, dsInsert]) or
     (tabUpdates.State          in [dsEdit, dsInsert]) or
     (tabComputer.State         in [dsEdit, dsInsert]) or
     (tabJustMajorUpdates.State in [dsEdit, dsInsert]) then
  begin
    MessageDlg ('You must save changes before close application', mtInformation, [mbOK], 0);
    CanClose := False;
  end;
end;

procedure TFMainAdmin.FormCreate(Sender: TObject);
begin
  FLoading     := False;
  FIniFileName := ExtractFilePath (ParamStr (0)) + TPath.GetFileNameWithoutExtension(ParamStr (0)) + '.ini';
end;

procedure TFMainAdmin.FormShow(Sender: TObject);
begin
  StatusBar1.Panels [PNL_App_Version].Text := 'Version: ' + GetAppVersionStr;
  PageControl1.ActivePage := pagParamsApps;
  if FileExists (FIniFileName) then
  begin
    LoadFromIniFile(FIniFileName);
    OpenConn;

    if Conn.Connected then
      cbApplicationsPropertiesChange(cbApplications);
  end;
end;

procedure TFMainAdmin.GridAppsDBTableView1Idle_TimePropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  if (DisplayValue <> '') and not ValidateIntervalFormat (DisplayValue) then
  begin
    Error     := True;
    ErrorText := ErrMsgInvalidFormat;
  end;
end;

procedure TFMainAdmin.GridComputerDBTableView1InitEdit(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit);
var
  Qry: TFDQuery;
begin
  if (AEdit is TcxComboBox) and (UpperCase (TcxGridDBColumn (AItem).DataBinding.Field.FieldName) = 'INSTALL_VER') then
  begin
    Qry := NewQuery;
    try
      Qry.Close;
      Qry.SQL.Clear;
      Qry.SQL.Add('Select Case When Lastest_Ver_Maj Is Null And Lastest_Ver_Min Is Null And Lastest_Ver_Rel Is Null Then ''''');
      Qry.SQL.Add('            Else Cast (Lastest_Ver_Maj As SQL_Varchar) + ''.'' + Cast (Lastest_Ver_Min As SQL_Varchar) + ''.'' + Cast (Lastest_Ver_Rel As SQL_Varchar)');
      Qry.SQL.Add('       End Version');
      Qry.SQL.Add('  From Updates');
      Qry.SQL.Add(' Where Upper (Application) = Upper (:App)');
      Qry.ParamByName('App').AsString := tabComputer.FieldByName('Application').AsString;
      Qry.Open;

      TcxComboBox(AEdit).Clear;
      while not Qry.Eof do
      begin
        TcxComboBox(AEdit).Properties.Items.Add (Qry.FieldByName('Version').AsString);

        Qry.Next;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFMainAdmin.GridJustMajorUpdatesDBTableView1InitEdit(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit);
var
  ColumnName: string;
  Query: TFDQuery;
begin
  ColumnName := UpperCase (TcxGridDBColumn (AItem).DataBinding.Field.FieldName);
  if AEdit is TcxComboBox then
  begin
    Query := NewQuery;
    try
      Query.Close;
      Query.SQL.Clear;
      if ColumnName = 'APPLICATION' then
      begin
        Query.SQL.Add('Select Distinct Application');
        Query.SQL.Add('  From Status_Machine_Updates');
        Query.Open;
      end
      else if ColumnName = 'ENTERPRISE' then
      begin
        Query.SQL.Add('Select Distinct Enterprise');
        Query.SQL.Add('  From Status_Machine_Updates');
        Query.SQL.Add(' Where Upper (Application) = Upper (:App)');
        Query.ParamByName('App').AsString := tabJustMajorUpdates.FieldByName('Application').AsString;
        Query.Open;
      end
      else if ColumnName = 'STORE' then
      begin
        Query.SQL.Add('Select Distinct Store');
        Query.SQL.Add('  From Status_Machine_Updates');
        Query.SQL.Add(' Where Upper (Application) = Upper (:App)');
        Query.SQL.Add('   And Upper (Enterprise)  = Upper (:Enterprise)');
        Query.ParamByName('App').AsString        := tabJustMajorUpdates.FieldByName('Application').AsString;
        Query.ParamByName('Enterprise').AsString := tabJustMajorUpdates.FieldByName('Enterprise').AsString;
        Query.Open;
      end;

      TcxComboBox(AEdit).Clear;
      while Query.Active and not Query.Eof do
      begin
        TcxComboBox(AEdit).Properties.Items.Add (Query.FieldByName(ColumnName).AsString);

        Query.Next;
      end;
    finally
      Query.Free;
    end;
  end;
end;

procedure TFMainAdmin.LoadFromIniFile (FileName: string);
var
  Ini: TIniFile;
  s:   string;
begin
  Ini      := TIniFile.Create(FileName);
  FLoading := True;
  try
    //Connection parameters
    edAlias.Text    := Ini.ReadString('Connection', 'Alias',      '');
    edUserName.Text := Ini.ReadString('Connection', 'UserName',   '');
    edPassword.Text := Ini.ReadString('Connection', 'Password',   '');
    s               := Ini.ReadString('Connection', 'ServerType', 'Local');

    if s = 'Local' then
      cbServerType.ItemIndex := 0
    else if s = 'Remote' then
      cbServerType.ItemIndex := 1
    else if s = 'Internet' then
      cbServerType.ItemIndex := 2;

    //Repository parameters
    edRepositoryURL.Text := Ini.ReadString('Repository', 'BaseURL',    '');

    //Global parameters
    edRootDirectory.Text := Ini.ReadString('General',    'RootAppDir', '');
  finally
    Ini.Free;
    FLoading := False;
  end;
end;

procedure TFMainAdmin.SaveToIniFile (FileName: string);
var
  Ini: TIniFile;
  s:   string;
begin
  Ini := TIniFile.Create(FileName);
  try
    //Connection parameters
    s := 'Local';
    case cbServerType.ItemIndex of
      0: s := 'Local';
      1: s := 'Remote';
      2: s := 'Internet';
    end;

    Ini.WriteString('Connection', 'Alias',      edAlias.Text);
    Ini.WriteString('Connection', 'UserName',   edUserName.Text);
    Ini.WriteString('Connection', 'Password',   edPassword.Text);
    Ini.WriteString('Connection', 'ServerType', s);

    //Repository parameters
    Ini.WriteString('Repository', 'BaseURL',    edRepositoryURL.Text);

    //Global parameters
    Ini.WriteString('General',    'RootAppDir', edRootDirectory.Text);
  finally
    Ini.Free;
  end;
end;

procedure TFMainAdmin.tabApplicationsCalcFields(DataSet: TDataSet);
begin
  qryCalcAppTotVersions.Close;
  qryCalcAppTotVersions.ParamByName('App').AsString := tabApplicationsApplication.AsString;
  qryCalcAppTotVersions.Open;
  tabApplicationsTotalVersions.AsInteger := qryCalcAppTotVersions.FieldByName('Total').AsInteger;

  qryCalcAppDownladsSummary.Close;
  qryCalcAppDownladsSummary.ParamByName('App').AsString := tabApplicationsApplication.AsString;
  qryCalcAppDownladsSummary.Open;
  tabApplicationsTotalDownloads.AsInteger := qryCalcAppDownladsSummary.FieldByName ('Total').AsInteger;
  tabApplicationsLastDownload.AsDateTime  := qryCalcAppDownladsSummary.FieldByName ('Last_Op').AsDateTime
end;

procedure TFMainAdmin.tabComputerCalcFields(DataSet: TDataSet);
var
  s, VerInst, VerNew: string;
  Qry: TFDQuery;
begin
  s := DataSet.FieldByName('Current_Ver_Maj').AsString + '.' +
       DataSet.FieldByName('Current_Ver_Min').AsString + '.' +
       DataSet.FieldByName('Current_Ver_Rel').AsString;

  if s = '..' then
    s := '';

  DataSet.FieldByName('Version').AsString := s;

  s := DataSet.FieldByName('Running_DB_Ver_Maj').AsString + '.' +
       DataSet.FieldByName('Running_DB_Ver_Min').AsString + '.' +
       DataSet.FieldByName('Running_DB_Ver_Rel').AsString;

  if s = '..' then
    s := '';

  DataSet.FieldByName('DBVersion').AsString := s;

  if DataSet = tabComputer then
  begin
    s   := '';
    Qry := NewQuery;
    try
      Qry.Close;
      Qry.SQL.Clear;
      Qry.SQL.Add('Select C.Enterprise, C.Store, C.Machine_Number, C.Computer_ID, C.Application, U.Lastest_Ver_Maj, U.Lastest_Ver_Min, U.Lastest_Ver_Rel');
      Qry.SQL.Add('  From            Status_Machine_Updates C');
      Qry.SQL.Add('  Left Outer Join Updates U On Upper (C.Application) = Upper (U.Application)');
      Qry.SQL.Add('                               And ((     U.Enterprise  Is Not Null');
      Qry.SQL.Add('                                      And U.Store       Is Null');
      Qry.SQL.Add('                                      And (   U.Computer_ID Is Null And C.Computer_ID Is Null');
      Qry.SQL.Add('                                           Or');
      Qry.SQL.Add('                                              U.Computer_ID Is Not Null And Upper (U.Computer_ID) = Upper (C.Computer_ID)');
      Qry.SQL.Add('                                          )');
      Qry.SQL.Add('                                      And Upper (C.Enterprise) = Upper (U.Enterprise)');
      Qry.SQL.Add('                                    )');
      Qry.SQL.Add('                                    Or');
      Qry.SQL.Add('                                    (     U.Enterprise Is Not Null');
      Qry.SQL.Add('                                      And U.Store      Is Not Null');
      Qry.SQL.Add('                                      And (   U.Computer_ID Is Null And C.Computer_ID Is Null');
      Qry.SQL.Add('                                           Or');
      Qry.SQL.Add('                                              U.Computer_ID Is Not Null And Upper (U.Computer_ID) = Upper (C.Computer_ID)');
      Qry.SQL.Add('                                          )');
      Qry.SQL.Add('                                      And Upper (C.Enterprise) = Upper (U.Enterprise)');
      Qry.SQL.Add('                                      And Upper (C.Store)      = Upper (U.Store)');
      Qry.SQL.Add('                                    )');
      Qry.SQL.Add('                                    Or');
      Qry.SQL.Add('                                    (    U.Enterprise Is Null');
      Qry.SQL.Add('                                     And U.Store      Is Null');
      Qry.SQL.Add('                                     And Upper (U.Computer_ID) = Upper (C.Computer_ID)');
      Qry.SQL.Add('                                    )');
      Qry.SQL.Add('                                   )');
      Qry.SQL.Add('  Where (    Upper (C.Enterprise)     = Upper (:Enterprise)');
      Qry.SQL.Add('         And Upper (C.Store)          = Upper (:Store)');
      Qry.SQL.Add('         And Upper (C.Machine_Number) = Upper (:Machine_Number)');
      Qry.SQL.Add('         And Upper (C.Application)    = Upper (:App))');
      Qry.SQL.Add('     Or Upper (C.Computer_ID) = Upper (:Computer_ID)');
      Qry.ParamByName('Enterprise').AsString     := DataSet.FieldByName('Enterprise').AsString;
      Qry.ParamByName('Store').AsString          := DataSet.FieldByName('Store').AsString;
      Qry.ParamByName('Machine_Number').AsString := DataSet.FieldByName('Machine_Number').AsString;
      Qry.ParamByName('App').AsString            := DataSet.FieldByName('Application').AsString;
      Qry.ParamByName('Computer_ID').AsString    := DataSet.FieldByName('Computer_ID').AsString;
      Qry.Open;

      if not Qry.Eof and not Qry.FieldByName ('Lastest_Ver_Maj').IsNull and
         not Qry.FieldByName ('Lastest_Ver_Min').IsNull                 and
         not Qry.FieldByName ('Lastest_Ver_Rel').IsNull then
      begin
        VerInst := DataSet.FieldByName('Version').AsString;
        VerNew  := Format ('%s.%s.%s', [Qry.FieldByName ('Lastest_Ver_Maj').AsString, Qry.FieldByName ('Lastest_Ver_Min').AsString, Qry.FieldByName ('Lastest_Ver_Rel').AsString]);
        if CompareVersion (VerInst, VerNew) = coLower then
          s := VerNew;
      end;
    finally
      Qry.Free;
      DataSet.FieldByName('GlobalUpdateVersion').AsString := s;
    end;
  end;
end;

procedure TFMainAdmin.tabUpdatesCalcFields(DataSet: TDataSet);
var
  s: string;
begin
  s := DataSet.FieldByName('Lastest_Ver_Maj').AsString + '.' +
       DataSet.FieldByName('Lastest_Ver_Min').AsString + '.' +
       DataSet.FieldByName('Lastest_Ver_Rel').AsString;

  if s = '..' then
    s := '';

  DataSet.FieldByName('Version').AsString := s;

  s := DataSet.FieldByName('Min_DB_Req_Maj').AsString + '.' +
       DataSet.FieldByName('Min_DB_Req_Min').AsString + '.' +
       DataSet.FieldByName('Min_DB_Req_Rel').AsString;

  if s = '..' then
    s := '';

  DataSet.FieldByName('DBVersion').AsString := s
end;

procedure TFMainAdmin.TimerAppTimer(Sender: TObject);
begin
  if Conn.Connected and tabApplications.Active and (tabApplications.State = dsBrowse) then
  begin
    tabApplications.Refresh;
    qryUpdatesByAppMain.Close;
    qryUpdatesByAppMain.ParamByName('App').AsString := tabApplications.FieldByName('Application').AsString;
    qryUpdatesByAppMain.Open;
  end;
end;

procedure TFMainAdmin.TimerDtlLogTimer(Sender: TObject);
begin
  if Conn.Connected and qryLog.Active then
    qryLog.Refresh;
end;

procedure TFMainAdmin.TimerLogTimer(Sender: TObject);
begin
  TimerLog.Enabled := False;
  if Conn.Connected and tabLog.Active then
    tabLog.Refresh;
end;

procedure TFMainAdmin.TimerMachineTimer(Sender: TObject);
begin
  TimerMachine.Enabled := False;
  if Conn.Connected and tabComputer.Active and (tabComputer.State = dsBrowse) then
  begin
    tabComputer.Refresh;
    qryLog.Refresh;
  end;
end;

procedure TFMainAdmin.OpenConn;
begin
  if (edAlias.Text = '') or (edUserName.Text = '') then exit;

  Conn.Close;
  Conn.Params.Clear;
  Conn.Params.Add('DriverID=ADS');
  Conn.Params.Add('TableType=ADT');
  Conn.Params.Add('Alias=' + edAlias.Text);
    case cbServerType.ItemIndex of
      0: Conn.Params.Add('ServerTypes=Local');
      1: Conn.Params.Add('ServerTypes=Remote');
      2: Conn.Params.Add('ServerTypes=Internet');
    end;
  Conn.Params.UserName := edUserName.Text;
  Conn.Params.Password := edPassword.Text;
  TimerMachine.Enabled := False;
  TimerLog.Enabled     := False;

  qryAppUpdateType.DisableControls;
  tabUpdateType.DisableControls;
  tabPriority.DisableControls;
  tabInstallers.DisableControls;
  tabApplications.DisableControls;
  tabUpdates.DisableControls;
  tabComputer.DisableControls;
  tabLog.DisableControls;
  qryLog.DisableControls;
  tabJustMajorUpdates.DisableControls;

  try
    try
      Conn.Connected := True;
    except
    end;
  finally
    qryAppUpdateType.EnableControls;
    tabUpdateType.EnableControls;
    tabPriority.EnableControls;
    tabInstallers.EnableControls;
    tabApplications.EnableControls;
    tabUpdates.EnableControls;
    tabComputer.EnableControls;
    tabLog.EnableControls;
    qryLog.EnableControls;
    tabJustMajorUpdates.EnableControls;
  end;

  if Conn.Connected then
  begin
    qryAppUpdateType.Open;
    tabUpdateType.Open;
    tabPriority.Open;
    tabInstallers.Open;
    tabApplications.Open;
    tabUpdates.Open;
    tabComputer.Open;
    tabLog.Open;
    qryLog.Open;
    tabJustMajorUpdates.Open;
    AlerterMachineInsert.Active := True;
    AlerterMachineUpdate.Active := True;
    AlerterMachineDelete.Active := True;
    AlerterLog.Active           := True;
  end;
end;

procedure TFMainAdmin.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  AllowChange := ((PageControl1.ActivePage = pagParamsApps)       and not (tabApplications.State     in [dsEdit, dsInsert])) or
                 ((PageControl1.ActivePage = pagUpdates)          and not (tabUpdates.State          in [dsEdit, dsInsert])) or
                 ((PageControl1.ActivePage = pagComputerStatus)   and not (tabUpdates.State          in [dsEdit, dsInsert])) or
                 ((PageControl1.ActivePage = pagJustMajorUpdates) and not (tabJustMajorUpdates.State in [dsEdit, dsInsert])) or
                  (PageControl1.ActivePage = pagLog)                                                                   or
                  (PageControl1.ActivePage = pagSetup);

  if not AllowChange then
    MessageDlg ('You must save the changes before changing tabs', mtInformation, [mbOK], 0);
end;

procedure TFMainAdmin.qryUpdatesByAppMainCalcFields(DataSet: TDataSet);
begin
  qryUpdatesByAppMainVersion.AsString   := Format ('%d.%d.%d', [qryUpdatesByAppMainLastest_Ver_Maj.AsInteger, qryUpdatesByAppMainLastest_Ver_Min.AsInteger, qryUpdatesByAppMainLastest_Ver_Rel.AsInteger]);
  qryUpdatesByAppMainDBVersion.AsString := Format ('%d.%d.%d', [qryUpdatesByAppMainMin_DB_Req_Maj.AsInteger,  qryUpdatesByAppMainMin_DB_Req_Min.AsInteger,  qryUpdatesByAppMainMin_DB_Req_Rel.AsInteger]);
end;

function TFMainAdmin.NewQuery: TFDQuery;
begin
  result                := TFDQuery.Create(Self);
  result.ConnectionName := Conn.ConnectionName;
end;


end.
