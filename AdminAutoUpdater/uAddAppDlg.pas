unit uAddAppDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, cxTextEdit, cxDBEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Vcl.Menus, cxButtons, DB,
  System.IOUtils, uProgramInfo;

type
  TFAddAppDlg = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    edProgramName: TcxDBTextEdit;
    edAppDirectory: TcxDBTextEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cbUpdateType: TcxDBLookupComboBox;
    cbPriority: TcxDBLookupComboBox;
    edIdleTime: TcxDBTextEdit;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    btnSelectFile: TcxButton;
    OpenDlg: TOpenDialog;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSelectFileClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FileVer:  string;
    FileName: string;
  end;

var
  FAddAppDlg: TFAddAppDlg;

implementation
uses uMainAdmin, uUploadDlg;

{$R *.dfm}

procedure TFAddAppDlg.btnOKClick(Sender: TObject);
var
  AppDir:   string;
  FileInfo: TCEVersionInfo;
  OK:       boolean;
  Maj,
  Min,
  Rel:      integer;
begin
  FileVer := '';
  if FMainAdmin.tabApplications.FieldByName ('Application').IsNull then
  begin
    MessageDlg ('Program name is required', mtInformation, [mbOK], 0);
    edProgramName.SetFocus;
    exit;
  end;

  if FMainAdmin.tabApplications.FieldByName ('App_Directory').IsNull then
  begin
    MessageDlg ('Repository app directory is required', mtInformation, [mbOK], 0);
    edAppDirectory.SetFocus;
    exit;
  end;

  FMainAdmin.tabApplications.Post;

  AppDir := FMainAdmin.edRootDirectory.Text;
  if not AppDir.EndsWith ('\') then
    AppDir := AppDir + '\';

  AppDir := AppDir + FMainAdmin.tabApplications.FieldByName ('App_Directory').AsString;
  ForceDirectories(AppDir);

  if not FileName.IsEmpty then
  begin
    FileInfo := TCEVersionInfo.Create(Self);
    FileVer  := '';
    try
      //Get file version
      FileInfo.GetInfo(FileName);
      OK := TryStrToInt(FileInfo.MajorVersion, Maj) and
            TryStrToInt(FileInfo.MinorVersion, Min) and
            TryStrToInt(FileInfo.Release,      Rel);

      if OK then
      begin
        FileVer := Format ('%s.%s.%s', [FileInfo.MajorVersion, FileInfo.MinorVersion, FileInfo.Release]);
        FUploadDlg.tabUpdates.Append;
        FUploadDlg.tabUpdates.FieldByName('Application').AsString      := FMainAdmin.tabApplications.FieldByName ('Application').AsString;
        FUploadDlg.tabUpdates.FieldByName('Lastest_Ver_Maj').AsInteger := Maj;
        FUploadDlg.tabUpdates.FieldByName('Lastest_Ver_Min').AsInteger := Min;
        FUploadDlg.tabUpdates.FieldByName('Lastest_Ver_Rel').AsInteger := Rel;
        FUploadDlg.tabUpdates.FieldByName('TargetFileName').AsString   := ExtractFileName(FileName);
        FUploadDlg.tabUpdates.FieldByName('Installer_Type').AsInteger  := 1; // Replacement
        FUploadDlg.tabUpdates.FieldByName('Active').AsBoolean          := True;
        FUploadDlg.tabUpdates.Post;
        FUploadDlg.cbApplicationsPropertiesChange(Self);
      end;
    finally
      FileInfo.Free;
    end;
  end;

  ModalResult := mrOK;
end;

procedure TFAddAppDlg.btnSelectFileClick(Sender: TObject);
var
  FN: string;
begin
  if OpenDlg.Execute (Handle) then
  begin
    FileName := OpenDlg.FileName;
    FN       := ExtractFileName (FileName);
    FMainAdmin.tabApplications.FieldByName('Application').AsString   := FN;
    FMainAdmin.tabApplications.FieldByName('App_Directory').AsString := TPath.GetFileNameWithoutExtension (FN);
  end;
end;

procedure TFAddAppDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if FMainAdmin.tabApplications.State in [dsEdit, dsInsert] then
    FMainAdmin.tabApplications.Cancel;
end;

procedure TFAddAppDlg.FormShow(Sender: TObject);
begin
  FileName := '';
  FileVer  := '';
  FMainAdmin.tabApplications.Append;
end;

end.
