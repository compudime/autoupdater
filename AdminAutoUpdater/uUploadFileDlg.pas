unit uUploadFileDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  Vcl.StdCtrls, cxButtons, WinAPI.URLMon, Vcl.ExtCtrls, System.IOUtils,
  cxControls, cxContainer, cxEdit, cxImage, System.ImageList, Vcl.ImgList,
  JvExControls, JvBmpAnimator, JvAnimatedImage;

type
  TUploadState = (usNone, usUploading, usChecking);
  TFUploadFile = class;
  TUploadThread = class (TThread)
  private
    FUploadDlg:  TFUploadFile;
    FParams:     string;
    FFinalURL:   string;
    FSuccessful: boolean;
    FState:      TUploadState;
    procedure NotifyStatus;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Execute; override;
    property Params: string read FParams write FParams;
    property FinalURL: string read FFinalURL write FFinalURL;
    property State: TUploadState read FState;
    property Successful: boolean read FSuccessful;
  end;

  TFUploadFile = class(TForm)
    Label1: TLabel;
    edFileName: TEdit;
    btnSelectFile: TcxButton;
    Label2: TLabel;
    edTargetFileName: TEdit;
    btnUploadFile: TcxButton;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    Label3: TLabel;
    edURLFile: TEdit;
    OpenDlg: TOpenDialog;
    Timer1: TTimer;
    labStatus: TLabel;
    ImageList1: TImageList;
    Animator: TJvBmpAnimator;
    procedure btnSelectFileClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnUploadFileClick(Sender: TObject);
    procedure WebBrowser1FileDownload(ASender: TObject;
      ActiveDocument: WordBool; var Cancel: WordBool);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    UploadThread:     TUploadThread;
    ThreadTerminated: boolean;
    procedure OnThreadTerminated (Sender: TObject);
  public
    { Public declarations }
    URL:       string;
    UserName:  string;
    Password:  string;
    WorkSpace: string;
    RepoSlug:  string;
  end;

var
  FUploadFile: TFUploadFile;

implementation
uses uMainAdmin, WinInet;

{$R *.dfm}

{Returns -1 if the Exec failed, otherwise returns the process' exit code when the process terminates }
function WinExecAndWait32 (AFileName, AParameters: string; Visibility: integer): DWord;
var
  zCommandLine: array[0..512] of char;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  StrPCopy(zCommandLine, AFileName + ' ' + AParameters);
  FillChar(StartupInfo, SizeOf(StartupInfo), #0);
  StartupInfo.cb          := Sizeof(StartupInfo);
  StartupInfo.dwFlags     := STARTF_USESHOWWINDOW;
  StartupInfo.wShowWindow := Visibility;

  if not CreateProcess(nil,
                       zCommandLine, { pointer to command line string }
                       nil, { pointer to process security attributes }
                       nil, { pointer to thread security attributes }
                       False, { handle inheritance flag }
                       CREATE_NEW_CONSOLE or { creation flags }
                       NORMAL_PRIORITY_CLASS,
                       nil, { pointer to new environment block }
                       nil, { pointer to current directory name }
                       StartupInfo, { pointer to STARTUPINFO }
                       ProcessInfo
                      ) then { pointer to PROCESS_INF }
    result := DWord(-1)
  else
  begin
    WaitforSingleObject(ProcessInfo.hProcess, INFINITE);
    GetExitCodeProcess(ProcessInfo.hProcess, result);
    CloseHandle(ProcessInfo.hProcess);
    CloseHandle(ProcessInfo.hThread);
  end;
end;

constructor TUploadThread.Create;
begin
  inherited Create;

  FUploadDlg   := nil;
  FParams      := '';
  FFinalURL    := '';
  FSuccessful  := False;
  FState       := usNone;
end;

destructor TUploadThread.Destroy;
begin

  inherited Destroy;
end;

procedure TUploadThread.Execute;
var
  tmp: string;
begin
  FState      := usUploading;
  FSuccessful := False;
  tmp         := System.IOUtils.TPath.GetTempFileName;
  Synchronize(NotifyStatus);
  WinExecAndWait32 ('Curl', Params, SW_HIDE);

  FState := usChecking;
  Synchronize(NotifyStatus);
  if URLDownloadToFile (nil, PChar (FinalURL), PChar (tmp), 0, nil) = 0 then
  begin
    DeleteFile (tmp);
    FSuccessful := True;
  end;
end;

procedure TUploadThread.NotifyStatus;
begin
  case FState of
    usNone:      FUploadDlg.labStatus.Caption := '';
    usUploading: FUploadDlg.labStatus.Caption := 'Uploading';
    usChecking:  FUploadDlg.labStatus.Caption := 'Checking';
  end;
end;

procedure TFUploadFile.btnSelectFileClick(Sender: TObject);
begin
  if OpenDlg.Execute (Handle) then
  begin
    edFileName.Text := OpenDlg.FileName;
    edURLFile.Text  := '';

    if edTargetFileName.Text = '' then
      edTargetFileName.Text := ExtractFileName(edFileName.Text);
  end;
end;

procedure TFUploadFile.btnUploadFileClick(Sender: TObject);
var
  API_URL:  string;
  Params:   string;
  FinalURL: string;
begin
  if edFileName.Text = '' then
  begin
    MessageDlg ('File name is required', mtError, [mbOK], 0);
    exit;
  end;

  if not FileExists (edFileName.Text) then
  begin
    MessageDlg (Format ('File [%s] does not exists', [edFileName.Text]), mtError, [mbOK], 0);
    exit;
  end;

  API_URL := Format ('https://api.bitbucket.org/2.0/repositories/%s/%s/downloads/', [WorkSpace, RepoSlug]);
  Params  := 'curl -s -u ' + UserName;

  if not Password.IsEmpty then
    Params := Params + ':' + Password;

  Params                       := Format ('%s -X POST %s -F files=@"%s"', [Params, API_URL, edFileName.Text]);
  FinalURL                     := URL + '/downloads/' + ExtractFileName (edFileName.Text);
  labStatus.Caption            := '';
  btnSelectFile.Enabled        := False;
  btnUploadFile.Enabled        := False;
  edFileName.ReadOnly          := True;
  Animator.Images              := ImageList1;
  Animator.Active              := True;
  UploadThread                 := TUploadThread.Create;
  UploadThread.FUploadDlg      := Self;
  UploadThread.FParams         := Params;
  UploadThread.FFinalURL       := FinalURL;
  UploadThread.OnTerminate     := OnThreadTerminated;
  UploadThread.FreeOnTerminate := False;
  UploadThread.Priority        := tpHighest;
  UploadThread.Resume;
  Timer1.Enabled := True;
end;

procedure TFUploadFile.OnThreadTerminated (Sender: TObject);
begin
  ThreadTerminated := True;
end;

procedure TFUploadFile.FormCreate(Sender: TObject);
begin
  UploadThread     := nil;
  ThreadTerminated := False;
end;

procedure TFUploadFile.FormShow(Sender: TObject);
begin
  btnOK.Enabled         := False;
  edFileName.Text       := '';
  edTargetFileName.Text := '';
  edURLFile.Text        := '';
end;

procedure TFUploadFile.Timer1Timer(Sender: TObject);
begin
  if ThreadTerminated then
  begin
    Timer1.Enabled        := False;
    labStatus.Caption     := '';
    btnSelectFile.Enabled := True;
    btnUploadFile.Enabled := True;
    edFileName.ReadOnly   := False;
    btnOK.Enabled         := True;
    edURLFile.Text        := UploadThread.FinalURL;
    Animator.Active       := False;
    Animator.Images       := nil;

    if UploadThread.Successful then
      MessageDlg ('File upload successful!', mtInformation, [mbOK], 0)
    else
      MessageDlg ('File can''t be uploaded, check your internet connection and try again later', mtInformation, [mbOK], 0);

    FreeAndNil (UploadThread);
  end;
end;

procedure TFUploadFile.WebBrowser1FileDownload(ASender: TObject;
  ActiveDocument: WordBool; var Cancel: WordBool);
begin
  Cancel := True;
  MessageDlg ('File found', mtInformation, [mbOK], 0);
end;

end.
