object FMainAdmin: TFMainAdmin
  Left = 0
  Top = 0
  Caption = 'AutoUpdater Admin'
  ClientHeight = 471
  ClientWidth = 930
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 930
    Height = 452
    ActivePage = pagParamsApps
    Align = alClient
    TabOrder = 0
    OnChanging = PageControl1Changing
    object pagParamsApps: TTabSheet
      Caption = 'Applications'
      object Shape1: TShape
        Left = 0
        Top = 212
        Width = 922
        Height = 171
        Align = alClient
        Brush.Color = clBtnFace
        Pen.Style = psClear
        ExplicitLeft = 64
        ExplicitTop = 24
        ExplicitWidth = 65
        ExplicitHeight = 65
      end
      object Splitter3: TSplitter
        Left = 0
        Top = 209
        Width = 922
        Height = 3
        Cursor = crVSplit
        Align = alTop
        Color = clMedGray
        ParentColor = False
        ExplicitWidth = 180
      end
      object pnlAppApp: TPanel
        Left = 0
        Top = 0
        Width = 922
        Height = 209
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          922
          209)
        object Shape9: TShape
          Left = 0
          Top = 0
          Width = 922
          Height = 209
          Align = alClient
          Brush.Color = clBtnFace
          Pen.Style = psClear
          ExplicitLeft = 168
          ExplicitTop = 32
          ExplicitWidth = 65
          ExplicitHeight = 65
        end
        object GridApps: TcxGrid
          Left = 16
          Top = 16
          Width = 886
          Height = 156
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 0
          object GridAppsDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.PriorPage.Visible = False
            Navigator.Buttons.NextPage.Visible = False
            Navigator.Buttons.Insert.Visible = False
            DataController.DataSource = dsApplications
            DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.Inserting = False
            object GridAppsDBTableView1Application: TcxGridDBColumn
              DataBinding.FieldName = 'Application'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 200
            end
            object GridAppsDBTableView1App_Directory: TcxGridDBColumn
              Caption = 'App Directory'
              DataBinding.FieldName = 'App_Directory'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 200
            end
            object GridAppsDBTableView1Update_Type_ID: TcxGridDBColumn
              Caption = 'Update Type'
              DataBinding.FieldName = 'Update_Type_ID'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'Update_Type_ID'
              Properties.ListColumns = <
                item
                  FieldName = 'Update_Desc'
                end>
              Properties.ListOptions.ShowHeader = False
              Properties.ListSource = dsAppUpdateType
              Width = 87
            end
            object GridAppsDBTableView1Priority_ID: TcxGridDBColumn
              Caption = 'Priority'
              DataBinding.FieldName = 'Priority_ID'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'Update_Priority_ID'
              Properties.ListColumns = <
                item
                  FieldName = 'Update_Priority_Desc'
                end>
              Properties.ListOptions.ShowHeader = False
              Properties.ListSource = dsPriority
            end
            object GridAppsDBTableView1Idle_Time: TcxGridDBColumn
              Caption = 'Idle Time'
              DataBinding.FieldName = 'Idle_Time'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.OnValidate = GridAppsDBTableView1Idle_TimePropertiesValidate
            end
            object GridAppsDBTableView1TotalVersions: TcxGridDBColumn
              Caption = 'Total Versions'
              DataBinding.FieldName = 'TotalVersions'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
            end
            object GridAppsDBTableView1TotalDownloads: TcxGridDBColumn
              Caption = 'Total Downloads'
              DataBinding.FieldName = 'TotalDownloads'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
            end
            object GridAppsDBTableView1LastDownload: TcxGridDBColumn
              Caption = 'Last Download'
              DataBinding.FieldName = 'LastDownload'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
            end
          end
          object GridAppsLevel1: TcxGridLevel
            GridView = GridAppsDBTableView1
          end
        end
        object cxButton4: TcxButton
          Left = 16
          Top = 178
          Width = 128
          Height = 25
          Action = actUpload
          Anchors = [akLeft, akBottom]
          TabOrder = 1
        end
        object cxButton6: TcxButton
          Left = 150
          Top = 178
          Width = 130
          Height = 25
          Action = actAppDelete
          Anchors = [akLeft, akBottom]
          TabOrder = 2
        end
      end
      object pnlAppBottom: TPanel
        Left = 0
        Top = 383
        Width = 922
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          922
          41)
        object Shape10: TShape
          Left = 0
          Top = 0
          Width = 922
          Height = 41
          Align = alClient
          Brush.Color = clBtnFace
          Pen.Style = psClear
          ExplicitLeft = 160
          ExplicitTop = 8
          ExplicitWidth = 65
          ExplicitHeight = 65
        end
        object cxButton3: TcxButton
          Left = 746
          Top = 7
          Width = 75
          Height = 25
          Action = actAppPost
          Anchors = [akRight, akBottom]
          TabOrder = 0
        end
        object cxButton5: TcxButton
          Left = 827
          Top = 7
          Width = 75
          Height = 25
          Action = actAppCancel
          Anchors = [akRight, akBottom]
          TabOrder = 1
        end
      end
      object pnlAppDetail: TPanel
        Left = 0
        Top = 212
        Width = 922
        Height = 171
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        DesignSize = (
          922
          171)
        object GridUpdatesByAppMain: TcxGrid
          Left = 16
          Top = 16
          Width = 886
          Height = 149
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 0
          object GridUpdatesByAppMainDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dsUpdatesByAppMain
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            object GridUpdatesByAppMainDBTableView1Version: TcxGridDBColumn
              DataBinding.FieldName = 'Version'
              Styles.Content = ReadOnly
            end
            object GridUpdatesByAppMainDBTableView1DBVersion: TcxGridDBColumn
              DataBinding.FieldName = 'DBVersion'
              Styles.Content = ReadOnly
            end
            object GridUpdatesByAppMainDBTableView1TargetFileName: TcxGridDBColumn
              DataBinding.FieldName = 'TargetFileName'
              Styles.Content = ReadOnly
              Width = 150
            end
            object GridUpdatesByAppMainDBTableView1Path_To_Install: TcxGridDBColumn
              DataBinding.FieldName = 'Path_To_Install'
              Styles.Content = ReadOnly
              Width = 150
            end
            object GridUpdatesByAppMainDBTableView1Installer_Desc: TcxGridDBColumn
              DataBinding.FieldName = 'Installer_Desc'
              Styles.Content = ReadOnly
            end
            object GridUpdatesByAppMainDBTableView1Update_Priority_Desc: TcxGridDBColumn
              DataBinding.FieldName = 'Update_Priority_Desc'
              Styles.Content = ReadOnly
              Width = 150
            end
            object GridUpdatesByAppMainDBTableView1Idle_Time: TcxGridDBColumn
              DataBinding.FieldName = 'Idle_Time'
              Styles.Content = ReadOnly
            end
            object GridUpdatesByAppMainDBTableView1Active: TcxGridDBColumn
              DataBinding.FieldName = 'Active'
              Styles.Content = ReadOnly
            end
            object GridUpdatesByAppMainDBTableView1Total_Updates: TcxGridDBColumn
              DataBinding.FieldName = 'Total_Updates'
              Styles.Content = ReadOnly
            end
            object GridUpdatesByAppMainDBTableView1Enterprise: TcxGridDBColumn
              DataBinding.FieldName = 'Enterprise'
              Styles.Content = ReadOnly
              Width = 120
            end
            object GridUpdatesByAppMainDBTableView1Store: TcxGridDBColumn
              DataBinding.FieldName = 'Store'
              Styles.Content = ReadOnly
              Width = 120
            end
            object GridUpdatesByAppMainDBTableView1Computer_ID: TcxGridDBColumn
              DataBinding.FieldName = 'Computer_ID'
              Styles.Content = ReadOnly
              Width = 120
            end
          end
          object GridUpdatesByAppMainLevel1: TcxGridLevel
            GridView = GridUpdatesByAppMainDBTableView1
          end
        end
      end
    end
    object pagUpdates: TTabSheet
      Caption = 'Updates'
      ImageIndex = 1
      object Shape2: TShape
        Left = 0
        Top = 0
        Width = 922
        Height = 248
        Align = alClient
        Brush.Color = clBtnFace
        Pen.Style = psClear
        ExplicitLeft = 64
        ExplicitTop = 24
        ExplicitWidth = 65
        ExplicitHeight = 65
      end
      object Splitter1: TSplitter
        Left = 0
        Top = 248
        Width = 922
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        Color = clGrayText
        ParentColor = False
        ExplicitTop = 249
        ExplicitWidth = 47
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 922
        Height = 248
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          922
          248)
        object Shape5: TShape
          Left = 0
          Top = 0
          Width = 922
          Height = 248
          Align = alClient
          Brush.Color = clBtnFace
          Pen.Style = psClear
          ExplicitWidth = 804
          ExplicitHeight = 251
        end
        object GridUpdates: TcxGrid
          Left = 16
          Top = 16
          Width = 894
          Height = 195
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 0
          object GridUpdatesDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dsUpdates
            DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            object GridUpdatesDBTableView1Application: TcxGridDBColumn
              DataBinding.FieldName = 'Application'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 145
            end
            object GridUpdatesDBTableView1Version: TcxGridDBColumn
              DataBinding.FieldName = 'Version'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 54
            end
            object GridUpdatesDBTableView1DBVersion: TcxGridDBColumn
              DataBinding.FieldName = 'DBVersion'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 60
            end
            object GridUpdatesDBTableView1Installer_Type: TcxGridDBColumn
              DataBinding.FieldName = 'Installer_Type'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'Installer_Type'
              Properties.ListColumns = <
                item
                  FieldName = 'Installer_Desc'
                end>
              Properties.ListOptions.ShowHeader = False
              Properties.ListSource = dsInstallers
            end
            object GridUpdatesDBTableView1Priority_ID: TcxGridDBColumn
              DataBinding.FieldName = 'Priority_ID'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'Update_Priority_ID'
              Properties.ListColumns = <
                item
                  FieldName = 'Update_Priority_Desc'
                end>
              Properties.ListOptions.ShowHeader = False
              Properties.ListSource = dsPriority
            end
            object GridUpdatesDBTableView1Idle_Time: TcxGridDBColumn
              DataBinding.FieldName = 'Idle_Time'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.OnValidate = GridAppsDBTableView1Idle_TimePropertiesValidate
            end
            object GridUpdatesDBTableView1Active: TcxGridDBColumn
              DataBinding.FieldName = 'Active'
            end
            object GridUpdatesDBTableView1Enterprise: TcxGridDBColumn
              DataBinding.FieldName = 'Enterprise'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 120
            end
            object GridUpdatesDBTableView1Store: TcxGridDBColumn
              DataBinding.FieldName = 'Store'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 120
            end
            object GridUpdatesDBTableView1Computer_ID: TcxGridDBColumn
              DataBinding.FieldName = 'Computer_ID'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 120
            end
            object GridUpdatesDBTableView1TargetFileName: TcxGridDBColumn
              DataBinding.FieldName = 'TargetFileName'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 200
            end
            object GridUpdatesDBTableView1Release_Notes: TcxGridDBColumn
              DataBinding.FieldName = 'Release_Notes'
              PropertiesClassName = 'TcxMemoProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 200
            end
            object GridUpdatesDBTableView1Path_To_Install: TcxGridDBColumn
              DataBinding.FieldName = 'Path_To_Install'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 200
            end
            object GridUpdatesDBTableView1URL_Download: TcxGridDBColumn
              DataBinding.FieldName = 'URL_Download'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 300
            end
          end
          object GridUpdatesLevel1: TcxGridLevel
            GridView = GridUpdatesDBTableView1
          end
        end
        object cxDBNavigator2: TcxDBNavigator
          Left = 16
          Top = 217
          Width = 260
          Height = 25
          Buttons.CustomButtons = <>
          Buttons.Insert.Visible = False
          Buttons.Delete.Visible = False
          DataSource = dsUpdates
          Anchors = [akLeft, akBottom]
          TabOrder = 1
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 251
        Width = 922
        Height = 173
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          922
          173)
        object Shape6: TShape
          Left = 0
          Top = 0
          Width = 922
          Height = 173
          Align = alClient
          Brush.Color = clBtnFace
          Pen.Style = psClear
          ExplicitLeft = 64
          ExplicitTop = 32
          ExplicitWidth = 65
          ExplicitHeight = 65
        end
        object Label5: TLabel
          Left = 16
          Top = 16
          Width = 52
          Height = 13
          Caption = 'Application'
        end
        object cbApplications: TcxLookupComboBox
          Left = 74
          Top = 16
          Properties.DropDownListStyle = lsEditList
          Properties.KeyFieldNames = 'Application'
          Properties.ListColumns = <
            item
              FieldName = 'Application'
            end>
          Properties.ListSource = dsApplications
          Properties.OnChange = cbApplicationsPropertiesChange
          TabOrder = 0
          Width = 145
        end
        object GridUpdatesByApp: TcxGrid
          Left = 16
          Top = 43
          Width = 894
          Height = 120
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 1
          object GridUpdatesByAppDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dsUpdatesByApp
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            Styles.ContentEven = ReadOnly
            Styles.ContentOdd = ReadOnly
            object GridUpdatesByAppDBTableView1Version: TcxGridDBColumn
              DataBinding.FieldName = 'Version'
            end
            object GridUpdatesByAppDBTableView1DBVersion: TcxGridDBColumn
              DataBinding.FieldName = 'DBVersion'
            end
            object GridUpdatesByAppDBTableView1Priority_ID: TcxGridDBColumn
              DataBinding.FieldName = 'Priority_ID'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'Update_Priority_ID'
              Properties.ListColumns = <
                item
                  FieldName = 'Update_Priority_Desc'
                end>
              Properties.ListSource = dsPriority
            end
            object GridUpdatesByAppDBTableView1Idle_Time: TcxGridDBColumn
              DataBinding.FieldName = 'Idle_Time'
            end
            object GridUpdatesByAppDBTableView1Installer_Type: TcxGridDBColumn
              DataBinding.FieldName = 'Installer_Type'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'Installer_Type'
              Properties.ListColumns = <
                item
                  FieldName = 'Installer_Desc'
                end>
              Properties.ListSource = dsInstallers
            end
            object GridUpdatesByAppDBTableView1Active: TcxGridDBColumn
              DataBinding.FieldName = 'Active'
            end
            object GridUpdatesByAppDBTableView1Enterprise: TcxGridDBColumn
              DataBinding.FieldName = 'Enterprise'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 120
            end
            object GridUpdatesByAppDBTableView1Store: TcxGridDBColumn
              DataBinding.FieldName = 'Store'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 120
            end
            object GridUpdatesByAppDBTableView1Computer_ID: TcxGridDBColumn
              DataBinding.FieldName = 'Computer_ID'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 120
            end
            object GridUpdatesByAppDBTableView1TargetFileName: TcxGridDBColumn
              DataBinding.FieldName = 'TargetFileName'
              Width = 200
            end
            object GridUpdatesByAppDBTableView1Release_Notes: TcxGridDBColumn
              DataBinding.FieldName = 'Release_Notes'
              Width = 200
            end
            object GridUpdatesByAppDBTableView1Path_To_Install: TcxGridDBColumn
              DataBinding.FieldName = 'Path_To_Install'
              Width = 200
            end
            object GridUpdatesByAppDBTableView1URL_Download: TcxGridDBColumn
              DataBinding.FieldName = 'URL_Download'
              Width = 300
            end
          end
          object GridUpdatesByAppLevel1: TcxGridLevel
            GridView = GridUpdatesByAppDBTableView1
          end
        end
      end
    end
    object pagComputerStatus: TTabSheet
      Caption = 'Computer status'
      ImageIndex = 2
      object Splitter2: TSplitter
        Left = 0
        Top = 201
        Width = 922
        Height = 3
        Cursor = crVSplit
        Align = alTop
        Color = clMedGray
        ParentColor = False
        ExplicitTop = 209
        ExplicitWidth = 180
      end
      object pnlComputerTop: TPanel
        Left = 0
        Top = 0
        Width = 922
        Height = 201
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          922
          201)
        object Shape3: TShape
          Left = 0
          Top = 0
          Width = 922
          Height = 201
          Align = alClient
          Brush.Color = clBtnFace
          Pen.Style = psClear
          ExplicitLeft = 160
          ExplicitTop = 24
          ExplicitWidth = 65
          ExplicitHeight = 65
        end
        object GridComputer: TcxGrid
          Left = 8
          Top = 8
          Width = 902
          Height = 153
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 0
          object GridComputerDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            OnInitEdit = GridComputerDBTableView1InitEdit
            DataController.DataSource = dsComputer
            DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsSelection.InvertSelect = False
            object GridComputerDBTableView1Computer_ID: TcxGridDBColumn
              DataBinding.FieldName = 'Computer_ID'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 100
            end
            object GridComputerDBTableView1Enterprise: TcxGridDBColumn
              DataBinding.FieldName = 'Enterprise'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 100
            end
            object GridComputerDBTableView1Store: TcxGridDBColumn
              DataBinding.FieldName = 'Store'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 100
            end
            object GridComputerDBTableView1Machine_Number: TcxGridDBColumn
              DataBinding.FieldName = 'Machine_Number'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 100
            end
            object GridComputerDBTableView1Application: TcxGridDBColumn
              DataBinding.FieldName = 'Application'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 128
            end
            object GridComputerDBTableView1Version: TcxGridDBColumn
              DataBinding.FieldName = 'Version'
              PropertiesClassName = 'TcxTextEditProperties'
              Styles.Content = ReadOnly
            end
            object GridComputerDBTableView1DBVersion: TcxGridDBColumn
              DataBinding.FieldName = 'DBVersion'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
            end
            object GridComputerDBTableView1Update_To_ID: TcxGridDBColumn
              DataBinding.FieldName = 'Update_To_ID'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'Update_Type_ID'
              Properties.ListColumns = <
                item
                  FieldName = 'Update_Desc'
                end>
              Properties.ListOptions.ShowHeader = False
              Properties.ListSource = dsUpdateType
            end
            object GridComputerDBTableView1Priority_ID: TcxGridDBColumn
              DataBinding.FieldName = 'Priority_ID'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'Update_Priority_ID'
              Properties.ListColumns = <
                item
                  FieldName = 'Update_Priority_Desc'
                end>
              Properties.ListOptions.ShowHeader = False
              Properties.ListSource = dsPriority
            end
            object GridComputerDBTableView1Idle_Time: TcxGridDBColumn
              DataBinding.FieldName = 'Idle_Time'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.OnValidate = GridAppsDBTableView1Idle_TimePropertiesValidate
            end
            object GridComputerDBTableView1Install_Ver: TcxGridDBColumn
              Caption = 'Select update'
              DataBinding.FieldName = 'Install_Ver'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 120
            end
            object GridComputerDBTableView1GlobalUpdateVersion: TcxGridDBColumn
              Caption = 'Global Update Version'
              DataBinding.FieldName = 'GlobalUpdateVersion'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 120
            end
            object GridComputerDBTableView1Last_Op: TcxGridDBColumn
              DataBinding.FieldName = 'Last_Op'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 100
            end
            object GridComputerDBTableView1Date_Op: TcxGridDBColumn
              DataBinding.FieldName = 'Date_Op'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
              Width = 150
            end
            object GridComputerDBTableView1External_IP: TcxGridDBColumn
              DataBinding.FieldName = 'External_IP'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
            end
            object GridComputerDBTableView1Local_IP: TcxGridDBColumn
              DataBinding.FieldName = 'Local_IP'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
            end
            object GridComputerDBTableView1Component_Version: TcxGridDBColumn
              DataBinding.FieldName = 'Component_Version'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Styles.Content = ReadOnly
            end
          end
          object GridComputerLevel1: TcxGridLevel
            GridView = GridComputerDBTableView1
          end
        end
        object cxDBNavigator3: TcxDBNavigator
          Left = 8
          Top = 167
          Width = 234
          Height = 25
          Buttons.CustomButtons = <>
          Buttons.Insert.Visible = False
          Buttons.Delete.Visible = False
          DataSource = dsComputer
          Anchors = [akLeft, akBottom]
          TabOrder = 1
        end
      end
      object pnlComputerBottom: TPanel
        Left = 0
        Top = 204
        Width = 922
        Height = 220
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          922
          220)
        object Shape8: TShape
          Left = 0
          Top = 0
          Width = 922
          Height = 220
          Align = alClient
          Brush.Color = clBtnFace
          Pen.Style = psClear
          ExplicitLeft = 328
          ExplicitTop = 56
          ExplicitWidth = 65
          ExplicitHeight = 65
        end
        object GridLogDetail: TcxGrid
          Left = 8
          Top = 7
          Width = 902
          Height = 202
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 0
          object GridLogDetailDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dsLogDetail
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Styles.Content = ReadOnly
            object GridLogDetailDBTableView1Version: TcxGridDBColumn
              DataBinding.FieldName = 'Version'
            end
            object GridLogDetailDBTableView1DBVersion: TcxGridDBColumn
              DataBinding.FieldName = 'DBVersion'
            end
            object GridLogDetailDBTableView1Desc_Op: TcxGridDBColumn
              DataBinding.FieldName = 'Desc_Op'
              Width = 100
            end
            object GridLogDetailDBTableView1Date_Op: TcxGridDBColumn
              DataBinding.FieldName = 'Date_Op'
              Width = 150
            end
          end
          object GridLogDetailLevel1: TcxGridLevel
            GridView = GridLogDetailDBTableView1
          end
        end
      end
    end
    object pagJustMajorUpdates: TTabSheet
      Caption = 'Customers with only major updates'
      ImageIndex = 5
      DesignSize = (
        922
        424)
      object Shape11: TShape
        Left = 0
        Top = 0
        Width = 922
        Height = 424
        Align = alClient
        Brush.Color = clBtnFace
        Pen.Style = psClear
        ExplicitLeft = 144
        ExplicitTop = 24
        ExplicitWidth = 65
        ExplicitHeight = 65
      end
      object GridJustMajorUpdates: TcxGrid
        Left = 16
        Top = 16
        Width = 889
        Height = 353
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object GridJustMajorUpdatesDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          OnInitEdit = GridJustMajorUpdatesDBTableView1InitEdit
          DataController.DataSource = dsJustMajorUpdates
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsData.Appending = True
          object GridJustMajorUpdatesDBTableView1Application: TcxGridDBColumn
            DataBinding.FieldName = 'Application'
            PropertiesClassName = 'TcxComboBoxProperties'
            Width = 200
          end
          object GridJustMajorUpdatesDBTableView1Enterprise: TcxGridDBColumn
            DataBinding.FieldName = 'Enterprise'
            PropertiesClassName = 'TcxComboBoxProperties'
            Width = 200
          end
          object GridJustMajorUpdatesDBTableView1Store: TcxGridDBColumn
            DataBinding.FieldName = 'Store'
            PropertiesClassName = 'TcxComboBoxProperties'
            Width = 200
          end
        end
        object GridJustMajorUpdatesLevel1: TcxGridLevel
          GridView = GridJustMajorUpdatesDBTableView1
        end
      end
      object navJustMajorUpdates: TcxDBNavigator
        Left = 16
        Top = 385
        Width = 270
        Height = 25
        Buttons.CustomButtons = <>
        Buttons.Insert.Visible = False
        Buttons.Append.Visible = True
        Buttons.Delete.Visible = True
        DataSource = dsJustMajorUpdates
        Anchors = [akLeft, akBottom]
        TabOrder = 1
      end
    end
    object pagLog: TTabSheet
      Caption = 'Log viewer'
      ImageIndex = 3
      DesignSize = (
        922
        424)
      object Shape4: TShape
        Left = 0
        Top = 0
        Width = 922
        Height = 424
        Align = alClient
        Brush.Color = clBtnFace
        Pen.Style = psClear
        ExplicitLeft = 64
        ExplicitTop = 24
        ExplicitWidth = 65
        ExplicitHeight = 65
      end
      object GridLog: TcxGrid
        Left = 8
        Top = 16
        Width = 902
        Height = 393
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object GridLogDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dsLog
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Styles.ContentEven = ReadOnly
          Styles.ContentOdd = ReadOnly
          object GridLogDBTableView1Computer_ID: TcxGridDBColumn
            DataBinding.FieldName = 'Computer_ID'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Styles.Content = ReadOnly
            Width = 100
          end
          object GridLogDBTableView1Enterprise: TcxGridDBColumn
            DataBinding.FieldName = 'Enterprise'
            Width = 100
          end
          object GridLogDBTableView1Store: TcxGridDBColumn
            DataBinding.FieldName = 'Store'
            Width = 100
          end
          object GridLogDBTableView1Machine_Number: TcxGridDBColumn
            DataBinding.FieldName = 'Machine_Number'
            Width = 100
          end
          object GridLogDBTableView1Application: TcxGridDBColumn
            DataBinding.FieldName = 'Application'
            Width = 128
          end
          object GridLogDBTableView1Version: TcxGridDBColumn
            DataBinding.FieldName = 'Version'
          end
          object GridLogDBTableView1DBVersion: TcxGridDBColumn
            DataBinding.FieldName = 'DBVersion'
          end
          object GridLogDBTableView1Desc_Op: TcxGridDBColumn
            DataBinding.FieldName = 'Desc_Op'
            Width = 100
          end
          object GridLogDBTableView1Date_Op: TcxGridDBColumn
            DataBinding.FieldName = 'Date_Op'
            Width = 140
          end
          object GridLogDBTableView1External_IP: TcxGridDBColumn
            DataBinding.FieldName = 'External_IP'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Styles.Content = ReadOnly
          end
          object GridLogDBTableView1Local_IP: TcxGridDBColumn
            DataBinding.FieldName = 'Local_IP'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Styles.Content = ReadOnly
          end
          object GridLogDBTableView1Component_Version: TcxGridDBColumn
            DataBinding.FieldName = 'Component_Version'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Styles.Content = ReadOnly
          end
        end
        object GridLogLevel1: TcxGridLevel
          GridView = GridLogDBTableView1
        end
      end
    end
    object pagSetup: TTabSheet
      Caption = 'Setup'
      ImageIndex = 4
      object Shape7: TShape
        Left = 0
        Top = 0
        Width = 922
        Height = 424
        Align = alClient
        Brush.Color = clBtnFace
        Pen.Style = psClear
        ExplicitLeft = 112
        ExplicitTop = 24
        ExplicitWidth = 65
        ExplicitHeight = 65
      end
      object Label1: TLabel
        Left = 16
        Top = 21
        Width = 22
        Height = 13
        Caption = 'Alias'
      end
      object Label2: TLabel
        Left = 144
        Top = 21
        Width = 57
        Height = 13
        Caption = 'Server type'
      end
      object Label3: TLabel
        Left = 240
        Top = 21
        Width = 51
        Height = 13
        Caption = 'User name'
      end
      object Label4: TLabel
        Left = 368
        Top = 21
        Width = 46
        Height = 13
        Caption = 'Password'
      end
      object Label6: TLabel
        Left = 16
        Top = 72
        Width = 74
        Height = 13
        Caption = 'Repository URL'
      end
      object Label7: TLabel
        Left = 16
        Top = 128
        Width = 69
        Height = 13
        Caption = 'Root directory'
      end
      object edAlias: TEdit
        Left = 16
        Top = 35
        Width = 121
        Height = 21
        TabOrder = 0
      end
      object cbServerType: TComboBox
        Left = 143
        Top = 35
        Width = 90
        Height = 21
        ItemIndex = 0
        TabOrder = 1
        Text = 'Local'
        Items.Strings = (
          'Local'
          'Remote'
          'Internet')
      end
      object edUserName: TEdit
        Left = 239
        Top = 35
        Width = 121
        Height = 21
        TabOrder = 2
      end
      object edPassword: TEdit
        Left = 366
        Top = 35
        Width = 121
        Height = 21
        PasswordChar = '*'
        TabOrder = 3
      end
      object cxButton1: TcxButton
        Left = 493
        Top = 33
        Width = 92
        Height = 25
        Action = actDBConnect
        TabOrder = 4
      end
      object cxButton2: TcxButton
        Left = 591
        Top = 33
        Width = 92
        Height = 25
        Action = actDBDisconnect
        TabOrder = 5
      end
      object edRepositoryURL: TEdit
        Left = 16
        Top = 88
        Width = 667
        Height = 21
        TabOrder = 6
      end
      object edRootDirectory: TEdit
        Left = 16
        Top = 144
        Width = 641
        Height = 21
        TabOrder = 7
      end
      object btnSelectFile: TcxButton
        Left = 658
        Top = 142
        Width = 25
        Height = 25
        OptionsImage.Glyph.SourceDPI = 96
        OptionsImage.Glyph.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          6100000006624B4744000000000000F943BB7F000000097048597300000DD700
          000DD70142289B7800000009767041670000001000000010005CC6ADC3000001
          A14944415438CB8D93BD6E13411485BF99B93BB36BE4B54C14840B9428145414
          9190E828481E2091F20CA910AF400B2F928A8282962225053450A6B0C58F5224
          B142C88F1D7B2FC5CCC6D820C747BA1A6966CEA77BEEEE185505C018B302AC30
          91E15F695A7BAADA8B3BAA24C8335D50E92EAA8A6CBC3918AA92ADEFEE01F0FD
          C74F9C73A92CC6984901AD566BAA2501ECABED7B7CFDD48E1BE21011ACB5B18C
          C1581BF398986A7D776FFFF9EB03002CC0F1AF11A7E7A30488E69B2E441017A1
          E21C003B4F9ABCDCBC9B3A50CCE5B53288FE89D95A6C5AEB08B50623B8BC8EF3
          1465EA2C0212A48E310B989DC154BE9BEC33E629C85F30F3F0C5BE025457A7D8
          D064115583336C1EBF86F8CCB1F57495FE61979D8DC79C5D5573CDCDDCF2F6C3
          17DAF75779F7B18BE4DE331843F768C8F1EF31FDF3F15CC070E4E81E0D692C43
          EE3D52840CEF2CCDDC916796C2EB5C409EC5BBDE598A902145F02C959E8B76A0
          6C0865436E9D41A71D582A3D45F0481E32965B9EC65A875EBFBAD50CF068ADC3
          9DD293D71DBCFF7C928E4E1602D4778BE031C427FC80F45BF3FF673C2B052AE0
          DB1FF45F935D1FEE512100000025744558746372656174652D64617465003230
          30392D31312D31355431373A30323A33342D30373A3030B6E78E120000002574
          455874646174653A63726561746500323031302D30322D32305432333A32363A
          31352D30373A3030063B5C810000002574455874646174653A6D6F6469667900
          323031302D30312D31315430393A32343A32362D30373A30304961C749000000
          67744558744C6963656E736500687474703A2F2F6372656174697665636F6D6D
          6F6E732E6F72672F6C6963656E7365732F62792D73612F332E302F206F722068
          7474703A2F2F6372656174697665636F6D6D6F6E732E6F72672F6C6963656E73
          65732F4C47504C2F322E312F5B8F3C6300000025744558746D6F646966792D64
          61746500323030392D30332D31395431303A35323A35312D30363A30307F68FD
          060000001974455874536F667477617265007777772E696E6B73636170652E6F
          72679BEE3C1A0000001374455874536F75726365004F787967656E2049636F6E
          73EC18AEE80000002774455874536F757263655F55524C00687474703A2F2F77
          77772E6F787967656E2D69636F6E732E6F72672FEF37AACB0000000049454E44
          AE426082}
        TabOrder = 8
        OnClick = btnSelectFileClick
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 452
    Width = 930
    Height = 19
    Panels = <
      item
        Width = 110
      end
      item
        Width = 80
      end
      item
        Width = 50
      end>
  end
  object ActionList1: TActionList
    Images = ImageList1
    OnUpdate = ActionList1Update
    Left = 492
    Top = 112
    object actDBConnect: TAction
      Category = 'Connection'
      Caption = 'Connect'
      ImageIndex = 0
      OnExecute = actDBConnectExecute
    end
    object actDBDisconnect: TAction
      Category = 'Connection'
      Caption = 'Disconnect'
      ImageIndex = 1
      OnExecute = actDBDisconnectExecute
    end
    object actUpload: TAction
      Category = 'Upload'
      Caption = 'Upload file'
      ImageIndex = 4
      OnExecute = actUploadExecute
    end
    object actAppPost: TAction
      Category = 'Application'
      Caption = 'OK'
      ImageIndex = 3
      OnExecute = actAppPostExecute
    end
    object actAppCancel: TAction
      Category = 'Application'
      Caption = 'Cancel'
      ImageIndex = 2
      OnExecute = actAppCancelExecute
    end
    object actAppDelete: TAction
      Category = 'Application'
      Caption = 'Delete Application'
      ImageIndex = 7
      OnExecute = actAppDeleteExecute
    end
  end
  object ImageList1: TImageList
    Height = 24
    Width = 24
    Left = 556
    Top = 112
    Bitmap = {
      494C010108001800040018001800FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000060000000480000000100200000000000006C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000202020F3414140EF444442E7444442E7444442E74444
      42E7444442E7444442E7444442E7444442E7444442E7222222EA000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CDD5CD34496C4ABC446744BB4468
      44BB446A44BB446B44BB487149BDC1CDC1410000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000403F3EF5E9E5E2FFF1EEECFFF1EEECFFF1EEECFFF1EE
      ECFFF1EEECFFF1EEECFFF1EEECFFF1EEECFFF1EEECFF444343E5000000000000
      000000000000000000000000000000000000F3F3F30CC7C7C738B4B4B44BB3B3
      B34CB3B3B34CB3B3B34CB3B3B34CB3B3B34CB3B3B34CB3B3B34CB3B3B34CB3B3
      B34CB3B3B34CB3B3B34CB3B3B34CB3B3B34CB3B3B34CB3B3B34CB3B3B34CB3B3
      B34CB3B3B34CB4B4B44BC7C7C738F3F3F30C0000000000000000000000000000
      0000000000000000000000000000000000003C663CC5547553FF1F6923FF0D67
      15FF14791DFF218D2CFF4FA558FF567F58AF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000403F3EF5E9E5E2FFF1EEECFFF1EEECFFF1EEECFFF1EE
      ECFFF1EEECFFF1EEECFFF1EEECFFF1EEECFFF1EEECFF444343E5000000000000
      000000000000000000000000000000000000F6EEE54AD9C3ACD7CEB59DE6CEB4
      9CE7CEB49CE7CEB49CE7CEB49CE7CEB49CE7CEB49CE7CEB49CE7CEB49CE7CEB4
      9CE7CEB49CE7CEB49CE7CEB49CE7CDB49CE7CDB49CE7CDB49CE7CDB49CE7CDB4
      9CE7CDB49CE7CEB59CE6D9C1AAD9F6EDE54B0000000000000000000000000000
      0000000000000000000000000000000000003B643BC43E8839FF22B628FF03A7
      14FF1BBF2AFF3BDE4CFF3FC04EFF537D53AC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000403F3EF5E9E5E2FFF1EEECFFE6E3E1FF484746FF4342
      42FF434242FF484746FFE6E3E1FFF1EEECFFF1EEECFF444343E5000000000000
      000000000000000000000000000000000000FCE1CC7EFBCAA0FBFBCFA3FFFBCE
      A2FFFBCEA2FFFBCEA2FFFBCEA2FFFBCEA2FFFBCEA2FFFBCEA2FFFBCEA2FFFBCE
      A2FFFBCEA2FFFBCEA2FFFBCEA2FFFBCEA2FFFBCEA2FFFBCEA2FFFBCEA2FFFBCE
      A2FFFBCEA2FFFBCEA2FFFAC99EFFFCE1CC7F0000000000000000000000000000
      0000000000000000000000000000000000003B653BC4398535FF20B527FF07A7
      17FF20C02FFF42DF52FF3EC44DFF537E53AC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000403F3EF5E9E5E2FFF1EEECFFE7E4E2FF7B7978FF7977
      76FF797776FF7B7978FFE7E4E2FFF1EEECFFF1EEECFF444343E5000000000000
      000000000000000000000000000000000000F9D1B398F8C397F6FBD1AAFCF9CF
      A6FFF6CDA5FFF5CDA5FFF5CDA5FFF5CDA5FFF5CDA5FFF5CDA5FFF5CDA5FFF5CD
      A5FFF5CDA5FFF5CDA5FFF6CDA5FFF9CEA6FFFACFA6FFFACFA6FFFACFA6FFFACF
      A6FFFACFA6FFFAD0A8FFF7C092FBF9D1B3970000000000000000000000000000
      0000000000000000000000000000000000003B653BC43A8A36FF21B928FF0BAB
      1BFF24C433FF47E356FF42CC51FF538053AC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000403F3EF5E9E5E2FFF1EEECFFF0EDEBFFBEBCBAFFBCBA
      B8FFBCBAB8FFBEBCBAFFF0EDEBFFF1EEECFFF1EEECFF444343E5000000000000
      000000000000000000000000000000000000F6C9A6AEF7C499F3FBD2ABFBF8CE
      A5FFF8D2ADFFFBD7B4FFFBD8B4FFFBD8B4FFFBD8B4FFFBD8B4FFFBD7B4FFFAD7
      B4FFFAD7B3FFF9D6B3FFF8D4B0FFF6CEA7FFF8CEA6FFFACFA7FFFACFA7FFFACF
      A7FFFACFA7FFFAD0A8FFF6C195F9F6C9A6AE0000000000000000000000000000
      0000000000000000000000000000000000003B663BC43A9137FF21B828FF0BAB
      1BFF24C433FF47E356FF42CE51FF538053AC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EAEA
      EA15ABABAB54767263A7403E3CF9E9E5E2FFF1EEECFFE6E3E1FF484746FF4342
      42FF434242FF484746FFE6E3E1FFF1EEECFFF1EEECFF43413CF05E5846C78680
      7395C1C0BF40000000000000000000000000F2BF97C1F5C196F1F9CBA1FBF6C7
      9CFFF8CDA6FFFDD5B1FFFDD5B0FFFDD5B0FFFDD5B0FFFDD5B0FFFDD5B0FFFCD4
      B0FFFCD4AFFFFBD3AFFFF9D2ADFFF6CDA8FFF3C79FFFF6C89DFFF8C99EFFF8C9
      9EFFF8C99EFFF8C99FFFF4BE91F8F2BF97C10000000000000000000000000000
      0000000000000000000000000000000000003B663BC43A9838FF20B326FF07A7
      17FF20C02FFF42DE51FF3FCD4EFF538153AC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A19E96704339
      1DF48F7427FEBB9930FF403E37FFE9E5E2FFF1EEECFFF1EEECFFE1DEDCFFD3D0
      CEFFD3D0CEFFE1DEDCFFF1EEECFFF1EEECFFF1EEECFF423D2FFFD1AA36FFB190
      2EFF796423FD4F4832DBE5E5E51A00000000EDB385D1F3BB8CF0F7C093FBF4BD
      8FFFF5C398FFFACAA2FFFACAA1FFFACAA1FFFACAA2FFFACAA2FFFACAA1FFFAC9
      A1FFF9C9A0FFF8C8A0FFF7C79EFFF6C69EFFF4C39AFFF1BD91FFF4BD8FFFF6BE
      90FFF6BE8FFFF6BF90FEF3B788F6EEB386D10000000000000000000000000000
      0000000000000000000000000000000000003E693EC1399D38FF20AB26FF02A0
      12FF18B827FF38D548FF3ACA4AFF578357A80000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE01FEFE
      FE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFE
      FE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFE
      FE01FEFEFE01FEFEFE01000000000000000000000000847F7299715D20FEE5BB
      3BFFEAC44BFFEDCB5CFF403E39FFE9E5E2FFF1EEECFFF1EEECFF27293AFF3237
      5FFF3D4265FF30323CFFF1EEECFFF1EEECFFF1EEECFF433F34FFEFD26CFFEFD1
      6AFFEDCB5DFFCCAA43FF433D2CE2F9F9F906E9A773DFF2B482EFF5B684FAF3B2
      80FFF4B788FFF8BF92FFF8BF92FFF8BF92FFF8BF92FFF9BF92FFF9BF92FFF8BE
      91FFF8BE91FFF7BD90FFF6BC8FFFF5BB8EFFF7BD90FFF2B88AFFF0B181FFF3B2
      80FFF4B381FFF5B482FEF2B07EF5E9A773DFCAD3CA373C673CC43B663BC43B66
      3BC43B663BC43B663BC43B663BC43D683DC2456D46BE4EAC4CFF1E9F23FF0093
      0CFF0DAE1CFF29C939FF3CC64CFF3A743DC8668966996387639C6387639C6387
      639C6386639C6385639C6286639DC3CFC33EF7F7F808D2D2E02DBBBBD344BBBB
      D344BBBBD444BBBBD444BBBBD544BBBBD544BBBBD544BBBBD644BBBBD644BBBB
      D644BBBBD644BBBBD644BBBBD644BBBBD644BBBBD644BBBBD544BBBBD544BBBB
      D544BBBBD444BDBDD542DCDCE723FCFCFC03D2D2D02F53461EF9E8BE3FFFEECE
      63FFF2D97CFFF2D97CFF413E3AFFE9E5E2FFF1EEECFFF1EEECFF2A2E48FF6877
      E8FF788CFFFF383B4CFFF1EEECFFF1EEECFFF1EEECFF434036FFF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFFA48E49FB817E7692E39A63EAF1AB78EDF3AB76FAF0A7
      71FFF1AC7AFFF6B484FFF6B483FFF6B483FFF6B483FFF6B483FFF6B483FFF6B4
      83FFF5B382FFF5B282FFF4B281FFF3B180FFF2B07FFFF2B07FFFF0AD7BFFEEA8
      73FFF2A873FFF2A974FEF0A974F3E39B63E9496A49C23C7A42FF127B19FF117B
      19FF117E1AFF11831AFF11861AFF118C1CFF259D30FF4FCB50FF1A8F1FFF007F
      08FF019F0FFF1EBB2EFF5DD067FF56BD5EFF47BB50FF48B951FF49B852FF49B3
      51FF48AC50FF49A54FFF5BA462FF678B69B0D6D6E929575ABDA82127B2DE2129
      B5DE212AB7DE212CB9DE212DBBDE212EBCDE212FBDDE212FBEDE2130BFDE2130
      BFDE2130BFDE2130BFDE2130BFDE212FBEDE212EBDDE212DBCDE212CBBDE212A
      B9DE2129B7DE2329B3DC7274C78DEBEBF4145E5743CBCFA835FFEFCF66FFF2D9
      7CFFF2D97CFFF2D97CFF413E3AFFE9E5E2FFF1EEECFFF1EEECFF2A2E48FF6877
      E8FF788CFFFF383B4CFFADABAAFF7F7D7CFF7F7D7CFF25231EFFF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFFF2D97CFF474339D5DC9054F1ECA571EBECA26BFAEA9F
      67FFEBA36FFFF0AB78FFF0AA78FFF0AB78FFF0AB78FFF0AB78FFF0AB78FFF0AB
      78FFEFAA78FFEFAA77FFEFA977FFEEA976FFEDA976FFEDA875FFEDA976FFEAA3
      6DFFEA9F67FFECA169FEECA36DF1DC9155F1436643BE13731CFF02970DFF0296
      0FFF01A40FFF02B212FF04BA15FF05BB15FF00B610FF00AA0CFF078812FF0D78
      17FF1F972AFF37B542FF56C95EFF66D46CFF70DB77FF77DF7EFF7BE081FF7BDF
      82FF7ADC80FF7BDD81FF60B467FF567B59AAC6C7E7393441C6CF0C22CBFF0E26
      CEFF0E29D0FF0E2BD1FF0E2DD3FF0E2ED4FF0E30D5FF0E31D6FF0E31D7FF0E31
      D8FF0E31D8FF0E32D7FF0E31D6FF0E30D5FF0E2ED5FF0E2DD4FF0E2BD1FF0E29
      D0FF0E26CDFF0D22C9FF4E58CBB5DFDFF020443C23ECEBC650FFF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFF413E3AFFE9E5E2FFF1EEECFFF1EEECFF2A2E48FF6877
      E8FF788CFFFF383B4CFF484746FFB0ACA9FF464443FF786E4AFFF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFFF2D97CFF454232E2D68D53FDEBBC97F6ECBA96FCEBB9
      94FFEBBB96FFECBE99FFECBE99FFECBE99FFECBE99FFECBE99FFECBE99FFECBE
      99FFECBE99FFECBE99FFECBD99FFECBD99FFECBD98FFECBC98FFECBD98FFEBBB
      96FFEBB994FFEBBA95FEEBBB96FADC9D6CFD416741BE0C5814FF00700AFF0088
      0DFF05A014FF10AE1FFF17B626FF1AB829FF1DB72BFF21B32FFF28AE35FF319E
      3CFF369140FF399F43FF3CB047FF44BB4FFF4CC158FF53C55EFF57C762FF59C6
      64FF59C264FF5EC168FF509A58FF557555AAC6C8E9393C54D3CF294DDFFF3056
      E3FF3059E5FF305BE7FF305EEAFF3060EBFF3061ECFF3063EDFF3063EEFF3064
      EFFF3063EEFF3063EEFF3061EDFF3060EBFF305EE9FF305CE8FF3059E6FF3056
      E3FF3053E1FF2B4ADBFF5968D5B5DEDEF02141381DF4EECF64FFF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFF413E3AFFE9E5E2FFF1EEECFFF1EEECFF2A2E48FF6877
      E8FF788CFFFF383B4CFF494847FF726F6DFF786E4AFFF2D97CFFF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFFF2D97CFF464236D9DBA172B4D7945EDCD28649FFCC82
      45FFCE8E5AFFDAA274FFDAA173FFDAA173FFDAA273FFDAA274FFDBA274FFDBA2
      74FFDBA274FFDBA274FFDBA274FFDAA173FFDAA173FFDAA173FFDAA274FFCE8E
      5AFFCC8245FFD28649FFD7945EDCDCA376B4416841BE0B5F14FF008B0BFF06A8
      15FF18BA27FF2ACA39FF3CD54AFF4CDA59FF53DA60FF51D35EFF47C753FF3CB9
      48FF3BA145FF409248FF449B4CFF48A751FF4DB056FF51B55BFF55B65FFF5AB5
      63FF5EB166FF67B16EFF568F5CFF557055AAC6C9EA393E59D8CF244FE4FF2757
      E8FF275BEBFF275EEEFF2761F1FF2764F3FF2766F5FF2767F6FF2768F7FF2769
      F7FF2768F6FF2767F5FF2765F4FF2763F2FF2760F0FF275DEDFF2759EAFF2755
      E7FF2752E4FF2247DDFF5366D7B5DEDEF121423C24EAF0D471FFF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFF252320FF7C7974FF807C76FF807C76FF212337FF6877
      E8FF788CFFFF2B2D39FF232220FF786E4AFFF2D97CFFF2D97CFFF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFFC6B266FF65645CAD00000000F1C9AE7FE09D68FFC98F
      5EFFCEB7A4FFFCFDFEFFFAFAFAFFFBFAFAFFFBFBFBFFFCFCFBFFFDFCFCFFFDFD
      FDFFFDFDFDFFFEFEFDFFFDFDFDFFFCFCFCFFFBFBFBFFFBFAFAFFFDFDFEFFCFB8
      A4FFC98F5EFFE09D68FFF1C9AE7F00000000416841BE10741AFF0DAD1CFF28C3
      36FF48D955FF65EB71FF78F783FF7FFA8BFF7BF687FF6FE97AFF5FD86AFF4FC8
      5AFF45B650FF479D50FF4E9856FF56A05DFF5CA663FF61AB68FF65AE6DFF6AAF
      71FF6FB075FF77B87EFF619366FF556B55AAC6C9EB39495FD7D03D5FE3FF4267
      E7FF426AE9FF426DECFF426FEEFF4271F0FF4273F1FF4274F2FF4275F3FF4275
      F3FF4275F3FF4274F2FF4272F1FF4271EFFF426EEEFF426CEBFF4269E9FF4266
      E6FF4263E3FF3955DDFF5B6AD7B5DEDEF2216F6A59B3BCA657FEF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFFCBB668FFBDA961FFBDA961FFBDA961FF262839FF6877
      E8FF788CFFFF323235FFC4B065FFF2D97CFFF2D97CFFF2D97CFFF2D97CFFF2D9
      7CFFF2D97CFFEAD278FF433E2FE4E7E7E71800000000F1CAAE7FE4A575FFCE9B
      70FFD0BCABFFFBFCFDFFFAFAFAFFFAFAFAFFFBFBFBFFFBFBFBFFFCFCFCFFFDFD
      FDFFFDFDFDFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFFFFFFFFD2BE
      ADFFCE9A70FFE4A575FFF1CAAE7F00000000446B45C4A2CBA7FF9FD9A4FFAAE1
      AEFFB2E9B7FFB8F0BDFFBFF5C4FFC4F6C8FFC2F6C8FF90EF97FF79E882FF5ED3
      6AFF4FC15BFF57B35EFF84C486FF78B97CFF98C89CFFB8D7BBFFB9D6BCFFBBD4
      BCFFBBD2BDFFBED5C0FFABBFADFF677767B4CFD1EF305C6CDABF586FE2F6637B
      E7F4637DE8F46380EAF46382ECF46385EEF46386EFF46387F0F46388F0F46388
      F0F46388F0F46387F0F46386EFF46385EEF46382ECF46381EAF4637EE8F4637C
      E7F46379E5F4576BDFF47581DEA2E6E6F619EDEDED12413D2CE6EED67AFFF2D9
      7CFFF2D97CFFF2D97CFFF2D97CFFF2D97CFFF2D97CFFF2D97CFF2A2C40FF6877
      E8FF788CFFFF38373BFFF2D97CFFF2D97CFFC5B065FECBB668FFF2D97CFFEED6
      7AFFB9A560FD433E31E1BEBEBA480000000000000000F1CAAE7FE5A97BFFCFA0
      78FFD0BDADFFFAFBFCFFF9F9F9FFFAFAFAFFFBFBFBFFFBFBFBFFFBFBFBFFFCFC
      FCFFFCFCFCFFFDFDFDFFFDFDFDFFFDFDFDFFFEFEFEFFFEFEFEFFFFFFFFFFD3C0
      B0FFCFA078FFE5A97BFFF1CAAE7F00000000BFCABF47557B56B34C764EB34C78
      4CB34C7A4DB34C7C4DB34C7E4DB34C7F4DB359A05DAE6BD373FF8CF494FF6ADA
      75FF5AC765FF63C06AFF75B778FF5F8B63BB4C764DB34C744CB34C714CB34C6F
      4CB34C6D4CB34C6C4CB3577657B3B9C3B953F2F2FB0DBCC1EF48A7B0ED69AAB5
      EF69AAB6F169AAB8F269AAB9F369AABBF469AABBF569AABCF669AABDF669AABD
      F669AABDF669AABDF669AABDF669AABCF569AABBF469AABAF369AAB9F269AAB7
      F169AAB6F069A9B3EE67CBD0F339FAFAFD0500000000B6B6B2524B4633E7DAC4
      70FFF2D97CFFF2D97CFFF2D97CFFF2D97CFFF2D97CFF94885CFF202234FF6877
      E8FF788CFFFF2B2B31FF9A8D58FFF2D97CFF3B372AE74A4943C2403D33D94F4B
      42CB807F788FEDEDED12000000000000000000000000F1CAAE7FE7AE82FFD2A6
      81FFD1BFB0FFF9FAFBFFF8F8F8FFF9F9F9FFFAFAFAFFFBFBFBFFFBFBFBFFFBFB
      FBFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFDFDFDFFFDFDFDFFFFFFFFFFD3C2
      B3FFD1A681FFE7AE82FFF1CAAE7F000000000000000000000000000000000000
      000000000000000000000000000000000000688D68975AD064FF95F79CFF71DC
      7BFF62CB6BFF6EC675FF5CA160FF587C58A70000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE01FEFE
      FE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFE
      FE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFEFE01FEFE
      FE01FEFEFE01FEFEFE0100000000000000000000000000000000D6D6D6294E4B
      41CD58533BE4A39458F7F2D97CFFF2D97CFFF2D97CFF6B634CFF282D58FF6675
      E6FF788CFFFF363C60FF746A47FFF2D97CFF6A6141E7D1D1D12E000000000000
      00000000000000000000000000000000000000000000F1CAAE7FE8B288FFD3AB
      8AFFD1C1B4FFF8F9FAFFF7F7F7FFF8F8F8FFF9F9F9FFFAFAFAFFFAFAFAFFFBFB
      FBFFFBFBFBFFFBFBFBFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFEFFFFFFD4C2
      B4FFD3A783FFE7AF84FFF0CAAE80000000000000000000000000000000000000
      0000000000000000000000000000000000006288629D58C961FF95F39CFF73DA
      7DFF66CA6FFF75C77BFF5D9E60FF537853AC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EBEBEB14414139CEE2CB74FFF2D97CFFF2D97CFFF2D97CFF69614BFF343B
      76FF41487BFF726846FFF2D97CFFF2D97CFF635B3EE8E2E2E21D000000000000
      00000000000000000000000000000000000000000000F0C9AE80EAB891FFD6B3
      95FFD2C4B8FFF7F8F8FFF6F6F6FFF7F7F7FFF8F8F8FFF9F9F9FFFAFAFAFFFAFA
      FAFFFAFAFAFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFDFEFEFFD3BF
      B2E6D7A480BFECB690B8F6DFCF51000000000000000000000000000000000000
      0000000000000000000000000000000000006288629D56BE5EFF91ED98FF72D5
      7BFF68C671FF7CC381FF609E63FF537753AC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000908F8B7AB2A05BFFF2D97CFFF2D97CFFF2D97CFFF2D97CFF7067
      50FF706750FFF2D97CFFF2D97CFFF2D97CFF494534DF00000000000000000000
      00000000000000000000000000000000000000000000F2D1B96EE7AA7DF8D29D
      75FFD0BBABFFF6F7F8FFF5F5F5FFF6F6F6FFF7F7F6FFF8F8F8FFF9F9F8FFF9F9
      F9FFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFBFBFBFFFAFAFAFFFCFCFCFFDBDB
      DBA1EFEFEF100000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006286629D52B35AFF8BE492FF6FCE
      78FF6CBD74FF84C088FF639D66FF537653AC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D7D7D728544F36E8F2D97CFFF2D97CFFF2D97CFFF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFFF2D97CFFDEC772FE6B695FAC00000000000000000000
      00000000000000000000000000000000000000000000FCF5F018F7E3D540E5D1
      C250D5CDC6BAF5F5F5FFF3F3F3FFF5F5F4FFF5F5F5FFF6F6F6FFF7F7F7FFF8F8
      F8FFF8F8F8FFF9F9F9FFF9F9F9FFFAFAFAFFFAFAFAFFFAFAFAFFFCFCFCFFDBDB
      DBA0EFEFEF100000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006285629D4FA855FF85DB8BFF6FC3
      78FF73B37AFF8AC28FFF669B68FF537553AC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000827F798D9C8B52F7F2D97CFFF2D97CFFF2D97CFFF2D9
      7CFFF2D97CFFF2D97CFFEFD67BFFF4E5B291F9F9F90600000000000000000000
      000000000000000000000000000000000000000000000000000000000000F4F4
      F40BD8D9D9A1F5F5F4FFF4F4F3FFF5F5F4FFF5F5F5FFF6F6F6FFF7F7F7FFF8F8
      F7FFF8F8F8FFF9F9F8FFF9F9F9FFF9F9F9FFF9F9F9FFFAFAFAFFFCFCFCFFDBDB
      DBA3F0F0F00F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006285629D4F9D54FF8AD18FFF7AB9
      81FF7FB685FF96CC99FF6A996BFF537453AC0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FDFDFD026A6961A7675D3DECE0C973FFF2D97CFFF2D9
      7CFFF2D97CFFAF9D5BFB424034DCDDDDDC250000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F5F5
      F50ADDDDDD61E6E6E69BE5E5E59BE5E5E59BE6E6E69BE6E6E69BE6E6E69BE7E7
      E79BE6E6E79BE7E7E79BE7E7E79BE7E7E79BE7E7E79BE7E7E79BE9E9E89BDFDF
      DF61F5F5F50A0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006184629F94BA97FFA0C5A3FF97BC
      9BFF99BC9CFFA5C6A8FFA9BFABFF547755B00000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0BF42555249C3444235DC4541
      32E248453AD37E7D7692F5F5F50A000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FEFE
      FE01F6F6F609F3F3F30CF3F3F30CF3F3F30CF3F3F30CF3F3F30CF3F3F30CF3F3
      F30CF3F3F30CF3F3F30CF3F3F30CF3F3F30CF3F3F30CF3F3F30CF3F3F30CF6F6
      F609FEFEFE010000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C4CFC53F6D8C6FB2507553B24E71
      50B24E6D4FB2506C52B2778A78B3BBC6BB510000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FDFD
      FD02C0C2E13F838BCB7CC1C3E23EFDFDFD02000000000000000000000000E6E6
      F1199898D06C9D9DD267EAEAF315000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FBFBFC04BDC0
      DF423C5CC6C3093CD0F63D53C2C2BEBFDF41FCFCFD0300000000E7E7F1187E7E
      C7893838C9EB4040CCE88D8DCD7DE9E9F3160000000000000000000000000000
      0000000000000000000000000000FEFEFE01FDFDFD02FCFCFC03FAFAFA05F9F9
      F906F9F9F906FAFAFA05FBFBFB04FCFCFC03FEFEFE01FEFEFE01000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D8D8E9284564
      C2BC014EE6FE0048F0FF012FD9FE3E4CBCC3C0C0DE41E5E5EF1A7C7CC4892C2C
      C5F03939E9FF4A4AF0FF4A4ACEE9A1A1D267000000000000000000000000FCFC
      FC03F6F6F609EFEFEF10E9E9E916E3E3E31CDEDEDE21D9D9D926D4D4D42BD1D1
      D12ED0D0D02FD0D0D02FD3D3D32CD8D8D827DDDDDD22E2E2E21DE8E8E817F0F0
      F00FF8F8F807FDFDFD0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D8D8E9284766
      C2BD0250E5FE0048EFFF0035E7FF011FD1FE3940B6CA5B5BB2AC2121C0F12828
      E0FF3A3AECFF4A4AF0FF4C4CCEEAA2A2D168000000000000000000000000F9F9
      F906F3F3F30CE9E9EC164F4FBDB44D4DBBB6D9D9DB26D6D6D629D2D2D22DCFCF
      CF30CECECE31D0D0D02FD3D3D32CD5D5D72A4D4DBAB74E4EBCB5E5E5E81AEFEF
      EF10F5F5F50AFBFBFB0400000000000000000000000000000000000000000000
      000000000000000000000000000000000000F3F6F40CD1E3D82EE6EEE919FEFE
      FE01000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FAF9F906F9F8F808FCFBFB040000
      000000000000000000000000000000000000ECECEC13AEAEAE51C0C0C03FF9F9
      F906000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFEFE01FEFEFE01FAFAFB05C0C3
      DF414260C4C30541DEFC0035E7FF0021DEFF020FCAFF0809BFFE1616D7FF2828
      E3FF3838E6FF4444CAEB9090CA7FE8E8F2170000000000000000000000000000
      0000FBFBFD045050C2B61E1EB4F51E1EB4F54D4DBFB9EEEEF111F0F0F00FEFEF
      EF10EFEFEF10F0F0F00FEDEDF0124D4DBFB91E1EB4F51E1EB4F55050C3B6FBFB
      FD04000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EDF2EF129BD1B3654EC081BE7BC99E8ADAE8
      E025FEFEFE010000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F2F1F110C4C1C153A59F9F8AA1999991AAA2A285C1BC
      BC61E1DEDE2EF7F7F70A0000000000000000DDDDDD22575757A8383838C7BFBF
      BF40FEFEFE010000000000000000000000000000000000000000000000000000
      00000000000000000000F5F4F40CCDCBCB44ADA7A778AAA3A380B7B1B16FCEC9
      C94CADADC7604358BAC60531D6FC0121DEFF0512D7FF0B0CD5FF1717DAFF2727
      DDFF3B3BC4EB8E8EC580ECECF31400000000000000000000000000000000FBFB
      FD045252C3B62121B5F53838C7FF3939C8FF2222B5F55252C3B6FBFBFD040000
      000000000000FBFBFD045252C3B62222B5F53939C8FF3939C8FF2121B5F55252
      C3B6FBFBFD040000000000000000000000000000000000000000000000000000
      00000000000000000000EAF0EC158FCCA97148C37FDB66DC9DFF5DD090EE71C5
      959AD5E5DB2AFDFDFD0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F2F1F10FA8A4A47F969191E7ACA7A7FBA29696FC8E7B7BFB8B77
      77F2928383D4B3ACAC83E7E5E52400000000FCFCFC03BCBCBC43242424DB6363
      639CEEEEEE110000000000000000000000000000000000000000000000000000
      000000000000F4F3F30DA8A4A479817C7CE08C8787F98D8080FA897575F78A77
      77EA988B89C3817C98AC2336AEEA253CE3FF4E53EFFF5555F0FF4747ECFF2C2C
      C7FC7171B4A0ECECF11400000000000000000000000000000000000000005D5D
      C7AC2323B6F53E3ECAFF3C3CC9FF3D3DCAFF3F3FCBFF2525B8F55353C5B6FBFB
      FD04FBFBFD045353C5B62525B8F54040CBFF3D3DCAFF3D3DCAFF3E3ECAFF2323
      B7F55D5DC7AC0000000000000000000000000000000000000000000000000000
      0000FEFEFE01E3EBE61C80C49B813DBF75E35EDE96FF87F2B8FF97F0C2FF67D1
      96F567BE8BA7C8DED037FBFBFB04000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FDFDFD02BCBAB956938F8EE6B1AFAFFFA2A2A2FFBCB8B8FFB9ABABFFA790
      90FFA68F8FFF9D8C8CF5A79D9DAAE4E1E12A00000000EEEEEE11505050AF3E3E
      3EC1DFDFDF200000000000000000000000000000000000000000000000000000
      0000FEFEFE01C1BFBF4F807B7BE2989696FF999999FFA9A4A4FFAA9A9AFFA78E
      8DFF927F86FF4C4F8AF5364BC7F9737BF8FF8C8CFEFF8B8BFEFF8888FDFF5E5E
      E4FE5B5BBCCCBEBED84AF9F9FA06000000000000000000000000000000005F5F
      C9AC2727B9F54545CDFF4141CDFF3F3FCCFF4242CEFF4747CEFF2929B9F55555
      C5B65555C5B62929B9F54747CEFF4242CEFF4040CDFF4141CDFF4646CEFF2727
      B9F55F5FC9AC000000000000000000000000000000000000000000000000FDFD
      FD02D9E6DD2675BE918C2FBB68EA40D77FFF56E791FF6EEFA6FF8DF5C0FF99F1
      C4FF60CE90F65DBA82B2BDD8C642F9F9F9060000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F9F9FB06F4F4
      F90BE1E0E41F8C878BA9A8A3A7FE929094FF7D7B7FFF969597FFC1B9B9FFB6A3
      A3FFB29C9CFFB7A2A2FFA89999FAAAA2A29CF0EEEE17EDEDED124B4B4BB44040
      40BFE0E0E01F0000000000000000000000000000000000000000FBFBFD04F7F7
      FA08E4E3E61C878284A68D898BFE8D8B8DFF807F81FF959394FFB3AAAAFFA492
      97FF565A94FF3E5BCDFF8792F9FFA6A6FFFF7476EFFF5A5BE5FF9898FCFFA1A1
      FEFF7272E8FE6363BECDBFBFD74BFAFAFB05000000000000000000000000FBFB
      FD045757C7B62C2CBBF54D4DD1FF4747D0FF4444CFFF4747D1FF4F4FD2FF3131
      BFF33131BFF34F4FD2FF4848D1FF4444D0FF4747D0FF4E4ED1FF2C2CBBF55757
      C7B6FBFBFD040000000000000000000000000000000000000000FEFEFE01D3E2
      D82C68B7859922B45BEF22D165FF28DD6BFF3EE37DFF5AE996FF71EFA9FF80F3
      B5FF7EECAFFF4FCB82FA4FB274BFB5D3BF4AF6F7F60900000000000000000000
      00000000000000000000000000000000000000000000F7FBFF08A1C3F0606A95
      DE9B5A82CDAB395EABEA476CBCFF375CAEFF3855A9FF6E7495FFB6B1B0FFC4B6
      B6FFBDAAAAFFBDABABFFC4B3B3FFA79A9AEAC3BEBE60ADADAD521F1F1FE07070
      708FF2F2F20D00000000000000000000000000000000F8FCFF07A7C5F0597192
      DC92607EC8A535519BEA4460ACFF3D59A7FF3C53A2FF727595FFA19CA0FF5F6A
      9EFF486ED1FF9EACFBFFC0C0FFFF8288EEFF3C41B4EF2E2E96ED5C5CD5FFAFAF
      FCFFBDBDFFFF8A8AECFF6F6FC1CDC3C3D9490000000000000000000000000000
      0000FBFBFD045858C8B62F2FBDF55454D4FF4D4DD3FF4949D3FF4D4DD4FF5656
      D6FF5656D6FF4D4DD4FF4949D3FF4D4DD4FF5555D4FF2F2FBDF55858C8B6FBFB
      FD040000000000000000000000000000000000000000FBFBFB04C9DBCE3657AE
      75A918AF51F312C955FF07D24CFF0ED755FF27DD6AFF3FE37FFF51E88DFF5AEA
      95FF5BEA96FF56E38FFF39C670FD42AC67CAA6CBB159F2F4F20D000000000000
      00000000000000000000000000000000000000000000D2ECFF2D45BCFBC91DB8
      F6FF19B1F6FF13A6F5FF0C9BF5FF0794F6FF187AE4FF707C9DFFACA8A6FFCBBF
      BFFFC8B8B8FFC8B8B8FFCCBCBCFFC2B6B6FD827A7ACC353434CE464646B9CBCB
      CB340000000000000000000000000000000000000000D6EDFF2948B9FAC41BAC
      F3FF17A6F3FF129CF2FF0C93F2FF078BF3FF1771E2FF717CA0FF8C8992FF3B5C
      AAFF759CF1FFD1D4FFFF94A0F1FF414CB3FE44446DE32D2C3EE1363685EB7373
      DBFAC7C7FDFFC1C1FEFF6D6DD1F49F9FC6780000000000000000000000000000
      000000000000FBFBFD045A5AC8B63434C0F55B5BD8FF5151D6FF4E4ED6FF5151
      D7FF5151D7FF4E4ED6FF5252D7FF5B5BD8FF3434C0F55A5AC8B6FBFBFD040000
      000000000000000000000000000000000000F7F7F708BDD4C3424DA769B20CA9
      45F708C148FF03C842FF00CC42FF03D249FF0FD756FF21DC65FF30E071FF37E1
      78FF36E177FF2EDF70FF28DA6AFF1FBF5AFD36A65BD299C3A566EEF1EE110000
      00000000000000000000000000000000000000000000B4E0FF4C45C8FFCA3DD3
      FFD92DBEEFE924ADE6FE24A6E7FF1E9DEBFF428BCBFF888B91FFA5A2A2FFCDC2
      C2FFD3C6C6FFD3C6C6FFD5C8C8FFD3C8C8FF766E6EF75A5858AECACACA35FCFC
      FC030000000000000000000000000000000000000000B6E1FF4A47CBFFC93FD7
      FFD92DC1EFEA23ADE4FE24A7E6FF1E9EEAFF438AC9FF888B91FF9D999BFF7E82
      A2FF4C6DC0FF6C8BE7FF4B5FBBFF807EA1FF696061F3605E5DA8A6A6B5616E6E
      B7BA7979DBFD8080DCFB7F7FBDB1D7D7E2320000000000000000000000000000
      00000000000000000000FBFBFD045C5CCAB63B3BC4F36464DCFF5555D9FF5252
      D9FF5252D9FF5555DAFF6464DCFF3B3BC4F35C5CCAB6FBFBFD04000000000000
      000000000000000000000000000000000000B2CDB84D439F5CBC05A238FA01B9
      3CFF01BF37FF03C33CFF09CA46FF11D150FF19D559FF21D961FF2BDC6AFF2FDD
      6DFF29DC69FF1DDA5FFF0ED653FF0DD151FF11B94BFE299E4DDC8EBA9971E9ED
      EA160000000000000000000000000000000000000000EFF8FF10D9EFFD27D1E6
      F62F8395A78E778597F98393A5FF76869AFF79808DFF8B8A8CFFA4A2A2FFCEC4
      C4FFDDD3D3FFDED4D4FFE0D5D5FFDCD3D3FF9F9797D9D0CECE3B000000000000
      00000000000000000000000000000000000000000000EFF8FF10DCF2FE25D7EE
      FA298598A58B6D7C89F98594A3FF798899FF7D828CFF8C8C8CFFA5A1A1FFC6BB
      BCFF9091B1FF5767ABFF9493B4FFD3CACAFF988F8ED9D0CDCD3AFDFDFD02D3D3
      E0367F7FBBA48484BC9FD8D8E32FFEFEFE010000000000000000000000000000
      00000000000000000000FBFBFD045F5FCAB63F3FC6F36A6ADFFF5A5ADCFF5757
      DCFF5757DCFF5B5BDDFF6A6ADFFF3F3FC6F35F5FCAB6FBFBFD04000000000000
      0000000000000000000000000000000000005BAA6EA4079B30F800AF30FF07B7
      34FF1AC047FF30CA5CFF41D26CFF49D775FF3CD76EFF29D562F838D96EFC4EDF
      7EFF4EDF7FFF48DD7AFF3CD970FF2AD360FF19CC52FF0CB543FF1C973FE47EB0
      8981E1E7E21E00000000000000000000000000000000FDFEFF02BBD7F845789E
      E48C4B6FB6C34464ADFC496BB5FF4162AFFF3D5BABFF6B749FFFA9A5A5FFD2C8
      C8FFE8E0E0FFEAE2E2FFEBE3E3FFE1DADAFFA49D9DCCDFDDDD29000000000000
      00000000000000000000000000000000000000000000FDFEFF02C3DBF83D86A7
      E67D5371B1B9445FA0FB516DB0FF4965A8FF445EA4FF6F769BFFA9A5A5FFD3C9
      C8FFE3DCDCFFD9D2D6FFE6DFE0FFDDD6D6FF9E9797CAE1DFDF2700000000FDFD
      FD02ECECF017EDEDF115FEFEFE01000000000000000000000000000000000000
      000000000000FBFBFD045F5FCCB63E3EC6F56E6EE1FF6060DFFF5B5BDEFF6060
      DFFF6060DFFF5B5BDEFF6161DFFF6E6EE1FF3E3EC6F55F5FCCB6FBFBFD040000
      00000000000000000000000000000000000057B76DA807A62CFA1AB33CFF42C4
      60FF5CCF79FF66D584FF66D886FF4DD475FE41D36DDB77E1988B5CDC84B547D8
      76F763DE8AFF68DF8EFF68DE8CFF64DB88FF55D67BFF30C95DFF09AE3BFF1790
      35E870A6788FDAE2DA25FDFDFD020000000000000000E6F4FF195CC1FCAD1CB3
      F7FF19AEF6FF13A6F7FF0D9BF7FF0792F7FF117DF0FF768ABFFFB7B0AFFFD7CD
      CDFFF1EBEBFFF2ECECFFF4EFEFFFDAD4D4FFA29D9DB2EAE9E919000000000000
      00000000000000000000000000000000000000000000E8F4FF175FC1FCA91AAE
      F5FE18A9F3FF14A1F4FF0E97F4FF088FF3FF117BEDFF7488BDFFB9B2B1FFDBD2
      D2FFF1ECECFFF3EEEDFFF4F0EFFFD2CDCDFFA09A9AABECEBEB16000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FBFBFD046161CDB64040C8F57474E3FF6464E1FF5F5FE1FF6565E2FF7777
      E5FF7777E5FF6666E2FF5F5FE1FF6565E2FF7575E4FF4141C8F56161CDB6FBFB
      FD0400000000000000000000000000000000A6DBAF593CB652CD49BE60FE7AD3
      8DFF7FD793FF7BD892FF58D078FE49CF6ED3A0E6B460F1FBF30ECDF3D8326ADB
      8CA652D678F675DE94FF7EDF9AFF7DDE99FF7DDC97FF71D78CFF3FC662FF0DA8
      36FF0F8626F0669C6B99D3DDD32CFDFDFD0200000000CCEAFF3355CCFFB844D8
      FFD53CCCF8DA25B1E5F62BB3F1FF28ACF4FF4AA1E8FFAEB6C2FFC2B9B8FFDED5
      D5FFF6F3F3FFF7F3F3FFF6F4F4FFB3AFAFF6AFABAB77FAFAFA05000000000000
      00000000000000000000000000000000000000000000CDEAFF3256CCFFB743D9
      FFD539CAF7DC22AEE3F92AB2F0FF28ACF5FF499FE8FFADB4C2FFCAC3C2FFE6DF
      DFFFF6F3F3FFF6F3F3FFF5F3F3FFAEA8A8F4B0ACAC71FAFAFA05000000000000
      000000000000000000000000000000000000000000000000000000000000FCFC
      FD046464CDB64444CAF57979E5FF6868E2FF6363E1FF6B6BE2FF7D7DE6FF4A4A
      CCF44A4ACCF47D7DE6FF6C6CE3FF6464E1FF6A6AE2FF7A7AE5FF4444CAF56464
      CDB6FCFCFD04000000000000000000000000F7FBF708B6E2BD4B5EC26FC26AC9
      7CFC87D798FF62CD7BFC54CA70C6B1E8BF4FF8FCF9070000000000000000DBF5
      E22477DA93975AD27BEF85DE9DFF94E1A8FF93DFA6FF93DEA5FF89D99CFF56C7
      6FFF16A334FF0B7E1BF458925AA7C4D2C43B00000000F1F9FF0ED6F1FF2AD5F2
      FF2DBEDAE743617887C38B9EAEFFA4BBCDFFB7C5D3FFCDCCCCFFCABEBEFFEBE6
      E6FFFCFBFBFFFBFAFAFFD3D1D1FE8F8B8BC2D9D7D72F00000000000000000000
      00000000000000000000000000000000000000000000F1F9FF0ED6F1FF2AD5F2
      FF2DB9D6E3485A7181CA899CADFFA4BACDFFB7C4D2FFCFCECFFFD8D0CFFFF2ED
      EDFFFCFAFAFFFBF9F9FFD1CFCFFE938E8EBBDEDCDC2900000000000000000000
      0000000000000000000000000000000000000000000000000000000000006D6D
      D3AC4444CBF57C7CE5FF6B6BE2FF6666E2FF6F6FE3FF8282E6FF4A4ACCF56464
      CFB66464CFB64A4ACCF58383E7FF7070E3FF6868E2FF6D6DE3FF7E7EE5FF4444
      CBF56D6DD3AC00000000000000000000000000000000F9FCF906C0E6C64068C5
      78B851C067F05EC774BCBCE8C643F9FCF9060000000000000000000000000000
      0000E0F5E51F85DA9B8862CF7DE995DEA6FFA8E3B6FFA8E2B5FFA8E1B4FFA0DD
      ACFF6AC77BFF1C9D30FF0D7C18F2679C68980000000000000000000000000000
      0000FAFAFA05918E8D7B837B79F7CEC9C7FFE0DEDCFFD3CACAFFD9CECEFFFCFA
      FAFFFAFAFAFFD1D0D0FF8C8888D7BEBCBC50FBFBFB0400000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F9F9F906908D8B7E877E7DF8CFC9C8FFE1DFDEFFDFD9D9FFE8E1E1FFFDFD
      FDFFF9F9F9FFCECDCDFF8C8989D3C2C0C04BFCFCFC0300000000000000000000
      0000000000000000000000000000000000000000000000000000000000006F6F
      D3AC4646CBF58181E7FF7070E3FF7272E4FF8787E7FF4B4BCCF66666CFB6FCFC
      FD04FCFCFD046666D1B64C4CCCF68888E7FF7373E4FF7272E4FF8484E7FF4747
      CCF56F6FD3AC0000000000000000000000000000000000000000FDFEFD02CAEA
      D03592D69E70C8EACE37FCFDFC03000000000000000000000000000000000000
      000000000000E7F7EA1890DAA17968CC7EE4A2DFAFFFBFE8C7FFBEE7C6FFBEE6
      C5FFB8E3BFFF7DC987FF239930FC509F56AF0000000000000000000000000000
      000000000000DADADA25716F6FB48E8888FCB9B1B1FFBDB3B3FFD4D1D1FFCCCC
      CCFFA09F9FF2888585BABEBCBC4DFAFAFA050000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D8D8D827706D6DBA979191FCC5BEBEFFCEC6C6FFDAD8D8FFCECE
      CEFFA2A1A1F1858383B9C2C0C048FAFAFA050000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FCFC
      FD046969D2B64B4BCDF68888E8FF8B8BE8FF4E4ECDF66969D2B6FCFCFD040000
      000000000000FCFCFD046969D2B64E4ECDF68C8CE9FF8B8BE8FF4C4CCDF66969
      D2B6FCFCFD040000000000000000000000000000000000000000000000000000
      0000FBFDFB04FEFEFE0100000000000000000000000000000000000000000000
      00000000000000000000F0F9F00F9DDBA86A70C97FD9ABDFB4FFD4EED8FFD5EE
      D9FFD4EDD8FFA3D6A7FF51AF58DE8FC892700000000000000000000000000000
      000000000000FEFEFE01CBCBCB357E7D7D9A6D6A6AC9777575CE818080C48785
      85A7A1A0A06EDAD9D927FDFDFD02000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD02C8C8C839817F7F9B716F6FCA787878CE828181C38A89
      89A5A6A5A56ADBDADA26FCFCFC03000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFD046969D2B64E4ECEF54F4FCEF56969D2B6FCFCFD04000000000000
      00000000000000000000FCFCFD046B6BD2B65151CEF54F4FCEF56969D2B6FCFC
      FD04000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F3FAF30CAADDB15C75C680CFB5E0BBFDE2F2
      E3FFB1DBB3FE68B86CD89DD09F69ECF5EC130000000000000000000000000000
      0000000000000000000000000000EDEDED12DADADA25D7D7D728DDDDDD22EBEB
      EB14FCFCFC030000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EDEDED12DADADA25D7D7D728DEDEDE21ECEC
      EC13FCFCFC030000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FCFCFD047474D7AC7474D7ACFCFCFD0400000000000000000000
      0000000000000000000000000000FCFCFD047474D7AC7474D7ACFCFCFD040000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7FBF708B5DEB94E78C27FC888C8
      8BF96BB96FD1A8D5A95AF4F9F40B000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FCFDFC03C0E1C24181C2
      8488B6DBB74BF9FBF90600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FCFDFC03F3F8
      F30CFBFCFB040000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000060000000480000000100010000000000600300000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FC003FFFFFFFFF00FFFFFFFFFC003F00
      0000FF00FFFFFFFFFC003F000000FF00FFFFFFFFFC003F000000FF00FFFFFFFF
      FC003F000000FF00FFFFFFFFFC003F000000FF00FFFFFFFFE00007000000FF00
      FFFFFFFFC00001000000FF00FFC0000380000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008000010000000000000000008000010000
      00000000000001800001000000000000800003800001FF00FFC00003C0003F80
      0001FF00FFFFFFFFF0003F800001FF00FFFFFFFFF8007F800007FF00FFFFFFFF
      F8007F800007FF00FFFFFFFFFC007FE00007FF00FFFFFFFFFC00FFE00007FF00
      FFFFFFFFFF01FFE00007FF00FFFFFFFFFFFFFFFFE0E1FFFFFFFFFFFFFFFFFFFF
      C040FE003FFFFFFFFFFFFFFFC000E00003FFFFFFFFFFFFFFC000E00003FF0FFF
      FF1F0FFF0000F0000FFE07FFFC0307FC0001E01807FC03FFF80107F80003E000
      07F001FFF00087F00001E00007E000FFC00007C00000E00007C0007F80000780
      0000F0000F80003F80000F800000F8001F00001F80000F800000FC003F00000F
      80003F800000FC003F00000780003F800021F8001F00000180003F80003FF000
      0F00000080003F80003FE0000700600080007F80007FE0000780F000F0007FF0
      007FE00007C1F800F800FFF800FFE01807F3FC00F801FFF801FFF03C0FFFFE00
      FE07FFFE07FFF87E1FFFFF01FFFFFFFFFFFFFFFFFFFFFF83FFFFFFFFFFFFFFFF
      FFFFFFC7FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object Conn: TFDConnection
    ConnectionName = 'AutoUpdater'
    Params.Strings = (
      'DriverID=ADS'
      'Alias=AutoUpdater'
      'TableType=ADT'
      'User_Name=AdsSys'
      'Password=lezaM12345$$'
      'ServerTypes=Local')
    Connected = True
    LoginPrompt = False
    AfterConnect = ConnAfterConnect
    AfterDisconnect = ConnAfterDisconnect
    BeforeDisconnect = ConnBeforeDisconnect
    Left = 412
    Top = 208
  end
  object FDPhysADSDriverLink1: TFDPhysADSDriverLink
    Left = 756
    Top = 104
  end
  object tabApplications: TFDTable
    OnCalcFields = tabApplicationsCalcFields
    IndexFieldNames = 'Application'
    Connection = Conn
    UpdateOptions.UpdateTableName = 'Applications'
    TableName = 'Applications'
    Left = 476
    Top = 208
    object tabApplicationsApplication: TStringField
      FieldName = 'Application'
      Origin = 'Application'
      Size = 200
    end
    object tabApplicationsUpdate_Type_ID: TIntegerField
      FieldName = 'Update_Type_ID'
      Origin = 'Update_Type_ID'
    end
    object tabApplicationsPriority_ID: TIntegerField
      FieldName = 'Priority_ID'
      Origin = 'Priority_ID'
    end
    object tabApplicationsIdle_Time: TStringField
      FieldName = 'Idle_Time'
      Origin = 'Idle_Time'
      Size = 8
    end
    object tabApplicationsApp_Directory: TStringField
      FieldName = 'App_Directory'
      Origin = 'App_Directory'
      Size = 100
    end
    object tabApplicationsTotalVersions: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'TotalVersions'
      Calculated = True
    end
    object tabApplicationsTotalDownloads: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'TotalDownloads'
      Calculated = True
    end
    object tabApplicationsLastDownload: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'LastDownload'
      Calculated = True
    end
  end
  object tabUpdates: TFDTable
    OnCalcFields = tabUpdatesCalcFields
    Connection = Conn
    UpdateOptions.UpdateTableName = 'Updates'
    TableName = 'Updates'
    Left = 556
    Top = 208
    object tabUpdatesApplication: TStringField
      FieldName = 'Application'
      Origin = 'Application'
      Size = 200
    end
    object tabUpdatesLastest_Ver_Maj: TIntegerField
      FieldName = 'Lastest_Ver_Maj'
      Origin = 'Lastest_Ver_Maj'
    end
    object tabUpdatesLastest_Ver_Min: TIntegerField
      FieldName = 'Lastest_Ver_Min'
      Origin = 'Lastest_Ver_Min'
    end
    object tabUpdatesLastest_Ver_Rel: TIntegerField
      FieldName = 'Lastest_Ver_Rel'
      Origin = 'Lastest_Ver_Rel'
    end
    object tabUpdatesMin_DB_Req_Maj: TIntegerField
      FieldName = 'Min_DB_Req_Maj'
      Origin = 'Min_DB_Req_Maj'
    end
    object tabUpdatesMin_DB_Req_Min: TIntegerField
      FieldName = 'Min_DB_Req_Min'
      Origin = 'Min_DB_Req_Min'
    end
    object tabUpdatesMin_DB_Req_Rel: TIntegerField
      FieldName = 'Min_DB_Req_Rel'
      Origin = 'Min_DB_Req_Rel'
    end
    object tabUpdatesURL_Download: TStringField
      FieldName = 'URL_Download'
      Origin = 'URL_Download'
      Size = 1000
    end
    object tabUpdatesTargetFileName: TStringField
      FieldName = 'TargetFileName'
      Origin = 'TargetFileName'
      Size = 200
    end
    object tabUpdatesRelease_Notes: TStringField
      FieldName = 'Release_Notes'
      Origin = 'Release_Notes'
      Size = 16000
    end
    object tabUpdatesPath_To_Install: TStringField
      FieldName = 'Path_To_Install'
      Origin = 'Path_To_Install'
      Size = 256
    end
    object tabUpdatesPriority_ID: TIntegerField
      FieldName = 'Priority_ID'
      Origin = 'Priority_ID'
    end
    object tabUpdatesIdle_Time: TStringField
      FieldName = 'Idle_Time'
      Origin = 'Idle_Time'
      Size = 8
    end
    object tabUpdatesInstaller_Type: TIntegerField
      FieldName = 'Installer_Type'
      Origin = 'Installer_Type'
    end
    object tabUpdatesActive: TBooleanField
      FieldName = 'Active'
      Origin = 'Active'
    end
    object tabUpdatesVersion: TStringField
      FieldKind = fkCalculated
      FieldName = 'Version'
      Calculated = True
    end
    object tabUpdatesDBVersion: TStringField
      FieldKind = fkCalculated
      FieldName = 'DBVersion'
      Calculated = True
    end
    object tabUpdatesEnterprise: TStringField
      FieldName = 'Enterprise'
      Origin = 'Enterprise'
      Size = 200
    end
    object tabUpdatesStore: TStringField
      FieldName = 'Store'
      Origin = 'Store'
      Size = 200
    end
    object tabUpdatesComputer_ID: TStringField
      FieldName = 'Computer_ID'
      Origin = 'Computer_ID'
      Size = 200
    end
  end
  object tabComputer: TFDTable
    Active = True
    OnCalcFields = tabComputerCalcFields
    Connection = Conn
    UpdateOptions.UpdateTableName = 'Status_Machine_Updates'
    TableName = 'Status_Machine_Updates'
    Left = 620
    Top = 208
    object tabComputerEnterprise: TStringField
      FieldName = 'Enterprise'
      Origin = 'Enterprise'
      Size = 200
    end
    object tabComputerStore: TStringField
      FieldName = 'Store'
      Origin = 'Store'
      Size = 200
    end
    object tabComputerMachine_Number: TStringField
      FieldName = 'Machine_Number'
      Origin = 'Machine_Number'
      Size = 100
    end
    object tabComputerApplication: TStringField
      FieldName = 'Application'
      Origin = 'Application'
      Size = 200
    end
    object tabComputerCurrent_Ver_Maj: TIntegerField
      FieldName = 'Current_Ver_Maj'
      Origin = 'Current_Ver_Maj'
    end
    object tabComputerCurrent_Ver_Min: TIntegerField
      FieldName = 'Current_Ver_Min'
      Origin = 'Current_Ver_Min'
    end
    object tabComputerCurrent_Ver_Rel: TIntegerField
      FieldName = 'Current_Ver_Rel'
      Origin = 'Current_Ver_Rel'
    end
    object tabComputerRunning_DB_Ver_Maj: TIntegerField
      FieldName = 'Running_DB_Ver_Maj'
      Origin = 'Running_DB_Ver_Maj'
    end
    object tabComputerRunning_DB_Ver_Min: TIntegerField
      FieldName = 'Running_DB_Ver_Min'
      Origin = 'Running_DB_Ver_Min'
    end
    object tabComputerRunning_DB_Ver_Rel: TIntegerField
      FieldName = 'Running_DB_Ver_Rel'
      Origin = 'Running_DB_Ver_Rel'
    end
    object tabComputerUpdate_To_ID: TIntegerField
      FieldName = 'Update_To_ID'
      Origin = 'Update_To_ID'
    end
    object tabComputerPriority_ID: TIntegerField
      FieldName = 'Priority_ID'
      Origin = 'Priority_ID'
    end
    object tabComputerIdle_Time: TStringField
      FieldName = 'Idle_Time'
      Origin = 'Idle_Time'
      Size = 8
    end
    object tabComputerLast_Op: TStringField
      FieldName = 'Last_Op'
      Origin = 'Last_Op'
      Size = 100
    end
    object tabComputerDate_Op: TSQLTimeStampField
      FieldName = 'Date_Op'
      Origin = 'Date_Op'
    end
    object tabComputerVersion: TStringField
      FieldKind = fkCalculated
      FieldName = 'Version'
      Calculated = True
    end
    object tabComputerDBVersion: TStringField
      FieldKind = fkCalculated
      FieldName = 'DBVersion'
      Calculated = True
    end
    object tabComputerComputer_ID: TStringField
      FieldName = 'Computer_ID'
      Origin = 'Computer_ID'
      Size = 200
    end
    object tabComputerExternal_IP: TStringField
      FieldName = 'External_IP'
      Origin = 'External_IP'
      Size = 17
    end
    object tabComputerLocal_IP: TStringField
      FieldName = 'Local_IP'
      Origin = 'Local_IP'
      Size = 17
    end
    object tabComputerComponent_Version: TStringField
      FieldName = 'Component_Version'
      Origin = 'Component_Version'
      Size = 15
    end
    object tabComputerInstall_Ver: TStringField
      FieldName = 'Install_Ver'
      Origin = 'Install_Ver'
    end
    object tabComputerGlobalUpdateVersion: TStringField
      FieldKind = fkCalculated
      FieldName = 'GlobalUpdateVersion'
      Calculated = True
    end
  end
  object qryLog: TFDQuery
    MasterSource = dsComputer
    MasterFields = 'Enterprise;Store;Machine_Number;Application'
    DetailFields = 'Enterprise;Store;Machine_Number'
    Connection = Conn
    FetchOptions.AssignedValues = [evMode, evCache]
    FetchOptions.Mode = fmAll
    FetchOptions.Cache = [fiBlobs, fiMeta]
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate]
    UpdateOptions.EnableDelete = False
    UpdateOptions.EnableInsert = False
    UpdateOptions.EnableUpdate = False
    SQL.Strings = (
      
        'Select Enterprise,  Store, Machine_Number, Application, Current_' +
        'Ver_Maj, Current_Ver_Min, Current_Ver_Rel, '
      
        '       Running_DB_Ver_Maj, Running_DB_Ver_Min, Running_DB_Ver_Re' +
        'l, Desc_Op, Date_Op,'
      
        '       Case When Current_Ver_Maj Is Null And Current_Ver_Min Is ' +
        'Null And Current_Ver_Rel Then '#39#39
      
        '            Else Cast (Current_Ver_Maj As SQL_Varchar) + '#39'.'#39' + C' +
        'ast (Current_Ver_Min As SQL_Varchar) + '#39'.'#39' + Cast (Current_Ver_R' +
        'el As SQL_Varchar)'
      '       End Version,  '
      
        '       Case When Running_DB_Ver_Maj Is Null And Running_DB_Ver_M' +
        'in Is Null And Running_DB_Ver_Rel Then '#39#39
      
        '            Else Cast (Running_DB_Ver_Maj As SQL_Varchar) + '#39'.'#39' ' +
        '+ Cast (Running_DB_Ver_Min As SQL_Varchar) + '#39'.'#39' + Cast (Running' +
        '_DB_Ver_Rel As SQL_Varchar)'
      '       End DBVersion    From Log_Machine_Updates'
      ' Where Upper (Enterprise) = Upper (:Enterprise)'
      '   And Upper (Store) = Upper (:Store)'
      '   And Upper (Machine_Number) = Upper (:Machine_Number)'
      '   And Upper (Application) = Upper (:Application)'
      ' Order By Date_Op Desc')
    Left = 620
    Top = 312
    ParamData = <
      item
        Name = 'ENTERPRISE'
        DataType = ftString
        ParamType = ptInput
        Size = 200
        Value = Null
      end
      item
        Name = 'STORE'
        DataType = ftString
        ParamType = ptInput
        Size = 200
        Value = Null
      end
      item
        Name = 'MACHINE_NUMBER'
        DataType = ftString
        ParamType = ptInput
        Size = 100
        Value = Null
      end
      item
        Name = 'APPLICATION'
        DataType = ftString
        ParamType = ptInput
        Size = 200
        Value = Null
      end>
  end
  object tabLog: TFDTable
    OnCalcFields = tabComputerCalcFields
    Connection = Conn
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate]
    UpdateOptions.EnableDelete = False
    UpdateOptions.EnableInsert = False
    UpdateOptions.EnableUpdate = False
    UpdateOptions.UpdateTableName = 'Log_Machine_Updates'
    TableName = 'Log_Machine_Updates'
    Left = 684
    Top = 208
    object tabLogEnterprise: TStringField
      FieldName = 'Enterprise'
      Origin = 'Enterprise'
      Size = 200
    end
    object tabLogStore: TStringField
      FieldName = 'Store'
      Origin = 'Store'
      Size = 200
    end
    object tabLogMachine_Number: TStringField
      FieldName = 'Machine_Number'
      Origin = 'Machine_Number'
      Size = 100
    end
    object tabLogApplication: TStringField
      FieldName = 'Application'
      Origin = 'Application'
      Size = 200
    end
    object tabLogCurrent_Ver_Maj: TIntegerField
      FieldName = 'Current_Ver_Maj'
      Origin = 'Current_Ver_Maj'
    end
    object tabLogCurrent_Ver_Min: TIntegerField
      FieldName = 'Current_Ver_Min'
      Origin = 'Current_Ver_Min'
    end
    object tabLogCurrent_Ver_Rel: TIntegerField
      FieldName = 'Current_Ver_Rel'
      Origin = 'Current_Ver_Rel'
    end
    object tabLogRunning_DB_Ver_Maj: TIntegerField
      FieldName = 'Running_DB_Ver_Maj'
      Origin = 'Running_DB_Ver_Maj'
    end
    object tabLogRunning_DB_Ver_Min: TIntegerField
      FieldName = 'Running_DB_Ver_Min'
      Origin = 'Running_DB_Ver_Min'
    end
    object tabLogRunning_DB_Ver_Rel: TIntegerField
      FieldName = 'Running_DB_Ver_Rel'
      Origin = 'Running_DB_Ver_Rel'
    end
    object tabLogDesc_Op: TStringField
      FieldName = 'Desc_Op'
      Origin = 'Desc_Op'
      Size = 100
    end
    object tabLogDate_Op: TSQLTimeStampField
      FieldName = 'Date_Op'
      Origin = 'Date_Op'
    end
    object tabLogVersion: TStringField
      FieldKind = fkCalculated
      FieldName = 'Version'
      Calculated = True
    end
    object tabLogDBVersion: TStringField
      FieldKind = fkCalculated
      FieldName = 'DBVersion'
      Calculated = True
    end
    object tabLogComputer_ID: TStringField
      FieldName = 'Computer_ID'
      Origin = 'Computer_ID'
      Size = 200
    end
    object tabLogExternal_IP: TStringField
      FieldName = 'External_IP'
      Origin = 'External_IP'
      Size = 17
    end
    object tabLogLocal_IP: TStringField
      FieldName = 'Local_IP'
      Origin = 'Local_IP'
      Size = 17
    end
    object tabLogComponent_Version: TStringField
      FieldName = 'Component_Version'
      Origin = 'Component_Version'
      Size = 15
    end
  end
  object dsApplications: TDataSource
    DataSet = tabApplications
    OnDataChange = dsApplicationsDataChange
    Left = 476
    Top = 256
  end
  object dsUpdates: TDataSource
    DataSet = tabUpdates
    Left = 544
    Top = 248
  end
  object dsComputer: TDataSource
    DataSet = tabComputer
    Left = 620
    Top = 240
  end
  object dsLog: TDataSource
    DataSet = tabLog
    Left = 684
    Top = 256
  end
  object dsLogDetail: TDataSource
    DataSet = qryLog
    Left = 620
    Top = 360
  end
  object tabUpdateType: TFDTable
    Connection = Conn
    UpdateOptions.UpdateTableName = 'Update_Type'
    TableName = 'Update_Type'
    Left = 44
    Top = 248
  end
  object dsUpdateType: TDataSource
    DataSet = tabUpdateType
    Left = 44
    Top = 304
  end
  object tabPriority: TFDTable
    Connection = Conn
    UpdateOptions.UpdateTableName = 'Update_Priority'
    TableName = 'Update_Priority'
    Left = 116
    Top = 248
  end
  object dsPriority: TDataSource
    DataSet = tabPriority
    Left = 116
    Top = 304
  end
  object qryAppUpdateType: TFDQuery
    Connection = Conn
    SQL.Strings = (
      'Select *'
      '  From Update_Type'
      ' Where Update_Type_ID > 0'
      ' Order By Update_Type_ID                   ')
    Left = 188
    Top = 248
  end
  object dsAppUpdateType: TDataSource
    DataSet = qryAppUpdateType
    Left = 188
    Top = 304
  end
  object tabInstallers: TFDTable
    Connection = Conn
    UpdateOptions.UpdateTableName = 'Update_Installer_Type'
    TableName = 'Update_Installer_Type'
    Left = 268
    Top = 248
  end
  object dsInstallers: TDataSource
    DataSet = tabInstallers
    Left = 268
    Top = 304
  end
  object TimerLog: TTimer
    Enabled = False
    OnTimer = TimerLogTimer
    Left = 148
    Top = 112
  end
  object TimerMachine: TTimer
    Enabled = False
    OnTimer = TimerMachineTimer
    Left = 204
    Top = 112
  end
  object qryUpdatesByApp: TFDQuery
    Connection = Conn
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate]
    UpdateOptions.EnableDelete = False
    UpdateOptions.EnableInsert = False
    UpdateOptions.EnableUpdate = False
    SQL.Strings = (
      
        'Select Application, URL_Download, TargetFileName, Release_Notes,' +
        ' Path_To_Install, Priority_ID, Idle_Time, Installer_Type, Active' +
        ', '
      '       Lastest_Ver_Maj, Lastest_Ver_Min, Lastest_Ver_Rel, '
      
        '       Case When Lastest_Ver_Maj Is Null And Lastest_Ver_Min Is ' +
        'Null And Lastest_Ver_Rel Then '#39#39
      
        '            Else Cast (Lastest_Ver_Maj As SQL_Varchar) + '#39'.'#39' + C' +
        'ast (Lastest_Ver_Min As SQL_Varchar) + '#39'.'#39' + Cast (Lastest_Ver_R' +
        'el As SQL_Varchar)'
      '       End Version,  '
      
        '       Case When Min_DB_Req_Maj Is Null And Min_DB_Req_Min Is Nu' +
        'll And Min_DB_Req_Rel Then '#39#39
      
        '            Else Cast (Min_DB_Req_Maj As SQL_Varchar) + '#39'.'#39' + Ca' +
        'st (Min_DB_Req_Min As SQL_Varchar) + '#39'.'#39' + Cast (Min_DB_Req_Rel ' +
        'As SQL_Varchar)'
      '       End DBVersion,'
      '       Enterprise, Store, Computer_ID  '
      '  From Updates'
      'Order By Lastest_Ver_Maj, Lastest_Ver_Min, Lastest_Ver_Rel')
    Left = 500
    Top = 328
  end
  object dsUpdatesByApp: TDataSource
    DataSet = qryUpdatesByApp
    Left = 476
    Top = 384
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 304
    Top = 116
    PixelsPerInch = 96
    object RowEven: TcxStyle
    end
    object RowOdd: TcxStyle
      AssignedValues = [svColor]
      Color = 15853276
    end
    object Header: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnShadow
    end
    object Selection: TcxStyle
      AssignedValues = [svColor]
      Color = clHighlight
    end
    object ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnFace
    end
  end
  object qryCalcAppTotVersions: TFDQuery
    Connection = Conn
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    SQL.Strings = (
      'Select Count (*) Total'
      '  From Updates'
      ' Where Upper (Application) = Upper (:App)')
    Left = 772
    Top = 176
    ParamData = <
      item
        Name = 'APP'
        ParamType = ptInput
      end>
  end
  object qryCalcAppDownladsSummary: TFDQuery
    Connection = Conn
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    SQL.Strings = (
      'Select Count (*) Total, Max (Date_Op) Last_Op  '
      '  From Log_Machine_Updates'
      ' Where Desc_Op = '#39'DOWNLOADED'#39
      '   And Upper (Application) = Upper (:App) ')
    Left = 772
    Top = 232
    ParamData = <
      item
        Name = 'APP'
        ParamType = ptInput
      end>
  end
  object SelectDirDlg: TJvBrowseForFolderDialog
    Title = 'Select app root directory'
    Left = 708
    Top = 160
  end
  object qryUpdatesByAppMain: TFDQuery
    OnCalcFields = qryUpdatesByAppMainCalcFields
    Connection = Conn
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate]
    UpdateOptions.EnableDelete = False
    UpdateOptions.EnableInsert = False
    UpdateOptions.EnableUpdate = False
    SQL.Strings = (
      
        'Select U.Lastest_Ver_Maj, U.Lastest_Ver_Min, U.Lastest_Ver_Rel, ' +
        'U.Min_DB_Req_Maj, U.Min_DB_Req_Min, U.Min_DB_Req_Rel,  '
      
        '       U.TargetFileName, U.Path_To_Install, P.Update_Priority_De' +
        'sc, U.Idle_Time, I.Installer_Desc,'
      
        '       U.Active, U.Enterprise, U.Store, U.Computer_ID, Count (M.' +
        'Application) Total_Updates'
      '  From            Updates                U'
      
        '  Left Outer Join Update_Priority        P  On     U.Priority_ID' +
        '         = P.Update_Priority_ID '
      
        '  Left Outer Join Update_Installer_Type  I  On     U.Installer_T' +
        'ype      = I.Installer_Type'
      
        '  Left Outer Join Status_Machine_Updates M  On     Upper (U.Appl' +
        'ication) = Upper(M.Application)'
      
        '                                               And U.Lastest_Ver' +
        '_Maj     = M.Current_Ver_Maj'
      
        '                                               And U.Lastest_Ver' +
        '_Min     = M.Current_Ver_Min'
      
        '                                               And U.Lastest_Ver' +
        '_Rel     = M.Current_Ver_Rel'
      ' Where Upper (U.Application) = Upper (:App)'
      
        ' Group By U.Lastest_Ver_Maj, U.Lastest_Ver_Min, U.Lastest_Ver_Re' +
        'l, U.Min_DB_Req_Maj, U.Min_DB_Req_Min, U.Min_DB_Req_Rel,  '
      
        '       U.TargetFileName, U.Path_To_Install, P.Update_Priority_De' +
        'sc, U.Idle_Time, I.Installer_Desc, U.Active, U.Enterprise, U.Sto' +
        're, U.Computer_ID')
    Left = 372
    Top = 268
    ParamData = <
      item
        Name = 'APP'
        DataType = ftString
        ParamType = ptInput
        Value = 'App1.exe'
      end>
    object qryUpdatesByAppMainLastest_Ver_Maj: TIntegerField
      FieldName = 'Lastest_Ver_Maj'
      Origin = 'Lastest_Ver_Maj'
    end
    object qryUpdatesByAppMainLastest_Ver_Min: TIntegerField
      FieldName = 'Lastest_Ver_Min'
      Origin = 'Lastest_Ver_Min'
    end
    object qryUpdatesByAppMainLastest_Ver_Rel: TIntegerField
      FieldName = 'Lastest_Ver_Rel'
      Origin = 'Lastest_Ver_Rel'
    end
    object qryUpdatesByAppMainMin_DB_Req_Maj: TIntegerField
      FieldName = 'Min_DB_Req_Maj'
      Origin = 'Min_DB_Req_Maj'
    end
    object qryUpdatesByAppMainMin_DB_Req_Min: TIntegerField
      FieldName = 'Min_DB_Req_Min'
      Origin = 'Min_DB_Req_Min'
    end
    object qryUpdatesByAppMainMin_DB_Req_Rel: TIntegerField
      FieldName = 'Min_DB_Req_Rel'
      Origin = 'Min_DB_Req_Rel'
    end
    object qryUpdatesByAppMainTargetFileName: TStringField
      FieldName = 'TargetFileName'
      Origin = 'TargetFileName'
      Size = 200
    end
    object qryUpdatesByAppMainPath_To_Install: TStringField
      FieldName = 'Path_To_Install'
      Origin = 'Path_To_Install'
      Size = 256
    end
    object qryUpdatesByAppMainUpdate_Priority_Desc: TStringField
      FieldName = 'Update_Priority_Desc'
      Origin = 'Update_Priority_Desc'
      Size = 100
    end
    object qryUpdatesByAppMainIdle_Time: TStringField
      FieldName = 'Idle_Time'
      Origin = 'Idle_Time'
      Size = 8
    end
    object qryUpdatesByAppMainInstaller_Desc: TStringField
      FieldName = 'Installer_Desc'
      Origin = 'Installer_Desc'
    end
    object qryUpdatesByAppMainActive: TBooleanField
      FieldName = 'Active'
      Origin = 'Active'
    end
    object qryUpdatesByAppMainTotal_Updates: TIntegerField
      FieldName = 'Total_Updates'
      Origin = 'Total_Updates'
    end
    object qryUpdatesByAppMainVersion: TStringField
      FieldKind = fkCalculated
      FieldName = 'Version'
      Calculated = True
    end
    object qryUpdatesByAppMainDBVersion: TStringField
      FieldKind = fkCalculated
      FieldName = 'DBVersion'
      Calculated = True
    end
    object qryUpdatesByAppMainEnterprise: TStringField
      FieldName = 'Enterprise'
      Origin = 'Enterprise'
      Size = 200
    end
    object qryUpdatesByAppMainStore: TStringField
      FieldName = 'Store'
      Origin = 'Store'
      Size = 200
    end
    object qryUpdatesByAppMainComputer_ID: TStringField
      FieldName = 'Computer_ID'
      Origin = 'Computer_ID'
      Size = 200
    end
  end
  object dsUpdatesByAppMain: TDataSource
    DataSet = qryUpdatesByAppMain
    Left = 356
    Top = 316
  end
  object tabJustMajorUpdates: TFDTable
    Active = True
    IndexFieldNames = 'Enterprise;Store;Application'
    Connection = Conn
    UpdateOptions.UpdateTableName = 'Just_Major_Updates'
    TableName = 'Just_Major_Updates'
    Left = 44
    Top = 360
  end
  object dsJustMajorUpdates: TDataSource
    DataSet = tabJustMajorUpdates
    Left = 44
    Top = 408
  end
  object AlerterMachineInsert: TFDEventAlerter
    Connection = Conn
    Names.Strings = (
      'Status_Machine_Insert')
    Options.Kind = 'Events'
    Options.Synchronize = False
    Options.MergeData = dmNone
    Options.AutoRefresh = afNone
    OnAlert = AlerterMachineInsertAlert
    Left = 52
    Top = 160
  end
  object AlerterMachineUpdate: TFDEventAlerter
    Connection = Conn
    Names.Strings = (
      'Status_Machine_Update')
    Options.Kind = 'Events'
    Options.Synchronize = False
    Options.MergeData = dmNone
    Options.AutoRefresh = afNone
    OnAlert = AlerterMachineInsertAlert
    Left = 44
    Top = 208
  end
  object AlerterLog: TFDEventAlerter
    Connection = Conn
    Names.Strings = (
      'Log_Machine')
    Options.Kind = 'Events'
    Options.Synchronize = False
    Options.MergeData = dmNone
    Options.AutoRefresh = afNone
    OnAlert = AlerterLogAlert
    Left = 156
    Top = 208
  end
  object AlerterMachineDelete: TFDEventAlerter
    Connection = Conn
    Names.Strings = (
      'Status_Machine_Delete')
    Options.Kind = 'Events'
    Options.Synchronize = False
    Options.MergeData = dmNone
    Options.AutoRefresh = afNone
    OnAlert = AlerterMachineInsertAlert
    Left = 156
    Top = 160
  end
end
