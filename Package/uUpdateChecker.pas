////////////////////////////////////////////////////////////////////////////////
//
// History
//
// Version 1.0.1
// The calls to the web service change, instead of sending data separately, a
// single string containing a JSON is sent to avoid future modifications in the
// call headers.
//
// To record computer information, now always send: BIOS Serial Number (as
// Computer ID), Enterprise, Store, ComputerNumber, External IP, LAN IP and
// Component Version.
//
// Version 1.0.2
// Delete Updates.ini after an effective update.
//
// Version 1.0.1
// Use a new web service interface, and send new fields: Computer_ID, External_IP
// and Local_IP.
//
// Version 1.0.0
// First release
//
////////////////////////////////////////////////////////////////////////////////
unit uUpdateChecker;

interface
uses System.Classes, System.SysUtils, uMinSecTimer, uIAutoUpdater, System.Variants,
  Windows, ActiveX, Vcl.ExtCtrls, ShellAPI, URLMon, uMemoryIniStructure, Vcl.Forms,
  Vcl.AppEvnts, System.IOUtils, System.JSON, Registry, TypInfo;

const
  coLower   = -1;
  coEqual   =  0;
  coGreater =  1;

  coURLTestExternalIP = 'http://ipinfo.io/json';
//  coWSAutoUpdater     = 'http://localhost/WSAutoUpdater/WSAutoUpdater.dll/soap/IAutoUpdater';
  coWSAutoUpdater     = 'http://connect.compudime.com:8080/WSAutoUpdater/WSAutoUpdater.dll/soap/IAutoUpdater';

type
  TUpdatePriority = (upOnProgramExit, upNow, upOnIdle);
  TThreadState = (tsNone, tsRunning, tsTerminated);
  TThreadCommand = (tcNone, tcGetExternalIP, tcGetVersionInfo, tcDownloadFile, tcLogDownloadDone, tcLogUpdatedDone);
  TUpdaterVerboseLevel = (uvlDisabled, uvlError, uvlInfo, uvlDebug);
  TDownloadStatus = (dsNone, dsStartDownloading, dsFinishedDownloading, dsDownloadError);

  TUpdateChecker = class;
  TUpdateCheckerThread = class (TThread)
  private
    FUpdateChecker:    TUpdateChecker;
    FDownloadDir:      string;
    FProgramName:      string;
    FWebServiceURL:    string;
    FCommand:          TThreadCommand;
    FEcho:             string;
    FText:             string;
    FVersion:          string;
    FURLDownload:      string;
    FReleaseNotes:     string;
    FTargetFileName:   string;
    FPathToInstall:    string;
    FEnterprise:       string;
    FStore:            string;
    FComputerNumber:   string;
    FExternalIP:       string;
    FLocalIP:          string;
    FComputerID:       string;
    FComponentVersion: string;
    FUpdatePriority:   TUpdatePriority;
    FIdleTime:         string;
    FCurrMajor:        integer;
    FCurrMiner:        integer;
    FCurrRelease:      integer;
    FDBMajor:          integer;
    FDBMiner:          integer;
    FDBRelease:        integer;
    FInstallerType:    string;
    FErrMsg:           string;
    FVerboseLevel:     TUpdaterVerboseLevel;
    FMsgLog:           string;
    procedure Log (Level: TUpdaterVerboseLevel; msg: string);
    procedure SyncLog;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Execute; override;
    property UpdaterChecker: TUpdateChecker read FUpdateChecker write FUpdateChecker;
    property ProgramName: string read FProgramName write FProgramName;
    property Command: TThreadCommand read FCommand write FCommand;
    property Echo: string read FEcho write FEcho;
    property Text: string read FText write FText;
    property DownloadDir: string read FDownloadDir write FDownloadDir;
    property Version: string read FVersion;
    property URLDownload: string read FURLDownload;
    property ReleaseNotes: string read FReleaseNotes;
    property TargetFileName: string read FTargetFileName;
    property PathToInstall: string read FPathToInstall;
    property Enterprise: string read FEnterprise;
    property ComputerNumber: string read FComputerNumber;
    property UpdatePriority: TUpdatePriority read FUpdatePriority;
    property IdleTime: string read FIdleTime;
    property InstallerType: string read FInstallerType;
    property ErrMsg: string read FErrMsg;
  end;

  TOnFoundNewAppVersionEvent = procedure (Sender: TObject; ProgramName, Version, URLDownload, ReleaseNotes, TargetFileName, PathToInstall, InstallerType: string; var StartDownload: boolean) of object;
  TOnDownloadEvent = procedure (Sender: TObject; ProgramName, Version, URLDownload, TargetFileName, PathToInstall: string) of object;
  TOnDownloadErrorEvent = procedure (Sender: TObject; ProgramName, Version, URLDownload, TargetFileName, PathToInstall, ErrMsg: string) of object;
  TOnFirstRunNewVersionEvent = procedure (Sender: TObject; ReleaseNotes: string; var MarkAsDisplayed: boolean) of object;
  TOnUpdateError = procedure (Sender: TObject; ErrorMsg: string) of object;


  /// <summary>
  ///   <para>
  ///     <c>TUpdateChecker</c> is a component created to give any
  ///     application written in Delphi the capabilities of AutoUpdater, that
  ///     is, to search for updates on a centralized server, download them,
  ///     and perform update tasks, without the need for the programmer to
  ///     take care of any of these tasks.
  ///   </para>
  ///   <para>
  ///     The URL for the web service is defined in the <see cref="uUpdateChecker|TUpdateChecker.WebServiceURL">
  ///     WebServiceURL</see> property, and by default it has the value of:
  ///   </para>
  ///   <para>
  ///     <see href="http://connect.compudime.com:8080/WSAutoUpdater/WSAutoUpdater.dll/soap/IAutoUpdater" />
  ///   </para>
  ///   <para>
  ///     You can perform a single search for updates with method <see cref="uUpdateChecker|TUpdateChecker.CheckForUpdate">
  ///     CheckForUpdate</see>, or several from time to time with methods <see cref="uUpdateChecker|TUpdateChecker.BeginCheckUpdate">
  ///     BeginCheckUpdate</see>, <see cref="uUpdateChecker|TUpdateChecker.EndCheckUpdate">
  ///     EndCheckUpdate</see>, and property <see cref="uUpdateChecker|TUpdateChecker.Interval">
  ///     Interval</see>.
  ///   </para>
  ///   <para>
  ///     The component has several events that notify about relevant events
  ///     of the updates, such as:
  ///   </para>
  ///   <list type="bullet">
  ///     <item>
  ///       <see cref="uUpdateChecker|TUpdateChecker.OnFoundNewAppVersion">
  ///       OnFoundNewAppVersion</see>.
  ///     </item>
  ///     <item>
  ///       <see cref="uUpdateChecker|TUpdateChecker.OnStartDownload">
  ///       OnStartDownload</see>.
  ///     </item>
  ///     <item>
  ///       <see cref="uUpdateChecker|TUpdateChecker.OnFinishDownload">
  ///       OnFinishDownload</see>.
  ///     </item>
  ///     <item>
  ///       <see cref="uUpdateChecker|TUpdateChecker.OnFirstRunNewVersion">
  ///       OnFirstRunNewVersion</see>.
  ///     </item>
  ///     <item>
  ///       <see cref="uUpdateChecker|TUpdateChecker.OnNotifyMustExitForUpdate">
  ///       OnNotifyMustExitForUpdate</see>.
  ///     </item>
  ///   </list>
  ///   <para>
  ///     <c>TUpdateChecker</c> cannot close the host application, due to
  ///     loss of information. Instead, it notifies the user when the
  ///     criteria to close the application and perform update operations
  ///     have been met by the <see cref="uUpdateChecker|TUpdateChecker.OnNotifyMustExitForUpdate">
  ///     OnNotifyMustExitForUpdate</see> event.
  ///   </para>
  /// </summary>
  /// <remarks>
  ///   TUpdateChecker requires the programmer to assign the database version
  ///   in the <see cref="uUpdateChecker|TUpdateChecker.DBVersion">DBVersion</see>
  ///    property so that the search for new versions is only on the database
  ///   versions supported by the host application.
  /// </remarks>
  /// <seealso cref="WebServiceURL" />
  /// <seealso cref="CheckForUpdate" />
  /// <seealso cref="BeginCheckUpdate" />
  /// <seealso cref="Interval" />
  /// <seealso cref="OnFoundNewAppVersion" />
  /// <seealso cref="OnStartDownload" />
  /// <seealso cref="OnFinishDownload" />
  /// <seealso cref="OnFirstRunNewVersion" />
  /// <seealso cref="OnNotifyMustExitForUpdate" />
  /// <seealso cref="DBVersion" />
  TUpdateChecker = class (TComponent)
  private
    FApplication:               TApplication;
    FIsProgramFilesDir:         boolean;
    FRootDir:                   string;
    FDownloadDir:               string;
    FLogDir:                    string;
    FProgramName:               string;
    FDBVersion:                 string;
    FEnterprise:                string;
    FStore:                     string;
    FExternalIP:                string;
    FComputerNumber:            string;
    FLocalIP:                   string;
    FVersion:                   string;
    FURLDownload:               string;
    FReleaseNotes:              string;
    FTargetFileName:            string;
    FInterval:                  string;
    FCompany:                   string;
    FAppDirectory:              string;
    FEnabled:                   boolean;
    FEncrypted:                 boolean;
    FEncryptionKey:             string;
    FPathToInstall:             string;
    FInstallerType:             string;
    FAutoUpdate:                boolean;
    FUpdateForInstall:          boolean;
    FMinTimer:                  TMinSecTimer;
    FThread:                    TUpdateCheckerThread;
    FWebServiceURL:             string;
    FText:                      string;
    FEcho:                      string;
    FTimer:                     TTimer;
    FThreadTerminated:          TThreadState;
    FInstalledVersion:          string;
    FReleaseNotesFileName:      string;
    FUpdatesIniFileName:        string;
    FIdleInterval:              string;
    FComponentVersion:          string;
    FComputerID:                string;
    FVerboseLevel:              TUpdaterVerboseLevel;
    FIdleSeconds:               DWord;
    FIdleNotified:              boolean;
    FDownloadFinished:          boolean;
    FExitNotified:              boolean;
    FDownloadFinishedTick:      Cardinal;
    FUpdatePriority:            TUpdatePriority;
    FDownloadStatus:            TDownloadStatus;
    FOnAfterCheck:              TNotifyEvent;
    FOnFoundNewAppVersion:      TOnFoundNewAppVersionEvent;
    FOnStartDownload:           TOnDownloadEvent;
    FOnFinishDownload:          TOnDownloadEvent;
    FOnErrorDownload:           TOnDownloadErrorEvent;
    FOnFirstRunNewVersion:      TOnFirstRunNewVersionEvent;
    FOnNotifyMustExitForUpdate: TNotifyEvent;
    FOnUpdateError:             TOnUpdateError;
    function GetInterval: string;
    procedure SetInterval (Value: string);
    procedure SetWebServiceURL (Value: string);
    function GetEnabled: boolean;
    procedure SetReleaseNotesFileName (Value: string);
    procedure SetUpdatesIniFileName (Value: string);
    procedure SetEnabled (Value: boolean);
    procedure SetIdleInterval (Value: string);
    procedure SetUpdatePriority (Value: TUpdatePriority);
    procedure SetDBVersion (Value: string);
    procedure SetCompany (Value: string);
    procedure SetAppDirectory (Value: string);
    procedure OnTimerMinSec (Sender: TObject);
    procedure OnTimer (Sender: TObject);
    procedure OnThreadTerminated (Sender: TObject);
    procedure CreateThreadCheckForUpdate;
    function SecondsIdle: DWord;
    procedure SaveIniFile;
    procedure SearchApplication;
    procedure DeletePreviousLogs;
    procedure Log (Level: TUpdaterVerboseLevel; msg: string);
    procedure InternalCheckForUpdate (ForceCheck: boolean);
    procedure ReadIniValues (var AVersion, AInstaller, APathToInstall, AInstallerType: string; var AFirst: boolean);
    function GetLogEnterprise: string;
    function GetLogStore: string;
    function GetLogComputerName: string;
    function IsValidIPOrVersionFormat (s: string): boolean;
    procedure VerifyDirectories;
    procedure ExtractVersionSegments (s: string; var Major, Miner, Release, Build: integer);
    procedure UpdateItself (var Successful: boolean; var ErrMsg: string);
    procedure UpdateReplacement (FileName: string; Path: string = ''; TimeOut: cardinal = 3);
    function GetComputerID: string;
    function SyncEcho (s: string; var errMsg: string): string;
    property Enabled: boolean read GetEnabled write SetEnabled;
    property Text: string read FText write FText;
    property Echo: string read FEcho write FEcho;
  public
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;

    /// <summary>
    ///   Starts the process of searching for updates in the web service,
    ///   searches are performed according to the time specified in the <see cref="uUpdateChecker|TUpdateChecker.Interval">
    ///   Interval</see> property.
    /// </summary>
    /// <seealso cref="Interval" />
    /// <seealso cref="EndCheckUpdate" />
    /// <seealso cref="CheckForUpdate" />
    /// <seealso cref="Update" />
    procedure BeginCheckUpdate;

    /// <summary>
    ///   Stops the process of searching for updates in the web service started by
    ///   method <see cref="uUpdateChecker|TUpdateChecker.BeginCheckUpdate">
    ///   BeginCheckUpdate</see> ends. <br />
    /// </summary>
    /// <seealso cref="Interval" />
    /// <seealso cref="BeginCheckUpdate" />
    /// <seealso cref="CheckForUpdate" />
    /// <seealso cref="Update" />
    procedure EndCheckUpdate;

    /// <summary>
    ///   It performs a single search process for updates to the web service.
    /// </summary>
    /// <seealso cref="EndCheckUpdate" />
    /// <seealso cref="CheckForUpdate" />
    /// <seealso cref="Update" />
    procedure CheckForUpdate;

    /// <summary>
    ///   If an update has been carried out successfully, proceed to start the
    ///   tasks to apply it.
    /// </summary>
    /// <remarks>
    ///   It is important that this method is invoked at the end of the OnClose
    ///   event in the main way, when all the tasks of saving the information
    ///   of the host application have already been carried out correctly to
    ///   prevent data loss or inconsistencies.
    /// </remarks>
    /// <example>
    ///  <code lang="Delphi">
    ///   procedure TForm1.FormClose(Sender: TObject; var Action:
    ///   TCloseAction); <br />begin <br />if Updater.UpdateForInstall then <br />
    ///   Updater.Update; <br />end; <br />
    ///  </code>
    /// </example>
    /// <seealso cref="UpdateForInstall" />
    procedure Update;

    /// <summary>
    ///   When a new update is available, it contains the new version to
    ///   install
    /// </summary>
    /// <seealso cref="URLDownload" />
    /// <seealso cref="ReleaseNotes" />
    /// <seealso cref="TargetFileName" />
    /// <seealso cref="PathToInstall" />
    /// <seealso cref="UpdatePriority" />
    /// <seealso cref="IdleInterval" />
    property Version: string read FVersion;

    /// <summary>
    ///   When a new update is available, contains the download URL of the
    ///   update file
    /// </summary>
    /// <seealso cref="Version" />
    /// <seealso cref="ReleaseNotes" />
    /// <seealso cref="TargetFileName" />
    /// <seealso cref="PathToInstall" />
    /// <seealso cref="UpdatePriority" />
    /// <seealso cref="IdleInterval" />
    property URLDownload: string read FURLDownload;

    /// <summary>
    ///   When a new update is available, contains the release notes with the
    ///   relevant detailed information, so that it can be displayed to the end
    ///   user in the GUI.
    /// </summary>
    /// <seealso cref="Version" />
    /// <seealso cref="URLDownload" />
    /// <seealso cref="TargetFileName" />
    /// <seealso cref="PathToInstall" />
    /// <seealso cref="UpdatePriority" />
    /// <seealso cref="IdleInterval" />
    property ReleaseNotes: string read FReleaseNotes;

    /// <summary>
    ///   When a new update is available, contains the name of the installer or
    ///   the file to replace, depending on the type of installation specified
    ///   in the <see cref="uUpdateChecker|TUpdateChecker.InstallerType">
    ///   InstallerType</see> property <br />
    /// </summary>
    /// <seealso cref="Version" />
    /// <seealso cref="URLDownload" />
    /// <seealso cref="ReleaseNotes" />
    /// <seealso cref="PathToInstall" />
    /// <seealso cref="UpdatePriority" />
    /// <seealso cref="IdleInterval" />
    /// <seealso cref="InstallerType" />
    property TargetFileName: string read FTargetFileName;

    /// <summary>
    ///   When a new update is available, contains the relative path where the
    ///   update will be applied. <br />
    /// </summary>
    /// <seealso cref="Version" />
    /// <seealso cref="URLDownload" />
    /// <seealso cref="ReleaseNotes" />
    /// <seealso cref="TargetFileName" />
    /// <seealso cref="UpdatePriority" />
    /// <seealso cref="IdleInterval" />
    /// <seealso cref="InstallerType" />
    property PathToInstall: string read FPathToInstall;

    /// <summary>
    ///   Contains the current version of the host application.
    /// </summary>
    /// <remarks>
    ///   It is calculated when searching for updates using the <see cref="uUpdateChecker|TUpdateChecker.BeginCheckUpdate">
    ///   BeginCheckUpdate</see> or <see cref="uUpdateChecker|TUpdateChecker.CheckForUpdate">
    ///   CheckForUpdate</see> methods.
    /// </remarks>
    /// <seealso cref="BeginCheckUpdate" />
    /// <seealso cref="CheckForUpdate" />
    property InstalledVersion: string read FInstalledVersion;

    /// <summary>
    ///   When a new update is available, It tells the <see cref="uUpdateChecker|TUpdateChecker" />
    ///    the type of update to perform: Replacement or Installer.. <br />
    /// </summary>
    /// <seealso cref="Version" />
    /// <seealso cref="URLDownload" />
    /// <seealso cref="ReleaseNotes" />
    /// <seealso cref="TargetFileName" />
    /// <seealso cref="UpdatePriority" />
    /// <seealso cref="IdleInterval" />
    /// <seealso cref="PathToInstall" />
    property InstallerType: string read FInstallerType;

    /// <summary>
    ///   <para>
    ///     When a new update is available, indicates the way in which the
    ///     update is to be performed:
    ///   </para>
    ///   <list type="bullet">
    ///     <item>
    ///       <font color="#2A2A2A"><u>On Program Exit</u>: Wait until the normal completion of the
    ///     program.</font>
    ///     </item>
    ///     <item>
    ///       <font color="#2A2A2A"><u>On Idle</u>: In this case the Idle Time is contained in the <see cref="uUpdateChecker|TUpdateChecker.IdleInterval">
    ///     IdleInterval</see> property</font>
    ///     </item>
    ///     <item>
    ///       <font color="#2A2A2A"><u>Now</u>: The update must be done at the time it is received.</font>
    ///     </item>
    ///   </list>
    ///   <para>
    ///     When the specified condition is met, the <see cref="uUpdateChecker|TUpdateChecker" />
    ///      component fires the <see cref="uUpdateChecker|TUpdateChecker.OnNotifyMustExitForUpdate">
    ///     OnNotifyMustExitForUpdate</see> event <br />
    ///   </para>
    /// </summary>
    /// <seealso cref="Version" />
    /// <seealso cref="URLDownload" />
    /// <seealso cref="ReleaseNotes" />
    /// <seealso cref="TargetFileName" />
    /// <seealso cref="InstallerType" />
    /// <seealso cref="IdleInterval" />
    /// <seealso cref="PathToInstall" />
    /// <seealso cref="OnNotifyMustExitForUpdate" />
    property UpdatePriority: TUpdatePriority read FUpdatePriority;// write SetUpdatePriority;

    /// <summary>
    ///   When a new update is availableand the value of the UpdatePriority
    ///   property is On Idle, it tells the <see cref="uUpdateChecker|TUpdateChecker" />
    ///    how long the host application should remain Idle before launching
    ///   the <see cref="uUpdateChecker|TUpdateChecker.OnNotifyMustExitForUpdate">
    ///   OnNotifyMustExitForUpdate</see> event. <br />
    /// </summary>
    /// <seealso cref="Version" />
    /// <seealso cref="URLDownload" />
    /// <seealso cref="ReleaseNotes" />
    /// <seealso cref="TargetFileName" />
    /// <seealso cref="InstallerType" />
    /// <seealso cref="UpdatePriority" />
    /// <seealso cref="PathToInstall" />
    /// <seealso cref="OnNotifyMustExitForUpdate" />
    property IdleInterval: string read FIdleInterval; // write SetIdleInterval;

    /// <summary>
    ///   Flag indicating if there is a new version available to apply the <see cref="uUpdateChecker|TUpdateChecker.Update">
    ///   Update</see> method
    /// </summary>
    /// <seealso cref="Update" />
    property UpdateForInstall: boolean read FUpdateForInstall;

    property ComputerID: string read FComputerID;
  published
    /// <summary>
    ///   When the <see cref="uUpdateChecker|TUpdateChecker.BeginCheckUpdate">
    ///   BeginCheckUpdate</see> method is executed, it allows you to specify
    ///   the time interval to search for new updates. <br /><br />The format
    ///   must be hh:mm:ss or mm:ss.
    /// </summary>
    /// <seealso cref="BeginCheckUpdate">
    ///   BeginCheckUpdate method
    /// </seealso>
    /// <seealso cref="OnAfterCheck">
    ///   OnAfterCheck event
    /// </seealso>
    property Interval: string read GetInterval write SetInterval;

    /// <summary>
    ///   <para>
    ///     Contains the URL where the web service is located. <br /><br />By
    ///     default it points to:
    ///   </para>
    ///   <para>

    ///     http://connect.compudime.com:8080/WSAutoUpdater/WSAutoUpdater.dll/soap/IAutoUpdater
    ///   </para>
    /// </summary>
    property WebServiceURL: string read FWebServiceURL write SetWebServiceURL;

    /// <summary>
    ///   Contains the name of the host application executable. It is used to
    ///   check for new updates to the web service. <br /><br />If you want the
    ///   <see cref="uUpdateChecker|TUpdateChecker" /> to query for a different
    ///   application name, you can set a different file name.
    /// </summary>
    /// <seealso cref="InstalledVersion" />
    /// <seealso cref="DBVersion" />
    /// <seealso cref="Enterprise" />
    /// <seealso cref="Store" />
    /// <seealso cref="ComputerNumber" />
    property ProgramName: string read FProgramName write FProgramName;

    /// <summary>
    ///   Contains the name of the file in which the notes containing the new
    ///   update that come in the <see cref="uUpdateChecker|TUpdateChecker.ReleaseNotes">
    ///   ReleaseNotes</see> property will be saved.
    /// </summary>
    /// <seealso cref="ReleaseNotes" />
    property ReleaseNotesFileName: string read FReleaseNotesFileName write SetReleaseNotesFileName;

    /// <summary>
    ///   Contains the name of the .ini file in which the information regarding
    ///   the new update that was just downloaded is stored.
    /// </summary>
    /// <seealso cref="Encrypted" />
    /// <seealso cref="EncryptionKey" />
    property UpdatesIniFileName: string read FUpdatesIniFileName write SetUpdatesIniFileName;

    /// <summary>
    ///   <para>
    ///     Allows you to set the name of the Enterprise client so <see cref="uUpdateChecker|TUpdateChecker" />
    ///      can notify the web service of the Enterprise looking for the
    ///     update.
    ///   </para>
    ///   <para>
    ///     In case it remains empty, <see cref="uUpdateChecker|TUpdateChecker" />
    ///      calculates the public IP and it is the one that it sends to the
    ///     web service.
    ///   </para>
    /// </summary>
    /// <remarks>
    ///   If this property has an empty value, the public IP is calculated when
    ///   executing the <see cref="uUpdateChecker|TUpdateChecker.BeginCheckUpdate">
    ///   BeginCheckUpdate</see> and <see cref="uUpdateChecker|TUpdateChecker.CheckForUpdate">
    ///   CheckForUpdate</see> methods
    /// </remarks>
    /// <seealso cref="InstalledVersion" />
    /// <seealso cref="DBVersion" />
    /// <seealso cref="ProgramName" />
    /// <seealso cref="Store" />
    /// <seealso cref="ComputerNumber" />
    property Enterprise: string read FEnterprise write FEnterprise;

    /// <summary>
    ///   <para>
    ///     Allows you to set the name of the store within a given Enterprise
    ///     so <see cref="uUpdateChecker|TUpdateChecker" /> can notify the
    ///     web service of the Store looking for the update.
    ///   </para>
    ///   <para>
    ///     In case it remains empty, <see cref="uUpdateChecker|TUpdateChecker" />
    ///      calculates the Local IP and it is the one that it sends to the
    ///     web service.
    ///   </para>
    /// </summary>
    /// <remarks>
    ///   If this property has an empty value, the Local IP is calculated when
    ///   executing the <see cref="uUpdateChecker|TUpdateChecker.BeginCheckUpdate">
    ///   BeginCheckUpdate</see> and <see cref="uUpdateChecker|TUpdateChecker.CheckForUpdate">
    ///   CheckForUpdate</see> methods
    /// </remarks>
    /// <seealso cref="InstalledVersion" />
    /// <seealso cref="DBVersion" />
    /// <seealso cref="ProgramName" />
    /// <seealso cref="Enterprise" />
    /// <seealso cref="ComputerNumber" />
    property Store: string read FStore write FStore;

    /// <summary>
    ///   <para>
    ///     Allows you to set the Computer Name within a given Store so <see cref="uUpdateChecker|TUpdateChecker" />
    ///      can notify the web service of the Computer Number looking for
    ///     the update.
    ///   </para>
    ///   <para>
    ///     In case it remains empty, <see cref="uUpdateChecker|TUpdateChecker" />
    ///      calculates the Local IP and it is the one that it sends to the
    ///     web service.
    ///   </para>
    /// </summary>
    /// <remarks>
    ///   If this property has an empty value, the Local IP is calculated when
    ///   executing the <see cref="uUpdateChecker|TUpdateChecker.BeginCheckUpdate">
    ///   BeginCheckUpdate</see> and <see cref="uUpdateChecker|TUpdateChecker.CheckForUpdate">
    ///   CheckForUpdate</see> methods
    /// </remarks>
    /// <seealso cref="InstalledVersion" />
    /// <seealso cref="DBVersion" />
    /// <seealso cref="ProgramName" />
    /// <seealso cref="Enterprise" />
    /// <seealso cref="Store" />
    property ComputerNumber: string read FComputerNumber write FComputerNumber;

    /// <summary>
    ///   <para>
    ///     If the application is installed inside the Program Files or
    ///     Program Files x86 directories, it is usually not possible to
    ///     write inside the application directory without administration
    ///     privileges. In this case the information must be written inside
    ///     AppData.
    ///   </para>
    ///   <para>
    ///     If this property has a value, this directory will be created
    ///     inside AppData, and inside the application directory for update
    ///     data will be created.
    ///   </para>
    /// </summary>
    /// <seealso cref="AppDirectory" />
    property Company: string read FCompany write SetCompany;

    /// <summary>
    ///   <para>
    ///     If the application is installed inside the Program Files or
    ///     Program Files x86 directories, it is usually not possible to
    ///     write inside the application directory without administration
    ///     privileges. In this case the information must be written inside
    ///     AppData.
    ///   </para>
    ///   <para>
    ///     If this property has a value, this directory will be created
    ///     inside AppData, below the company directory if the <see cref="uUpdateChecker|TUpdateChecker.Company">
    ///     Company</see> property has a value, or directly within AppData.
    ///     and inside the application directory for update data will be
    ///     created.
    ///   </para>
    ///   <para>
    ///     If this property does not have a value, it will take the name of
    ///     the host application executable to create the application
    ///     directory. <br />
    ///   </para>
    /// </summary>
    /// <seealso cref="Company" />
    property AppDirectory: string read FAppDirectory write SetAppDirectory;

    /// <summary>
    ///   Allows you to set the database version number used by the host
    ///   application so <see cref="uUpdateChecker|TUpdateChecker" /> can
    ///   notify the web service of the DB Version looking for the update.
    /// </summary>
    /// <remarks>
    ///   If this property has an empty value, the public IP is calculated when
    ///   executing the <see cref="uUpdateChecker|TUpdateChecker.BeginCheckUpdate">
    ///   BeginCheckUpdate</see> and <see cref="uUpdateChecker|TUpdateChecker.CheckForUpdate">
    ///   CheckForUpdate</see> methods
    /// </remarks>
    /// <seealso cref="InstalledVersion" />
    /// <seealso cref="Enterprise" />
    /// <seealso cref="ProgramName" />
    /// <seealso cref="Store" />
    /// <seealso cref="ComputerNumber" />
    property DBVersion: string read FDBVersion write SetDBVersion;

    /// <summary>
    ///   Indicates whether or not the .ini file defined in the <see cref="uUpdateChecker|TUpdateChecker.UpdatesIniFileName">
    ///   UpdatesIniFileName</see> property is encrypted with the <see cref="uUpdateChecker|TUpdateChecker.EncryptionKey">
    ///   EncryptionKey</see>.
    /// </summary>
    /// <seealso cref="EncryptionKey" />
    /// <seealso cref="UpdatesIniFileName" />
    property Encrypted: boolean read FEncrypted write FEncrypted;

    /// <summary>
    ///   Indicates the key to encrypt the .ini file defined in the <see cref="uUpdateChecker|TUpdateChecker.UpdatesIniFileName">
    ///   UpdatesIniFileName</see> property in case <see cref="uUpdateChecker|TUpdateChecker.Encrypted">
    ///   Encrypted</see> has a true value.
    /// </summary>
    /// <seealso cref="Encrypted" />
    /// <seealso cref="UpdatesIniFileName" />
    property EncryptionKey: string read FEncryptionKey write FEncryptionKey;
    /// <summary>
    ///   Contains the current version of the TUpdateChecker component
    /// </summary>
    property ComponentVersion: string read FComponentVersion;

    property VerboseLevel: TUpdaterVerboseLevel read FVerboseLevel write FVerboseLevel;

    /// <summary>
    ///   When the method BeginCheckUpdate has been executed, it is fired every
    ///   time the time set in the Interval property is met, regardless of
    ///   whether or not it finds an update
    /// </summary>
    property OnAfterCheck: TNotifyEvent read FOnAfterCheck write FOnAfterCheck;

    /// <summary>
    ///   Notify when a new version has been found, so that the host application
    ///   can send a message or start an animation that indicates the new
    ///   version.
    /// </summary>
    /// <remarks>
    ///   This event contains a StartDownload variable, by default it has a value
    ///   of True; if its value is changed to False, the download will not start.
    /// </remarks>
    property OnFoundNewAppVersion: TOnFoundNewAppVersionEvent read FOnFoundNewAppVersion write FOnFoundNewAppVersion;

    /// <summary>
    ///   Fired when <see cref="uUpdateChecker|TUpdateChecker" /> starts
    ///   downloading the new update from the server, so that the host
    ///   application can show some status indicating that the download is in
    ///   progress.
    /// </summary>
    /// <example>
    ///   <code lang="Delphi">  procedure TForm1.UpdaterStartDownload(Sender: TObject; ProgramName,
    ///   Version, URLDownload, TargetFileName, PathToInstall: string);
    ///   begin
    /// StatusBar.Panels [PNL_Autoupdate].Text := 'Downloading
    ///   update';
    /// (StatusBar.Panels [PNL_Autoupdate].PanelStyle as
    ///   TdxStatusBarTextPanelStyle).ImageIndex := ICO_Update_Download;
    ///   end; </code>
    /// </example>
    property OnStartDownload: TOnDownloadEvent read FOnStartDownload write FOnStartDownload;

    /// <summary>
    ///   Fired when <see cref="uUpdateChecker|TUpdateChecker" /> when the
    ///   update download from the server has finished, so that the host
    ///   application can show some status indicating that the download has
    ///   finished. <br />
    /// </summary>
    /// <example>
    ///   <code lang="Delphi">procedure TForm1.UpdaterFinishDownload(Sender: TObject; ProgramName,
    ///   Version, URLDownload, TargetFileName, PathToInstall: string);
    /// begin
    ///   StatusBar.Panels [PNL_Autoupdate].Text := 'Ready for update';
    ///   (StatusBar.Panels [PNL_Autoupdate].PanelStyle as TdxStatusBarTextPanelStyle).ImageIndex := ICO_Update_New;
    /// end;</code>
    /// </example>
    property OnFinishDownload: TOnDownloadEvent read FOnFinishDownload write FOnFinishDownload;

    /// <summary>
    ///   Triggered when an error occurred during the update download process
    ///   from the server, so that the host application can show some status
    ///   indicating the downloading error.
    /// </summary>
    /// <example>
    ///   <code lang="Delphi">
    ///   procedure TForm1.UpdaterErrorDownload(Sender: TObject;
    ///   ProgramName, <br />Version, URLDownload, TargetFileName,
    ///   PathToInstall, ErrMsg: string); <br />begin <br />StatusBar.Panels
    ///   [PNL_Autoupdate].Text := 'Download error'; <br />(StatusBar.Panels
    ///   [PNL_Autoupdate].PanelStyle as TdxStatusBarTextPanelStyle).ImageIndex
    ///   := ICO_Update_Download_Error; <br />end; <br />
    ///   </code>
    /// </example>
    property OnErrorDownload: TOnDownloadErrorEvent read FOnErrorDownload write FOnErrorDownload;

    /// <summary>
    ///   This event is fired when <see cref="uUpdateChecker|TUpdateChecker" />
    ///   checks for new updates using methods <see cref="uUpdateChecker|TUpdateChecker.BeginCheckUpdate">
    ///   BeginCheckUpdate</see> or <see cref="uUpdateChecker|TUpdateChecker.CheckForUpdate">
    ///   CheckForUpdate</see>, and detects that the new version has been
    ///   installed successfully and is the first execution, so that a message
    ///   can be displayed to the user with the description of the updates. <br />
    /// </summary>
    /// <example>
    ///   <code lang="Delphi">
    ///   procedure TForm1.UpdaterFirstRunNewVersion(Sender: TObject; <br />
    ///   ReleaseNotes: string; var MarkAsDisplayed: Boolean); <br />begin <br />
    ///   FNewVersionDlg.Installed := True; <br />FNewVersionDlg.Version :=
    ///   Updater.Version; <br />FNewVersionDlg.Notes.Lines.Text :=
    ///   ReleaseNotes; <br />FNewVersionDlg.ShowModal; <br />end; <br />
    ///  </code>
    /// </example>
    /// <seealso cref="BeginCheckUpdate" />
    /// <seealso cref="CheckForUpdate" />
    property OnFirstRunNewVersion: TOnFirstRunNewVersionEvent read FOnFirstRunNewVersion write FOnFirstRunNewVersion;

    /// <summary>
    ///   Notifies the host program that the application must be closed safely
    ///   to perform update tasks.
    /// </summary>
    /// <remarks>
    ///   The TUpdateChecker component does not shut down the host application
    ///   on its own, it just notifies the application that it should be closed
    ///   safely.
    /// </remarks>
    /// <example>
    ///   <code lang="Delphi">  procedure TForm1.UpdaterNotifyMustExitForUpdate(Sender: TObject);
    ///   begin
    /// if Updater.UpdatePriority = 'Now' then
    /// MessageDlg
    ///   ('It is necessary to close the application to install the new
    ///   update', mtInformation, [mbOK], 0);
    /// Close;
    /// end; </code>
    /// </example>
    /// <seealso cref="UpdatePriority" />
    /// <seealso cref="IdleTime" />
    property OnNotifyMustExitForUpdate: TNotifyEvent read FOnNotifyMustExitForUpdate write FOnNotifyMustExitForUpdate;

    /// <summary>
    ///   It is triggered when an error occurred while trying to perform the
    ///   update processes, so that the host application can notify the user.
    /// </summary>
    /// <example>
    ///   <code lang="Delphi">
    ///   procedure TFMainDoctor.UpdaterUpdateError(Sender: TObject; ErrorMsg:
    ///   string); <br />begin <br />ErrorMsg := Format ('An error occurred in
    ///   the update process with the message: [%s]', [ErrorMsg]); <br /><br />
    ///   StatusBar.Panels [PNL_Autoupdate].Text := 'Download error'; <br />
    ///   (StatusBar.Panels [PNL_Autoupdate].PanelStyle as
    ///   TdxStatusBarTextPanelStyle).ImageIndex := ICO_Update_Error; <br />
    ///   DMDatabaseFixer.Log_Exception(ErrorMsg); <br /><br />MessageDlg
    ///   (ErrorMsg, mtError, [mbOK], 0); <br />end; <br />
    ///    </code>
    /// </example>
    property OnUpdateError: TOnUpdateError read FOnUpdateError write FOnUpdateError;
  end;

function GetAppVersionStr: string;
function CompareVersion (Ver1, Ver2: string): integer;
procedure Register;

implementation
uses WinSock, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  IdIOHandler, IdIOHandlerSocket, IdSSLOpenSSL, ShlObj, ComObj;

{$region 'miscellaneous routines'}
const
  CSIDL_DESKTOP                  = $0000; { <desktop> }
  CSIDL_INTERNET                 = $0001; { Internet Explorer (icon on desktop) }
  CSIDL_PROGRAMS                 = $0002; { Start Menu\Programs }
  CSIDL_CONTROLS                 = $0003; { My Computer\Control Panel }
  CSIDL_PRINTERS                 = $0004; { My Computer\Printers }
  CSIDL_PERSONAL                 = $0005; { My Documents.  This is equivalent to CSIDL_MYDOCUMENTS in XP and above }
  CSIDL_FAVORITES                = $0006; { <user name>\Favorites }
  CSIDL_STARTUP                  = $0007; { Start Menu\Programs\Startup }
  CSIDL_RECENT                   = $0008; { <user name>\Recent }
  CSIDL_SENDTO                   = $0009; { <user name>\SendTo }
  CSIDL_BITBUCKET                = $000a; { <desktop>\Recycle Bin }
  CSIDL_STARTMENU                = $000b; { <user name>\Start Menu }
  CSIDL_MYDOCUMENTS              = $000c; { logical "My Documents" desktop icon }
  CSIDL_MYMUSIC                  = $000d; { "My Music" folder }
  CSIDL_MYVIDEO                  = $000e; { "My Video" folder }
  CSIDL_DESKTOPDIRECTORY         = $0010; { <user name>\Desktop }
  CSIDL_DRIVES                   = $0011; { My Computer }
  CSIDL_NETWORK                  = $0012; { Network Neighborhood (My Network Places) }
  CSIDL_NETHOOD                  = $0013; { <user name>\nethood }
  CSIDL_FONTS                    = $0014; { windows\fonts }
  CSIDL_TEMPLATES                = $0015; { <user name>\appdata\roaming\template folder }
  CSIDL_COMMON_STARTMENU         = $0016; { All Users\Start Menu }
  CSIDL_COMMON_PROGRAMS          = $0017; { All Users\Start Menu\Programs }
  CSIDL_COMMON_STARTUP           = $0018; { All Users\Startup }
  CSIDL_COMMON_DESKTOPDIRECTORY  = $0019; { All Users\Desktop }
  CSIDL_APPDATA                  = $001a; { <user name>\Application Data }
  CSIDL_PRINTHOOD                = $001b; { <user name>\PrintHood }
  CSIDL_LOCAL_APPDATA            = $001c; { <user name>\Local Settings\Application Data (non roaming) }
  CSIDL_ALTSTARTUP               = $001d; { non localized startup }
  CSIDL_COMMON_ALTSTARTUP        = $001e; { non localized common startup }
  CSIDL_COMMON_FAVORITES         = $001f; { User favourites }
  CSIDL_INTERNET_CACHE           = $0020; { temporary inter files }
  CSIDL_COOKIES                  = $0021; { <user name>\Local Settings\Application Data\..\cookies }
  CSIDL_HISTORY                  = $0022; { <user name>\Local Settings\Application Data\..\history}
  CSIDL_COMMON_APPDATA           = $0023; { All Users\Application Data }
  CSIDL_WINDOWS                  = $0024; { GetWindowsDirectory() }
  CSIDL_SYSTEM                   = $0025; { GetSystemDirectory() }
  CSIDL_PROGRAM_FILES            = $0026; { C:\Program Files }
  CSIDL_MYPICTURES               = $0027; { C:\Program Files\My Pictures }
  CSIDL_PROFILE                  = $0028; { USERPROFILE }
  CSIDL_SYSTEMX86                = $0029; { x86 system directory on RISC }
  CSIDL_PROGRAM_FILESX86         = $002a; { x86 C:\Program Files on RISC }
  CSIDL_PROGRAM_FILES_COMMON     = $002b; { C:\Program Files\Common }
  CSIDL_PROGRAM_FILES_COMMONX86  = $002c; { x86 C:\Program Files\Common on RISC }
  CSIDL_COMMON_TEMPLATES         = $002d; { All Users\Templates }
  CSIDL_COMMON_DOCUMENTS         = $002e; { All Users\Documents }
  CSIDL_COMMON_ADMINTOOLS        = $002f; { All Users\Start Menu\Programs\Administrative Tools }
  CSIDL_ADMINTOOLS               = $0030; { <user name>\Start Menu\Programs\Administrative Tools }
  CSIDL_CONNECTIONS              = $0031; { Network and Dial-up Connections }
  CSIDL_COMMON_MUSIC             = $0035; { All Users\My Music }
  CSIDL_COMMON_PICTURES          = $0036; { All Users\My Pictures }
  CSIDL_COMMON_VIDEO             = $0037; { All Users\My Video }
  CSIDL_RESOURCES                = $0038; { Resource Directory }
  CSIDL_RESOURCES_LOCALIZED      = $0039; { Localized Resource Directory }
  CSIDL_CDBURN_AREA              = $003b; { USERPROFILE\Local Settings\Application Data\Microsoft\CD Burning }

function IsDirectoryWriteable(const AName: string): Boolean;
var
  FileName: String;
  H: THandle;
begin
  FileName := IncludeTrailingPathDelimiter(AName) + 'chk.tmp';
  H := CreateFile(PChar(FileName), GENERIC_READ or GENERIC_WRITE, 0, nil,
    CREATE_NEW, FILE_ATTRIBUTE_TEMPORARY or FILE_FLAG_DELETE_ON_CLOSE, 0);
  Result := H <> INVALID_HANDLE_VALUE;
  if Result then CloseHandle(H);
end;

function GetSpecialFolderPath(CSIDLFolder: Integer): string;
var
   FilePath: array [0..MAX_PATH] of char;
begin
  SHGetFolderPath(0, CSIDLFolder, 0, 0, FilePath);
  Result := FilePath;
end;

function IsDirectoryInProgramFiles (const DirName: string): boolean;
begin
  result := (Pos (UpperCase (GetSpecialFolderPath (CSIDL_PROGRAM_FILES)),    UpperCase (DirName)) > 0) or
            (Pos (UpperCase (GetSpecialFolderPath (CSIDL_PROGRAM_FILESX86)), UpperCase (DirName)) > 0);
end;

function GetAppVersionStr: string;
type
  TBytes = array of byte;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d',
    [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
     LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
     LongRec(FixedPtr.dwFileVersionLS).Hi,  //release
     LongRec(FixedPtr.dwFileVersionLS).Lo]) //build
end;

function CompareVersion (Ver1, Ver2: string): integer;
var
  A1, A2: TArray <string>;
  i, n1, n2: integer;
begin
  result := coEqual;

  A1 := Ver1.Split(['.']);
  A2 := Ver2.Split(['.']);

  for i := 0 to Length (A1) - 1 do
    if (Length (A2) < (i + 1)) and (Length (A2) < 3) then
    begin
      result := coGreater;
      exit;
    end
    else
    begin
      if not TryStrToInt (A1 [i], n1) or not TryStrToInt (A2 [i], n2) then exit;

      if n1 > n2 then
      begin
        result := coGreater;
        exit;
      end
      else if n2 > n1 then
      begin
        result := coLower;
        exit;
      end;
    end;
end;

function GetLocalIP: string;
type
  TaPInAddr = array [0..10] of PInAddr;
  PaPInAddr = ^TaPInAddr;
var
  phe: PHostEnt;
  pptr: PaPInAddr;
  Buffer: array [0..63] of Ansichar;
  i: Integer;
  GInitData: TWSADATA;
begin
  WSAStartup($101, GInitData);
  Result := '';
  GetHostName(Buffer, SizeOf(Buffer));
  phe := GetHostByName(Buffer);
  if phe = nil then
    Exit;
  pptr := PaPInAddr(phe^.h_addr_list);
  i := 0;
  while pptr^[i] <> nil do
  begin
    Result := StrPas(inet_ntoa(pptr^[i]^));
    Inc(i);
  end;
  WSACleanup;
end;

procedure DeleteFilesOlderThan(const Days: Integer; const Path: string; const SearchPattern: string = '.');
var
  FileName: string;
  OlderThan: TDateTime;
begin
  Assert(Days >= 0);
  OlderThan := Now() - Days;
  for FileName in TDirectory.GetFiles(Path, SearchPattern) do
    if TFile.GetCreationTime(FileName) < OlderThan then
      TFile.Delete(FileName);
end;

function BooleanToString (b: boolean): string;
begin
  result := GetEnumName (TypeInfo (Boolean), Integer (b));
end;

var
  FSWbemLocator : OLEVariant;
  FWMIService   : OLEVariant;

function GetWMIstring(const WMIClass, WMIProperty: string): string;
const
  wbemFlagForwardOnly = $00000020;
var
  FWbemObjectSet: OLEVariant;
  FWbemObject   : OLEVariant;
  oEnum         : IEnumvariant;
  iValue        : LongWord;
begin;
  Result:='';
  FSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  FWMIService   := FSWbemLocator.ConnectServer('localhost', 'root\CIMV2', '', '');
  FWbemObjectSet:= FWMIService.ExecQuery(Format('Select %s from %s',[WMIProperty, WMIClass]),'WQL',wbemFlagForwardOnly);
  oEnum         := IUnknown(FWbemObjectSet._NewEnum) as IEnumVariant;
  if oEnum.Next(1, FWbemObject, iValue) = 0 then
   if not VarIsNull(FWbemObject.Properties_.Item(WMIProperty).Value) then
   Result:=FWbemObject.Properties_.Item(WMIProperty).Value;
  FWbemObject:=Unassigned;
end;

function GetMotherBoardSerial: string;
begin
  result := Trim (GetWMIstring ('Win32_BIOS','SerialNumber'));
end;
{$endregion}

{$region 'TUpdateCheckerThread'}
constructor TUpdateCheckerThread.Create;
begin
  inherited Create;

  FCommand          := tcNone;
  FDownloadDir      := '';
  FWebServiceURL    := '';
  FEcho             := '';
  FText             := '';
  FProgramName      := '';
  FVersion          := '';
  FURLDownload      := '';
  FReleaseNotes     := '';
  FTargetFileName   := '';
  FErrMsg           := '';
  FExternalIP       := '';
  FUpdatePriority   := upOnProgramExit;
  FIdleTime         := '';
  FLocalIP          := '';
  FComputerID       := '';
  FComponentVersion := '';
  FCurrMajor        := 0;
  FCurrMiner        := 0;
  FCurrRelease      := 0;
  FDBMajor          := 0;
  FDBMiner          := 0;
  FDBRelease        := 0;
end;

destructor TUpdateCheckerThread.Destroy;
begin
  FDownloadDir      := '';
  FWebServiceURL    := '';
  FEcho             := '';
  FText             := '';
  FProgramName      := '';
  FVersion          := '';
  FURLDownload      := '';
  FReleaseNotes     := '';
  FTargetFileName   := '';
  FErrMsg           := '';
  FExternalIP       := '';
  FIdleTime         := '';
  FLocalIP          := '';
  FComputerID       := '';
  FComponentVersion := '';

  inherited Destroy;
end;

procedure TUpdateCheckerThread.Log (Level: TUpdaterVerboseLevel; msg: string);
begin
  FVerboseLevel := Level;
  FMsgLog       := msg;

  Synchronize(SyncLog);
end;

procedure TUpdateCheckerThread.SyncLog;
begin
  FUpdateChecker.Log(FVerboseLevel, FMsgLog);
end;

procedure TUpdateCheckerThread.Execute;
var
  n:       integer;
  JSON:    TJSONValue;
  Obj:     TJSONObject;
  IdHTTP:  TIdHTTP;
  s:       string;
  TmpFile: string;
begin
  CoInitialize (nil);
  try
    FErrMsg := '';
    Log (uvlInfo, 'procedure TUpdateCheckerThread.Execute; [Begin]');
    Log (uvlInfo, 'Command: ' + GetEnumName (TypeInfo (TThreadCommand), Integer (FCommand)));
    case FCommand of
      tcGetExternalIP: begin
        IdHTTP := TIdHTTP.Create(nil);
        JSON   := nil;
        try
          try
            Log (uvlInfo, Format ('Call: IdHTTP.Get(''%s'');', [coURLTestExternalIP]));
            s           := IdHTTP.Get(coURLTestExternalIP);
            JSON        := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(s), 0);
            FExternalIP := JSON.GetValue<string>('ip');
            Log (uvlDebug, 'External IP: ' + FExternalIP);
          except
            on E: Exception do
            begin
              FErrMsg := E.Message;
              Log (uvlError, 'Error: ' + FErrMsg);
            end;
          end;
        finally
          IdHTTP.Free;

          if Assigned (JSON) then
            JSON.Free;
        end;
      end;
      tcGetVersionInfo: begin
        FVersion        := '';
        FURLDownload    := '';
        FReleaseNotes   := '';
        FTargetFileName := '';
        FPathToInstall  := '';
        FUpdatePriority := upOnProgramExit;
        FIdleTime       := '';
        FInstallerType  := '';
        if FProgramName <> '' then
        begin
          Obj := TJSONObject.Create;
          try
            Obj.AddPair(TJSONPair.Create('Application',   FProgramName));
            Obj.AddPair(TJSONPair.Create('ComputerID',    FComputerID));
            Obj.AddPair(TJSONPair.Create('Enterprise',    FEnterprise));
            Obj.AddPair(TJSONPair.Create('Store',         FStore));
            Obj.AddPair(TJSONPair.Create('MachineNumber', FComputerNumber));
            Obj.AddPair(TJSONPair.Create('ExternalIP',    FExternalIP));
            Obj.AddPair(TJSONPair.Create('LocalIP',       FLocalIP));
            Obj.AddPair(TJSONPair.Create('CurrMajor',     FCurrMajor.ToString));
            Obj.AddPair(TJSONPair.Create('CurrMiner',     FCurrMiner.ToString));
            Obj.AddPair(TJSONPair.Create('CurrRelease',   FCurrRelease.ToString));
            Obj.AddPair(TJSONPair.Create('DBMajor',       FDBMajor.ToString));
            Obj.AddPair(TJSONPair.Create('DBMiner',       FDBMiner.ToString));
            Obj.AddPair(TJSONPair.Create('DBRelease',     FDBRelease.ToString));
            Obj.AddPair(TJSONPair.Create('ComponentVer',  FComponentVersion));
            try
              Log (uvlDebug, 'JSON Request: ' + Obj.ToJSON);
              Log (uvlInfo, Format ('GetIAutoUpdater(False, ''%s'').GetUpdateInfo(Obj.ToJSON)', [FWebServiceURL]));
              s := GetIAutoUpdater(False, FWebServiceURL).GetUpdateInfo(Obj.ToJSON);
              try
                JSON            := TJSONObject.ParseJSONValue(s);
                FVersion        := JSON.GetValue<string>('Version');
                FURLDownload    := JSON.GetValue<string>('URL');
                FReleaseNotes   := JSON.GetValue<string>('ReleaseNotes');
                FTargetFileName := JSON.GetValue<string>('TargetFileName');
                FPathToInstall  := JSON.GetValue<string>('PathToInstall');
                FIdleTime       := JSON.GetValue<string>('IdleTime');
                FInstallerType  := JSON.GetValue<string>('InstallerType');
                s               := JSON.GetValue<string>('UpdatePriority');

                if      s = 'Now' then
                  FUpdatePriority := upNow
                else if s = 'On Idle' then
                  FUpdatePriority := upOnIdle;

                if JSON.GetValue<string>('ErrorMsg') <> '' then
                  Log (uvlError, Format ('Response error [%s]', [JSON.GetValue<string>('ErrorMsg')]));
              finally
                JSON.Free;
              end;

              Log (uvlDebug, 'Version:        ' + FVersion);
              Log (uvlDebug, 'URL:            ' + FURLDownload);
              Log (uvlDebug, 'ReleaseNotes:   ' + FReleaseNotes);
              Log (uvlDebug, 'TargetFileName: ' + FTargetFileName);
              Log (uvlDebug, 'PathToInstall:  ' + FPathToInstall);
              Log (uvlDebug, 'IdleTime:       ' + FIdleTime);
              Log (uvlDebug, 'InstallerType:  ' + FInstallerType);
              Log (uvlDebug, 'UpdatePriority: ' + s);
            except
              on E: Exception do
              begin
                FErrMsg := E.Message;
                Log (uvlError, 'Error: ' + FErrMsg);
              end;
            end;
          finally
            Obj.Free;
          end;
        end;
      end;
      tcDownloadFile: begin
        if not FURLDownload.IsEmpty and not FTargetFileName.IsEmpty then
        begin
          try
            Log (uvlDebug, Format ('n := URLDownloadToFile (nil, PChar (''%s''), PChar (''%s''), 0, nil);', [FURLDownload, FDownloadDir + '\' + FTargetFileName]));
            n := URLDownloadToFile (nil, PChar (FURLDownload), PChar (FDownloadDir + '\' + FTargetFileName), 0, nil);
            if n = 0 then
              Log (uvlInfo, 'Download successful')
            else
            begin
              FErrMsg := Format ('An error has ocurred downloading %s to %s', [FURLDownload, FTargetFileName]);
              Log (uvlError, 'Downloading error: ' + FErrMsg);
            end;
          except
            on E: Exception do
            begin
              FErrMsg := E.Message;
              Log (uvlError, 'Error: ' + FErrMsg);
            end;
          end;
        end;
      end;
      tcLogDownloadDone: begin
        try
          s := Format ('%d.%d.%d', [FDBMajor, FDBMiner, FDBRelease]);
          Obj := TJSONObject.Create;
          try
            Obj.AddPair(TJSONPair.Create('Application',   FProgramName));
            Obj.AddPair(TJSONPair.Create('Version',       FVersion));
            Obj.AddPair(TJSONPair.Create('DBVersion',     s));
            Obj.AddPair(TJSONPair.Create('ComputerID',    FComputerID));
            Obj.AddPair(TJSONPair.Create('Enterprise',    FEnterprise));
            Obj.AddPair(TJSONPair.Create('Store',         FStore));
            Obj.AddPair(TJSONPair.Create('MachineNumber', FComputerNumber));
            Obj.AddPair(TJSONPair.Create('ExternalIP',    FExternalIP));
            Obj.AddPair(TJSONPair.Create('LocalIP',       FLocalIP));
            Obj.AddPair(TJSONPair.Create('ComponentVer',  FComponentVersion));
            Log (uvlDebug, 'JSON Request: ' + Obj.ToJSON);
            Log (uvlInfo,  'Call GetIAutoUpdater(False, FWebServiceURL).ConfirmDownload(JSON);');
            GetIAutoUpdater(False, FWebServiceURL).ConfirmDownload(Obj.ToJSON);
            Log (uvlInfo, 'tcLogDownloadDone successful');
          finally
            Obj.Free;
          end;
        except
          on E: Exception do
          begin
            FErrMsg := E.Message;
            Log (uvlError, 'Error: ' + FErrMsg);
          end;
        end;
      end;
      tcLogUpdatedDone: begin
        try
          s := Format ('%d.%d.%d', [FDBMajor, FDBMiner, FDBRelease]);
          Obj := TJSONObject.Create;
          try
            Obj.AddPair(TJSONPair.Create('Application',   FProgramName));
            Obj.AddPair(TJSONPair.Create('Version',       FVersion));
            Obj.AddPair(TJSONPair.Create('DBVersion',     s));
            Obj.AddPair(TJSONPair.Create('ComputerID',    FComputerID));
            Obj.AddPair(TJSONPair.Create('Enterprise',    FEnterprise));
            Obj.AddPair(TJSONPair.Create('Store',         FStore));
            Obj.AddPair(TJSONPair.Create('MachineNumber', FComputerNumber));
            Obj.AddPair(TJSONPair.Create('ExternalIP',    FExternalIP));
            Obj.AddPair(TJSONPair.Create('LocalIP',       FLocalIP));
            Obj.AddPair(TJSONPair.Create('ComponentVer',  FComponentVersion));
            Log (uvlDebug, 'JSON Request: ' + Obj.ToJSON);
            Log (uvlInfo,  'Call GetIAutoUpdater(False, FWebServiceURL).ConfirmUpdate(JSON);');
            GetIAutoUpdater(False, FWebServiceURL).ConfirmUpdate(Obj.ToJSON);
            Log (uvlInfo, 'tcLogDownloadDone successful');
          finally
            Obj.Free;
          end;
        except
          on E: Exception do
          begin
            FErrMsg := E.Message;
            Log (uvlError, 'Error: ' + FErrMsg);
          end;
        end;
      end;
    end;
  finally
    CoUninitialize;
    Log (uvlInfo, 'procedure TUpdateCheckerThread.Execute; [End]');
  end;
end;
{$endregion}

{$region 'TUpdateChecker'}
constructor TUpdateChecker.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);

  FMinTimer             := TMinSecTimer.Create(Self);
  FMinTimer.Enabled     := False;
  FMinTimer.OnTimer     := OnTimerMinSec;
  FThread               := nil;
  FTimer                := TTimer.Create(Self);
  FTimer.Enabled        := False;
  FTimer.OnTimer        := OnTimer;
  FTimer.Interval       := 250;
  FProgramName          := '';
  FInstalledVersion     := '';
  FReleaseNotesFileName := 'ReleaseNotes.txt';
  FUpdatesIniFileName   := 'Updates.ini';
  FIdleInterval         := '03:00';
  FWebServiceURL        := coWSAutoUpdater;
  FUpdatePriority       := upOnProgramExit;
  FDownloadFinished     := False;
  FIdleNotified         := False;
  FEncrypted            := False;
  FEncryptionKey        := '';
  FLocalIP              := '';
  FEnterprise           := '';
  FStore                := '';
  FComputerNumber       := '';
  FExternalIP           := '';
  FDBVersion            := '';
  FInstallerType        := '';
  FUpdateForInstall     := False;
  FExitNotified         := False;
  FDownloadFinishedTick := 0;
  FIsProgramFilesDir    := IsDirectoryInProgramFiles(ExtractFileDir (ParamStr (0)));
  FRootDir              := '';
  FDownloadDir          := '';
  FLogDir               := '';
  FCompany              := '';
  FAppDirectory         := '';
  FComponentVersion     := '1.0.2';
  FComputerID           := GetComputerID;
  FVerboseLevel         := uvlDisabled;
  FDownloadStatus       := dsNone;
  SearchApplication;
end;

destructor TUpdateChecker.Destroy;
begin
  FMinTimer.Enabled := False;
  FTimer.Enabled    := False;
  FMinTimer.Free;
  FTimer.Free;

  if Assigned (FThread) and not FThread.Terminated then
    FThread.Terminate;

  inherited Destroy;
end;

procedure TUpdateChecker.Log (Level: TUpdaterVerboseLevel; msg: string);
var
  s:     string;
  LogFN: string;
begin
  if (FVerboseLevel > uvlDisabled) and (FVerboseLevel >= Level) and not FLogDir.IsEmpty then
  begin
    s     := GetEnumName (TypeInfo (TUpdaterVerboseLevel), Integer (Level));
    s     := Copy (s, 3, Length (s));
    LogFN := Format ('%s\TUpdateChecker_%s.log', [FLogDir, FormatDateTime ('yyyy_mm_dd', Now)]);

    try
      TFile.AppendAllText(LogFN, Format ('%s [%s] %s', [FormatDateTime ('hh:nn:ss.zzz', Now), s, msg]) + sLineBreak);
    except

    end;
  end;
end;

procedure TUpdateChecker.DeletePreviousLogs;
begin
  if FVerboseLevel > uvlDisabled then
  begin
    DeleteFilesOlderThan(1, FLogDir, '*.log');
    Log (uvlInfo, 'procedure TUpdateChecker.DeletePreviousLogs;');
  end;
end;


procedure TUpdateChecker.SearchApplication;
var
  c: TComponent;
begin
  FApplication := nil;
  c            := Owner;

  while Assigned (c) do
  begin
    if c is TApplication then
    begin
      FApplication := TApplication (c);
      exit;
    end;

    c := c.Owner;
  end;
end;

{$region 'Getters and setters'}
function TUpdateChecker.GetInterval: string;
begin
  result := FMinTimer.Interval;
end;

procedure TUpdateChecker.SetInterval (Value: string);
begin
  FMinTimer.Interval := Value;
end;

procedure TUpdateChecker.SetWebServiceURL (Value: string);
begin
  if FWebServiceURL <> Value then
  begin
    FWebServiceURL := Value;
  end;
end;

function TUpdateChecker.GetEnabled: boolean;
begin
  result := FMinTimer.Enabled;
end;

procedure TUpdateChecker.SetReleaseNotesFileName (Value: string);
begin
  if Value.IsEmpty then
    raise Exception.Create('ReleaseNotesFileName must have value');

  if Value <> FReleaseNotesFileName then
    FReleaseNotesFileName := Value;
end;

procedure TUpdateChecker.SetUpdatesIniFileName (Value: string);
begin
  if Value.IsEmpty then
    raise Exception.Create('UpdatesIniFileName must have value');

  if Value <> FUpdatesIniFileName then
    FUpdatesIniFileName := Value;
end;

procedure TUpdateChecker.SetEnabled (Value: boolean);
begin
  FMinTimer.Enabled := Value;
  FTimer.Enabled    := Value;
end;

procedure TUpdateChecker.SetIdleInterval (Value: string);
begin
  if Value <> FIdleInterval then
  begin
    if not FMinTimer.ValidateIntervalFormat(Value) then
      raise Exception.Create(ErrMsgInvalidFormat);

    FIdleInterval := Value;
    FIdleSeconds  := FMinTimer.GetSeconds(Value);
  end;
end;

procedure TUpdateChecker.SetUpdatePriority (Value: TUpdatePriority);
begin
  if FUpdatePriority <> Value then
  begin
    FUpdatePriority := Value;
  end;
end;

procedure TUpdateChecker.SetDBVersion (Value: string);
begin
  if FDBVersion <> Value then
  begin
    if not IsValidIPOrVersionFormat (Value) then
      raise Exception.Create('Invalid version format');

    FDBVersion := Value;
  end;
end;

procedure TUpdateChecker.SetCompany (Value: string);
begin
  if FCompany <> Value then
  begin
    FCompany := Value;
  end;
end;

procedure TUpdateChecker.SetAppDirectory (Value: string);
begin
  if FAppDirectory <> Value then
  begin
    FAppDirectory := Value;
  end;
end;

function TUpdateChecker.GetLogEnterprise: string;
begin
  if not FEnterprise.IsEmpty then
    result := FEnterprise
  else
    result := FExternalIP;
end;

function TUpdateChecker.GetLogStore: string;
begin
  if not FStore.IsEmpty then
    result := FStore
  else
    result := FLocalIP;
end;

function TUpdateChecker.GetLogComputerName: string;
begin
  if not FComputerNumber.IsEmpty then
    result := FComputerNumber
  else
    result := FLocalIP;
end;

function TUpdateChecker.GetComputerID: string;
begin
  result := GetMotherBoardSerial;
end;
{$endregion}

function TUpdateChecker.SecondsIdle: DWord;
var
   liInfo:   TLastInputInfo;
   IdleTick: Cardinal;
begin
  result := 0;
  if not FDownloadFinished then exit;

  liInfo.cbSize := SizeOf(TLastInputInfo) ;
  GetLastInputInfo(liInfo) ;

  IdleTick := liInfo.dwTime;
  if IdleTick < FDownloadFinishedTick then
    IdleTick := FDownloadFinishedTick;

  result   := (GetTickCount - IdleTick) div 1000;
end;

function TUpdateChecker.IsValidIPOrVersionFormat (s: string): boolean;
var
  A:    TArray <string>;
  i, n: integer;
begin
  result := False;
  A      := s.Split(['.']);

  if Length (A) <> 4 then exit;

  for i := 0 to Length (A) - 1 do
    if not TryStrToInt (A [i], n) then
      exit;

  result := True;
end;

procedure TUpdateChecker.ExtractVersionSegments (s: string; var Major, Miner, Release, Build: integer);
var
  A: TArray <string>;
begin
  Major   := 0;
  Miner   := 0;
  Release := 0;
  Build   := 0;
  A       := s.Split(['.']);

  if Length (A) >= 1 then TryStrToInt(A [0], Major);
  if Length (A) >= 2 then TryStrToInt(A [1], Miner);
  if Length (A) >= 3 then TryStrToInt(A [2], Release);
  if Length (A) >= 4 then TryStrToInt(A [3], Build);
end;

procedure TUpdateChecker.ReadIniValues (var AVersion, AInstaller, APathToInstall, AInstallerType: string; var AFirst: boolean);
var
  Upd, s: string;
  Ini: TStringList;
  Mem: TMemoryIniStructure;
begin
  Log (uvlInfo, 'procedure TUpdateChecker.ReadIniValues (var AVersion, AInstaller, APathToInstall, AInstallerType: string; var AFirst: boolean); [Begin]');
  AVersion       := '';
  AInstaller     := '';
  APathToInstall := '';
  AFirst         := False;
  Upd            := FRootDir + '\' + FUpdatesIniFileName;
  if FileExists (Upd) then
  begin
    Ini := TStringList.Create;
    Mem := TMemoryIniStructure.Create;
    try
      try
        s := TFile.ReadAllText(Upd);

        if FEncrypted and not FEncryptionKey.IsEmpty then
          s := Mem.DecryptStringList(s, FEncryptionKey);

        Ini.Text := s;
        Mem.LoadFromStringList(Ini);

        AVersion       := Mem.ReadString ('AppInfo', 'Version',       '0.0.0.0');
        AInstaller     := Mem.ReadString ('AppInfo', 'Installer',     '');
        APathToInstall := Mem.ReadString ('AppInfo', 'PathToInstall', '');
        AInstallerType := Mem.ReadString ('AppInfo', 'InstallerType', '');
        AFirst         := Mem.ReadBool   ('AppInfo', 'First',         False);

        Log (uvlDebug, 'AVersion:       ' + AVersion);
        Log (uvlDebug, 'AInstaller:     ' + AInstaller);
        Log (uvlDebug, 'APathToInstall: ' + APathToInstall);
        Log (uvlDebug, 'AInstallerType: ' + AInstallerType);
        Log (uvlDebug, 'AFirst:         ' + BooleanToString (AFirst));
      except
        on E: Exception do
          Log (uvlError, Format ('TUpdateChecker.ReadIniValues Error: [%s]', [E.Message]));
      end;
    finally
      Ini.Free;
      Mem.Free;
    end;
  end;
  Log (uvlDebug, Format ('AVersion: [%s], AInstaller: [%s], APathToInstall: [%s], AInstallerType: [%s], AFirst: [%s]', [AVersion, AInstaller, APathToInstall, AInstallerType, BooleanToString (AFirst)]));
  Log (uvlInfo, 'procedure TUpdateChecker.ReadIniValues (var AVersion, AInstaller, APathToInstall, AInstallerType: string; var AFirst: boolean); [End]');
end;

procedure TUpdateChecker.CreateThreadCheckForUpdate;
var
  Major, Miner, Release, Build: integer;
begin
  if not (FDownloadStatus in [dsNone, dsDownloadError]) then exit;

  Log (uvlInfo, 'procedure TUpdateChecker.CreateThreadCheckForUpdate; [Begin]');

  try
    ExtractVersionSegments(FInstalledVersion, Major, Miner, Release, Build);
    FVersion                  := '';
    FURLDownload              := '';
    FReleaseNotes             := '';
    FTargetFileName           := '';
    FPathToInstall            := '';
    FThreadTerminated         := tsRunning;
    FThread                   := TUpdateCheckerThread.Create;
    FThread.FUpdateChecker    := Self;
    FThread.FProgramName      := FProgramName;
    FThread.FWebServiceURL    := WebServiceURL;
    FThread.FEnterprise       := GetLogEnterprise;
    FThread.FStore            := GetLogStore;
    FThread.FComputerNumber   := GetLogComputerName;
    FThread.FComputerID       := FComputerID;
    FThread.FExternalIP       := FExternalIP;
    FThread.FLocalIP          := FLocalIP;
    FThread.FComponentVersion := FComponentVersion;
    FThread.FCommand          := tcGetVersionInfo;
    FThread.FCurrMajor        := Major;
    FThread.FCurrMiner        := Miner;
    FThread.FCurrRelease      := Release;
    FThread.OnTerminate       := OnThreadTerminated;
    FThread.FreeOnTerminate   := False;

    ExtractVersionSegments(FDBVersion, Major, Miner, Release, Build);
    FThread.FDBMajor   := Major;
    FThread.FDBMiner   := Miner;
    FThread.FDBRelease := Release;

    Log (uvlDebug, 'WebServiceURL: ' + FThread.FWebServiceURL);
    Log (uvlDebug, Format ('Program Name: [%s], Enterprise [%s], Store [%s], ComputerNumber: [%s]', [FThread.FProgramName, FThread.FEnterprise, FThread.FStore, FThread.FComputerNumber]));
    Log (uvlDebug, Format ('External IP: [%s], Local IP: [%s], ComputerID: [%s]', [FExternalIP, FLocalIP, FComputerID]));
    Log (uvlDebug, Format ('Command: [tcGetVersionInfo], Version: [%d.%d.%d], DB: [%d.%d.%d], ComponentVersion [%s]', [FThread.FCurrMajor, FThread.FCurrMiner, FThread.FCurrRelease, FThread.FDBMajor, FThread.FDBMiner, FThread.FDBRelease, FComponentVersion]));

    FThread.Resume;
    FTimer.Enabled := True;
  except
    on E: Exception do
      Log (uvlError, Format ('TUpdateChecker.CreateThreadCheckForUpdate Error: [%s]', [E.Message]));
  end;

  Log (uvlInfo, 'procedure TUpdateChecker.CreateThreadCheckForUpdate; [Begin]');
end;

procedure TUpdateChecker.VerifyDirectories;
begin
  Log (uvlInfo, 'procedure TUpdateChecker.VerifyDirectories; [Begin]');
  if not FRootDir.IsEmpty and not FDownloadDir.IsEmpty then exit;

  if FIsProgramFilesDir then
  begin
    FRootDir := GetSpecialFolderPath (CSIDL_APPDATA);

    if not FCompany.IsEmpty then
    begin
      FRootDir := FRootDir + '\' + Company;

      if not DirectoryExists (FRootDir) then
      begin
        CreateDir (FRootDir);
        Log (uvlInfo, Format ('CreateDir (''%s'');', [FRootDir]));
      end;
    end;

    if not FAppDirectory.IsEmpty then
      FRootDir := FRootDir + '\' + FAppDirectory
    else
      FRootDir := FRootDir + '\' + TPath.GetFileNameWithoutExtension(ParamStr (0));

    if not DirectoryExists (FRootDir) then
    begin
      CreateDir (FRootDir);
      Log (uvlInfo, Format ('CreateDir (''%s'');', [FRootDir]));
    end;
  end
  else
    FRootDir := ExtractFileDir (ParamStr (0));

  FDownloadDir := FRootDir + '\AutoUpdater';
  if not DirectoryExists (FDownloadDir) then
  begin
    CreateDir(FDownloadDir);
    Log (uvlInfo, Format ('CreateDir (''%s'');', [FDownloadDir]));
  end;

  FLogDir := FDownloadDir + '\Log';
  if not DirectoryExists  (FLogDir) then
  begin
    CreateDir (FLogDir);
    Log (uvlInfo, Format ('CreateDir (''%s'');', [FLogDir]));
  end;

  Log (uvlInfo, 'procedure TUpdateChecker.VerifyDirectories; [End]');
end;

procedure TUpdateChecker.InternalCheckForUpdate (ForceCheck: boolean);
var
  RN, Upd, IniVer, s, Path, Source, Target, BakFile: string;
  IniFirst, MarkUpdateAsDisplayed: boolean;
  Ini, Rel: TStringList;
  Mem: TMemoryIniStructure;
  Compare: integer;
  Major, Miner, Release, Build: integer;

  procedure RegQueryStringValue (RootKey: HKey; RootRegistry, AKey: string; var AValue: string);
  var
    Reg: TRegistry;
  begin
    AValue := '';
    Reg    := TRegistry.Create;
    try
       Reg.RootKey := RootKey;
       if Reg.OpenKeyReadOnly (RootRegistry) then
       try
         try
           AValue := Reg.ReadString(AKey);
         except
         end;
       finally
         Reg.CloseKey;
       end;

    finally
      Reg.Free;
    end;
  end;

  procedure ReadEnvironmentVar (VarName: string; var AVar: string);
  begin
    if AVar.IsEmpty then
    begin
      RegQueryStringValue(HKEY_LOCAL_MACHINE, 'SYSTEM\CurrentControlSet\Control\Session Manager\Environment', VarName, AVar);
      Log (uvlDebug, Format ('Read registry environment variable %s: %s', [VarName, AVar]));
    end;
  end;

begin
  Log (uvlInfo, Format ('procedure TUpdateChecker.InternalCheckForUpdate (%s); [Begin]', [BooleanToString (ForceCheck)]));

  if FProgramName.IsEmpty then
  begin
    FProgramName := ExtractFileName (ParamStr (0));
    Log (uvlDebug, 'Get FProgramName: ' + FProgramName);
  end;

  if FInstalledVersion.IsEmpty then
  begin
    FInstalledVersion := GetAppVersionStr;
    Log (uvlDebug, 'Get FInstalledVersion: ' + FInstalledVersion);
  end;

  VerifyDirectories;
  ReadEnvironmentVar ('EnterpriseID', FEnterprise);
  ReadEnvironmentVar ('StoreID',      FStore);
  ReadEnvironmentVar ('ComputerName', FComputerNumber);

  DeletePreviousLogs;

  if FLocalIP.IsEmpty then
  begin
    FLocalIP := GetLocalIP;
    Log (uvlDebug, 'Get FLocalIP: ' + FLocalIP);
  end;

  if FExternalIP.IsEmpty and not Assigned (FThread) then
  begin
    Log (uvlInfo, 'Create thread to get external IP');
    FThreadTerminated       := tsRunning;
    FThread                 := TUpdateCheckerThread.Create;
    FThread.FUpdateChecker  := Self;
    FThread.FCommand        := tcGetExternalIP;
    FThread.OnTerminate     := OnThreadTerminated;
    FThread.FreeOnTerminate := False;
    FThread.Resume;
    FTimer.Enabled := True;
    exit;
  end;

  RN  := FRootDir + '\' + FReleaseNotesFileName;
  Upd := FRootDir + '\' + FUpdatesIniFileName;
  Log (uvlDebug, 'FReleaseNotesFileName: ' + RN);
  Log (uvlDebug, 'FUpdatesIniFileName: ' + Upd);

  if FileExists (Upd) then
  begin
    Log (uvlInfo, 'Exists an update');
    Ini := TStringList.Create;
    Mem := TMemoryIniStructure.Create;
    try
      s := TFile.ReadAllText(Upd);

      if FEncrypted and not FEncryptionKey.IsEmpty then
        s := Mem.DecryptStringList(s, FEncryptionKey);

      Ini.Text := s;
      Mem.LoadFromStringList(Ini);

      IniVer   := Mem.ReadString ('AppInfo', 'Version', '0.0.0.0');
      IniFirst := Mem.ReadBool   ('AppInfo', 'First',   False);
      Compare  := CompareVersion (FInstalledVersion, IniVer);
      Log (uvlDebug, 'Updated version: ' + IniVer);
      Log (uvlDebug, 'Installed version: ' + FInstalledVersion);
      Log (uvlDebug, 'First time run: ' + BooleanToString (IniFirst));

      if (Compare = coEqual) and IniFirst then
      begin
        Log (uvlInfo, 'First running of new version: ' + IniVer);
        Rel := TStringList.Create;
        try
          Path   := Mem.ReadString ('AppInfo', 'PathToInstall', '');
          Source := Mem.ReadString ('AppInfo', 'Installer',     '');

          //Calculate filenames
          Target := ExtractFilePath (ParamStr (0));
          if not Path.IsEmpty then
            Target := Target + Path + '\';
          Target := Target + Source;

          BakFile := Target + '.bak';

          //Delete bak file if exists
          DeleteFile (PWideChar (BakFile));
          Log (uvlDebug, 'Delete bak file: ' + BakFile);

          if FileExists (RN) then
            Rel.LoadFromFile(RN);

          MarkUpdateAsDisplayed := True;
          if Assigned (FOnFirstRunNewVersion) then
          begin
            Log (uvlInfo, 'Fires OnFirstRunNewVersion event');
            FOnFirstRunNewVersion (Self, Rel.Text, MarkUpdateAsDisplayed);
          end;

          //Deletes Updates.ini file
          DeleteFile (PWideChar (Upd));
          Log (uvlInfo, Format ('Updates ini file [%s]', [Upd]));

          Log (uvlInfo, 'Notifies to web service that update is applied successfuly');
          ExtractVersionSegments(FInstalledVersion, Major, Miner, Release, Build);
          if FVersion.IsEmpty then
            FVersion := Format ('%d.%d.%d', [Major, Miner, Release]);

          FThreadTerminated         := tsRunning;
          FThread                   := TUpdateCheckerThread.Create;
          FThread.FUpdateChecker    := Self;
          FThread.FProgramName      := FProgramName;
          FThread.FWebServiceURL    := WebServiceURL;
          FThread.FCommand          := tcLogUpdatedDone;
          FThread.FVersion          := FVersion;
          FThread.FCurrMajor        := Major;
          FThread.FCurrMiner        := Miner;
          FThread.FCurrRelease      := Release;
          FThread.FEnterprise       := GetLogEnterprise;
          FThread.FStore            := GetLogStore;
          FThread.FComputerNumber   := GetLogComputerName;
          FThread.FComputerID       := FComputerID;
          FThread.FExternalIP       := FExternalIP;
          FThread.FLocalIP          := FLocalIP;
          FThread.FComponentVersion := FComponentVersion;
          FThread.OnTerminate       := OnThreadTerminated;
          FThread.FreeOnTerminate   := False;

          Log (uvlDebug, 'FThread.FProgramName:      ' + FThread.FProgramName);
          Log (uvlDebug, 'FThread.FWebServiceURL:    ' + FThread.FWebServiceURL);
          Log (uvlDebug, 'FThread.FCommand:          tcLogUpdatedDone');
          Log (uvlDebug, 'FThread.FVersion:          ' + FThread.FVersion);
          Log (uvlDebug, 'FThread.FCurrMajor:        ' + FThread.FCurrMajor.ToString);
          Log (uvlDebug, 'FThread.FCurrMiner:        ' + FThread.FCurrMiner.ToString);
          Log (uvlDebug, 'FThread.FCurrRelease:      ' + FThread.FCurrRelease.ToString);
          Log (uvlDebug, 'FThread.FEnterprise:       ' + FThread.FEnterprise);
          Log (uvlDebug, 'FThread.FStore:            ' + FThread.FStore);
          Log (uvlDebug, 'FThread.FComputerNumber:   ' + FThread.FComputerNumber);
          Log (uvlDebug, 'FThread.FComputerID:       ' + FThread.FComputerID);
          Log (uvlDebug, 'FThread.FExternalIP:       ' + FThread.FExternalIP);
          Log (uvlDebug, 'FThread.FLocalIP:          ' + FThread.FLocalIP);
          Log (uvlDebug, 'FThread.FComponentVersion: ' + FThread.FComputerNumber);
          FThread.Resume;
        finally
          Rel.Free;
        end;
      end
      else if not FExitNotified and (Compare = coLower) then
      begin
        Log (uvlInfo, 'New version installed and must be installed');
        FTargetFileName   := Mem.ReadString('AppInfo', 'Installer',     '');
        FPathToInstall    := Mem.ReadString('AppInfo', 'PathToInstall', '');
        FInstallerType    := Mem.ReadString('AppInfo', 'InstallerType', '');
        FIdleNotified     := True;
        FUpdateForInstall := True;

        if Assigned (FOnNotifyMustExitForUpdate) then
        begin
          FExitNotified := True;
          Log (uvlInfo, 'Fires OnNotifyMustExitForUpdate event');
          FOnNotifyMustExitForUpdate (Self);
        end;

        Log (uvlDebug, 'FTargetFileName:   ' + FTargetFileName);
        Log (uvlDebug, 'FPathToInstall:    ' + FPathToInstall);
        Log (uvlDebug, 'FInstallerType:    ' + FInstallerType);
        Log (uvlDebug, 'FIdleNotified:     ' + BooleanToString (FIdleNotified));
        Log (uvlDebug, 'FUpdateForInstall: ' + BooleanToString (FUpdateForInstall));
        Log (uvlDebug, 'FExitNotified:     ' + BooleanToString (FExitNotified));
        FMinTimer.Enabled := False;
      end;
    finally
      Ini.Free;
      Mem.Free;
    end;
  end;

  if not Assigned (FThread) and (ForceCheck or not FIdleNotified) then
    CreateThreadCheckForUpdate;
  Log (uvlInfo, Format ('procedure TUpdateChecker.InternalCheckForUpdate (%s); [End]', [BooleanToString (ForceCheck)]));
end;


procedure TUpdateChecker.CheckForUpdate;
begin
  Log (uvlInfo, 'procedure TUpdateChecker.CheckForUpdate; [Begin]');
  InternalCheckForUpdate(True);
  Log (uvlInfo, 'procedure TUpdateChecker.CheckForUpdate; [End]');
end;

procedure TUpdateChecker.Update;
var
  Params, ErrMsg: string;
  Successful: boolean;
begin
  if not UpdateForInstall then exit;

  Log (uvlInfo, 'procedure TUpdateChecker.Update; [Begin]');
  if FInstallerType = 'Installer' then
  begin
    Log (uvlInfo, 'Update by installer');
    Params := '/CLOSEAPPLICATIONS ';
    if not FPathToInstall.IsEmpty then
      Params := Params + Format ('DIR="%s"', [FPathToInstall]);

    Log (uvlDebug, Format ('ShellExecute(%x, ''open'', PChar(''%s''), PChar(''%s''), nil, SW_SHOWNORMAL); ',
                          [FApplication.Handle, FDownloadDir + '\' + FTargetFileName, Params]));
    ShellExecute(FApplication.Handle,
                 'open',
                 PChar(FDownloadDir + '\' + FTargetFileName),
                 PChar(Params),
                 nil,
                 SW_SHOWNORMAL);
  end
  else if FInstallerType = 'Replacement' then
  begin
    Log (uvlInfo, 'Update by replacement');
    UpdateItself(Successful, ErrMsg);

    if not Successful and Assigned (FOnUpdateError) then
    begin
      Log (uvlError, 'Error applying the update: ' + ErrMsg);
      FOnUpdateError (Self, ErrMsg);
    end;
  end;
  Log (uvlInfo, 'procedure TUpdateChecker.Update; [Begin]');
end;

//Deprecated
procedure TUpdateChecker.UpdateReplacement (FileName: string; Path: string = ''; TimeOut: cardinal = 3);
var
  Params, UpdaterFileName: string;
begin
  UpdaterFileName := ExtractFilePath (ParamStr (0)) + 'Updater.exe';
  Params          := Format ('App="%s" PID=%d TimeOut=%d Replace_FileName="%s"',
                            [ParamStr (0), GetCurrentProcessId, TimeOut, FileName]);

  if not Path.IsEmpty then
    Params := Params + Format (' Path="%s"', [Path]);

  ShellExecute(0, 'open', PChar(UpdaterFileName), PChar (Params), nil, SW_SHOWNORMAL)
end;

procedure TUpdateChecker.UpdateItself (var Successful: boolean; var ErrMsg: string);
var
  TargetPathFileName, SourceFileName, BakFile: string;

  procedure LaunchApp;
  var
    i: integer;
    Params: string;
  begin
    Params := '';
    for i := 1 to ParamCount do
      Params := Params + ParamStr (i) + ' ';

    Log (uvlInfo, Format ('ShellExecute(0, ''open'', PChar(''%s''), PChar (''%s''), nil, SW_SHOWNORMAL);', [ParamStr (0), Params]));
    ShellExecute(0, 'open', PChar(ParamStr (0)), PChar (Params), nil, SW_SHOWNORMAL);
  end;
begin
  Log (uvlInfo, 'procedure TUpdateChecker.UpdateItself (var Successful: boolean; var ErrMsg: string); [Begin]');
  //Initialize output variables
  Successful := False;
  ErrMsg     := 'Operation not completed';

  //Calculate filenames
  TargetPathFileName := ExtractFilePath (ParamStr (0));
  if not FPathToInstall.IsEmpty then
    TargetPathFileName := TargetPathFileName + FPathToInstall + '\';
  TargetPathFileName := TargetPathFileName + TargetFileName;

  SourceFileName := FDownloadDir + '\' + FTargetFileName;
  BakFile        := TargetPathFileName + '.bak';

  //Delete bak file if exists
  Log (uvlInfo, Format ('Delete %s if exists', [BakFile]));
  DeleteFile (PWideChar (BakFile));

  if not FileExists (TargetPathFileName) then
  begin
    // If it doesn't exist, just copy it
    if not CopyFile(PWideChar (SourceFileName), PWideChar (TargetPathFileName), False) then
    begin
      ErrMsg := Format ('File [%s] can''t be copied to [%s]', [SourceFileName, TargetPathFileName]);
      exit;
    end;
    Log (uvlInfo, Format ('Copy file %s to %s', [SourceFileName, TargetPathFileName]));

    //Try to delete the source file, it it can't, do not send error
    DeleteFile (PWideChar (SourceFileName));
    Log (uvlInfo, 'Try delete ' + SourceFileName);

    //Executes the app again
    LaunchApp;
    Successful := True;
    ErrMsg     := '';
  end
  else
  begin
    //If the file exists, rename it
    if RenameFile (TargetPathFileName, BakFile) then
    begin
      Log (uvlInfo, Format ('Rename from %s to %s', [TargetPathFileName, BakFile]));

      //Try to copy the new file from download directory to the specified directory
      if CopyFile(PWideChar (SourceFileName), PWideChar (TargetPathFileName), False) then
      begin
        Log (uvlInfo, Format ('Copy %s to %s', [SourceFileName, TargetPathFileName]));

        if FileExists (TargetPathFileName) then
        begin
          //Delete source file from download directory
          DeleteFile(PWideChar (SourceFileName));
          Log (uvlInfo, 'Delete ' + SourceFileName);

          //Delete bak file
          DeleteFile (PWideChar (BakFile));
          Log (uvlInfo, 'Try delete ' + BakFile);

          //Executes the app again
          LaunchApp;
          Successful := True;
          ErrMsg     := '';
        end
        else
        begin
          //Could not copy the file. Restores it and issues the error message
          RenameFile (BakFile, TargetPathFileName);

          ErrMsg := Format ('File [%s] can''t be copied to [%s]', [SourceFileName, TargetPathFileName]);
          Log (uvlError, ErrMsg);
        end;
      end
      else
      begin
        //Could not copy the file. Restores it and issues the error message
        RenameFile (BakFile, TargetPathFileName);

        ErrMsg := Format ('File [%s] can''t be copied to [%s]', [SourceFileName, TargetPathFileName]);
        Log (uvlError, ErrMsg);
      end;
    end
    else
    begin
      //Could not rename the file, issues the error message
      ErrMsg := Format ('File [%s] can''t be renamed to [%s]', [TargetPathFileName, BakFile]);
      Log (uvlError, ErrMsg);
    end;
  end;
  Log (uvlInfo, Format ('procedure TUpdateChecker.UpdateItself (%s, ''%s''); [End]', [BooleanToString (Successful), ErrMsg]));
end;


procedure TUpdateChecker.OnTimerMinSec (Sender: TObject);
begin
  if (FMinTimer.Interval = '00:00') or (FMinTimer.Interval = '00:00:00') then exit;

  InternalCheckForUpdate (False);
end;

procedure TUpdateChecker.BeginCheckUpdate;
begin
  Log (uvlInfo, 'procedure TUpdateChecker.BeginCheckUpdate; [Begin]');

  FTimer.Enabled := True;
  Log (uvlInfo, 'Start FTimer');

  InternalCheckForUpdate(True);

  Log (uvlInfo, 'Start FMinTimer');
  FMinTimer.Enabled := True;

  Log (uvlInfo, 'procedure TUpdateChecker.BeginCheckUpdate; [End]');
end;

procedure TUpdateChecker.EndCheckUpdate;
begin
  Log (uvlInfo, 'procedure TUpdateChecker.EndCheckUpdate; [Begin]');

  FMinTimer.Enabled := False;

  Log (uvlInfo, 'procedure TUpdateChecker.EndCheckUpdate; [End]');
end;

procedure TUpdateChecker.SaveIniFile;
var
  Ini, Rel: TStringList;
  Mem: TMemoryIniStructure;
  Upd, RN, s: string;
begin
  Log (uvlInfo, 'procedure TUpdateChecker.SaveIniFile; [Begin]');

  Upd := FRootDir + '\' + FUpdatesIniFileName;
  RN  := FRootDir + '\' + FReleaseNotesFileName;
  Ini := TStringList.Create;
  Rel := TStringList.Create;
  Mem := TMemoryIniStructure.Create;

  try
    if FileExists (Upd) then
    begin
      Ini.LoadFromFile(Upd);
      Mem.LoadFromStringList(Ini);
    end;

    Mem.Writestring ('AppInfo', 'Version',       FVersion);
    Mem.WriteBool   ('AppInfo', 'First',         True);
    Mem.WriteString ('AppInfo', 'Installer',     FTargetFileName);
    Mem.WriteString ('AppInfo', 'PathToInstall', FPathToInstall);
    Mem.WriteString ('AppInfo', 'InstallerType', FInstallerType);

    Log (uvlDebug, 'Version:       ' + FVersion);
    Log (uvlDebug, 'First:         True');
    Log (uvlDebug, 'Installer:     ' + FTargetFileName);
    Log (uvlDebug, 'PathToInstall: ' + FPathToInstall);
    Log (uvlDebug, 'InstallerType: ' + FInstallerType);

    if FEncrypted and not FEncryptionKey.IsEmpty then
      s := Mem.EncryptContent(FEncryptionKey)
    else
    begin
      Ini.Clear;
      Ini.AddStrings(Mem.SaveToStringList);
      s := Ini.Text;
    end;

    TFile.WriteAllText(Upd, s);

    Rel.Text := FReleaseNotes;
    Rel.SaveToFile(RN);
  finally
    Rel.Free;
    Mem.Free;
    Ini.Free;
  end;

  Log (uvlInfo, 'procedure TUpdateChecker.SaveIniFile; [End]');
end;

procedure TUpdateChecker.OnTimer (Sender: TObject);
var
  StartDownload, IniFirst: boolean;
  IniVersion, IniInstaller, IniPathToInstall, IniInstallerType: string;
  Major, Miner, Release, Build: integer;
begin
  if not FExitNotified and FDownloadFinished and not FIdleNotified and (FUpdatePriority = upOnIdle) and (SecondsIdle > FIdleSeconds) then
  begin
    FIdleNotified     := True;
    FUpdateForInstall := True;

    if Assigned (FOnNotifyMustExitForUpdate) then
    begin
      FOnNotifyMustExitForUpdate (Self);
      FExitNotified := True;

      Log (uvlInfo, 'TUpdateChecker.OnTimer Fires OnNotifyMustExitForUpdate');
    end;
  end;

  if (FThreadTerminated = tsTerminated) and Assigned (FThread) then
  begin
    Log (uvlInfo, 'TUpdateChecker.OnTimer Command: ' + GetEnumName (TypeInfo (TThreadCommand), Integer (FThread.Command)));
    case FThread.Command  of
      tcNone: ;
      tcGetExternalIP: begin
        FExternalIP  := FThread.FExternalIP;
        Log (uvlDebug, 'External IP: ' + FExternalIP);
        FreeAndNil (FThread);

        if not Assigned (FThread) and not FIdleNotified then
          InternalCheckForUpdate(True);

        exit;
      end;
      tcGetVersionInfo: begin
        FVersion        := FThread.Version;
        FURLDownload    := FThread.URLDownload;
        FReleaseNotes   := FThread.ReleaseNotes;
        FTargetFileName := FThread.TargetFileName;
        FPathToInstall  := FThread.PathToInstall;
        FInstallerType  := FThread.InstallerType;
        FUpdatePriority := FThread.FUpdatePriority;
        FreeAndNil (FThread);

        if Assigned (FOnAfterCheck) then
        begin
          Log (uvlInfo, 'TUpdateChecker.OnTimer Fires OnAfterCheck');
          FOnAfterCheck (Self);
        end;

        ReadIniValues (IniVersion, IniInstaller, IniPathToInstall, IniInstallerType, IniFirst);
        if not FVersion.IsEmpty and (IniVersion.IsEmpty or (CompareVersion (IniVersion, FVersion) = coLower)) then
        begin
          Log (uvlDebug, 'FDownloadStatus: ' + GetEnumName (TypeInfo (TDownloadStatus), Integer (FDownloadStatus)));
          if (CompareVersion (FVersion, FInstalledVersion) = coGreater) and (FDownloadStatus in [dsNone, dsDownloadError]) then
          begin
            FMinTimer.Enabled := False;
            StartDownload     := True;
            if Assigned (FOnFoundNewAppVersion) then
            begin
              Log (uvlInfo, 'TUpdateChecker.OnTimer Fires OnFoundNewAppVersion');
              FOnFoundNewAppVersion (Self, FProgramName, Version, FURLDownload, FReleaseNotes, FTargetFileName, FPathToInstall, FInstallerType, StartDownload);
              Log (uvlDebug, 'FProgramName:    ' + FProgramName);
              Log (uvlDebug, 'Version:         ' + Version);
              Log (uvlDebug, 'FURLDownload:    ' + FURLDownload);
              Log (uvlDebug, 'FReleaseNotes:   ' + FReleaseNotes);
              Log (uvlDebug, 'FTargetFileName: ' + FTargetFileName);
              Log (uvlDebug, 'FPathToInstall:  ' + FPathToInstall);
              Log (uvlDebug, 'FInstallerType:  ' + FInstallerType);
              Log (uvlDebug, 'StartDownload:   ' + BooleanToString (StartDownload));
            end;

            if StartDownload then
            begin
              FIdleNotified     := False;
              FUpdateForInstall := False;
              if Assigned (FOnStartDownload) then
              begin
                Log (uvlInfo, 'TUpdateChecker.OnTimer Fires OnAfterCheck');
                Log (uvlDebug, 'FProgramName:    ' + FProgramName);
                Log (uvlDebug, 'FVersion:        ' + FVersion);
                Log (uvlDebug, 'FURLDownload:    ' + FURLDownload);
                Log (uvlDebug, 'FTargetFileName: ' + FTargetFileName);
                Log (uvlDebug, 'FPathToInstall:  ' + FPathToInstall);
                FOnStartDownload (Self, FProgramName, FVersion, FURLDownload, FTargetFileName, FPathToInstall);
              end;

              FThreadTerminated       := tsRunning;
              FThread                 := TUpdateCheckerThread.Create;
              FThread.FUpdateChecker  := Self;
              FThread.FProgramName    := FProgramName;
              FThread.FWebServiceURL  := WebServiceURL;
              FThread.FCommand        := tcDownloadFile;
              FThread.FDownloadDir    := FDownloadDir;
              FThread.FURLDownload    := FURLDownload;
              FThread.FTargetFileName := FTargetFileName;
              FThread.FPathToInstall  := FPathToInstall;
              FThread.OnTerminate     := OnThreadTerminated;
              FThread.FreeOnTerminate := False;
              FDownloadStatus         := dsStartDownloading;
              Log (uvlInfo, 'TUpdateChecker.OnTimer Start downloading file');
              Log (uvlDebug, 'FThread.FProgramName:    ' + FThread.FProgramName);
              Log (uvlDebug, 'FThread.FWebServiceURL:  ' + FThread.FWebServiceURL);
              Log (uvlDebug, 'FThread.FCommand:        tcDownloadFile');
              Log (uvlDebug, 'FThread.FURLDownload:    ' + FThread.FURLDownload);
              Log (uvlDebug, 'FThread.FDownloadDir:    ' + FThread.FDownloadDir);
              Log (uvlDebug, 'FThread.FTargetFileName: ' + FThread.FTargetFileName);
              Log (uvlDebug, 'FThread.FPathToInstall:  ' + FThread.FPathToInstall);
              Log (uvlDebug, 'FDownloadStatus:         ' + GetEnumName (TypeInfo (TDownloadStatus), Integer (FDownloadStatus)));

              FThread.Resume;
              exit;
            end;
          end
        end;
      end;
      tcDownloadFile: begin
        if FThread.ErrMsg = '' then
        begin
          FDownloadStatus := dsFinishedDownloading;
          Log (uvlDebug, 'FDownloadStatus: ' + GetEnumName (TypeInfo (TDownloadStatus), Integer (FDownloadStatus)));
          if Assigned (FOnFinishDownload) then
          begin
            Log (uvlInfo, 'TUpdateChecker.OnTimer Fires OnFinishDownload');
            Log (uvlDebug, 'FProgramName:    ' + FProgramName);
            Log (uvlDebug, 'FVersion:        ' + FVersion);
            Log (uvlDebug, 'FURLDownload:    ' + FURLDownload);
            Log (uvlDebug, 'FTargetFileName: ' + FTargetFileName);
            Log (uvlDebug, 'FPathToInstall:  ' + FPathToInstall);
            FOnFinishDownload (Sender, FProgramName, FVersion, FURLDownload, FTargetFileName, FPathToInstall);
          end;

          SaveIniFile;

          ExtractVersionSegments(FDBVersion, Major, Miner, Release, Build);
          FThreadTerminated         := tsRunning;
          FThread                   := TUpdateCheckerThread.Create;
          FThread.FUpdateChecker    := Self;
          FThread.FProgramName      := FProgramName;
          FThread.FWebServiceURL    := WebServiceURL;
          FThread.FCommand          := tcLogDownloadDone;
          FThread.FVersion          := FVersion;
          FThread.FDBMajor          := Major;
          FThread.FDBMiner          := Miner;
          FThread.FDBRelease        := Release;
          FThread.FEnterprise       := GetLogEnterprise;
          FThread.FStore            := GetLogStore;
          FThread.FComputerNumber   := GetLogComputerName;
          FThread.FComputerID       := FComputerID;
          FThread.FExternalIP       := FExternalIP;
          FThread.FLocalIP          := FLocalIP;
          FThread.FComponentVersion := FComponentVersion;
          FThread.OnTerminate       := OnThreadTerminated;
          FThread.FreeOnTerminate   := False;
          Log (uvlInfo, 'TUpdateChecker.OnTimer Notifies the web service of the completed download');
          Log (uvlDebug, 'FThread.FProgramName:      ' + FThread.FProgramName);
          Log (uvlDebug, 'FThread.FWebServiceURL:    ' + FThread.FWebServiceURL);
          Log (uvlDebug, 'FThread.FVersion:          ' + FThread.FVersion);
          Log (uvlDebug, 'DBVersion:                 ' + Format ('%d.%d.%d', [Major, Miner, Release]));
          Log (uvlDebug, 'FThread.FEnterprise:       ' + FThread.FEnterprise);
          Log (uvlDebug, 'FThread.FStore:            ' + FThread.FStore);
          Log (uvlDebug, 'FThread.FComputerNumber:   ' + FThread.FComputerNumber);
          Log (uvlDebug, 'FThread.FComputerID:       ' + FThread.FComputerID);
          Log (uvlDebug, 'FThread.FExternalIP:       ' + FThread.FExternalIP);
          Log (uvlDebug, 'FThread.FLocalIP:          ' + FThread.FLocalIP);
          Log (uvlDebug, 'FThread.FComponentVersion: ' + FThread.FComputerNumber);
          FThread.Resume;

          FDownloadFinished     := True;
          FDownloadFinishedTick := GetTickCount;
          FIdleNotified         := False;
          FUpdateForInstall     := True;

          if FUpdatePriority = upNow then
          begin
            FIdleNotified := True;
            if Assigned (FOnNotifyMustExitForUpdate) then
            begin
              Log (uvlInfo, 'TUpdateChecker.OnTimer Fires OnNotifyMustExitForUpdate');
              FOnNotifyMustExitForUpdate (Self);
            end;
          end;

          exit;
        end
        else
        begin
          FDownloadStatus := dsDownloadError;
          Log (uvlDebug, 'FDownloadStatus: ' + GetEnumName (TypeInfo (TDownloadStatus), Integer (FDownloadStatus)));
          if Assigned (FOnErrorDownload) then
          begin
            Log (uvlInfo, 'TUpdateChecker.OnTimer Fires OnErrorDownload');
            Log (uvlDebug, 'FProgramName:    ' + FProgramName);
            Log (uvlDebug, 'FVersion:        ' + FVersion);
            Log (uvlDebug, 'FURLDownload:    ' + FURLDownload);
            Log (uvlDebug, 'FTargetFileName: ' + FTargetFileName);
            Log (uvlDebug, 'FPathToInstall:  ' + FPathToInstall);
            Log (uvlDebug, 'FThread.ErrMsg:  ' + FThread.ErrMsg);

            FOnErrorDownload (Sender, FProgramName, FVersion, FURLDownload, FTargetFileName, FPathToInstall, FThread.ErrMsg);
          end;
        end;
        FreeAndNil(FThread);
      end;
      tcLogDownloadDone,
      tcLogUpdatedDone: FreeAndNil (FThread);
    end;

    FThreadTerminated := tsNone;
  end;
end;

procedure TUpdateChecker.OnThreadTerminated (Sender: TObject);
begin
  FThreadTerminated := tsTerminated;
end;

//Deprecated
function TUpdateChecker.SyncEcho (s: string; var errMsg: string): string;
begin
  result := '';
  errMsg := '';
  try
    result := GetIAutoUpdater (False, FWebServiceURL).echoStr(s);
  except
    on E: Exception do
      errMsg := E.Message;
  end;
end;

{$endregion}

procedure Register;
begin
  System.Classes.RegisterComponents('Compudime', [TUpdateChecker]);
end;

end.
