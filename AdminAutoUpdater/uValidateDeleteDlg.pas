unit uValidateDeleteDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  Vcl.StdCtrls, cxButtons;

type
  TFValidateDeleteDlg = class(TForm)
    Label2: TLabel;
    edPassword: TEdit;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure edPasswordChange(Sender: TObject);
  private
    { Private declarations }
    LoginRetries: integer;
    LoginCount:   integer;
  public
    { Public declarations }
    Password: string;
  end;

var
  FValidateDeleteDlg: TFValidateDeleteDlg;

implementation

{$R *.dfm}

procedure TFValidateDeleteDlg.btnOKClick(Sender: TObject);
begin
  Inc (LoginCount);

  if edPassword.Text = Password then
    ModalResult := mrOK
  else
    MessageDlg ('Invalid password!', mtError, [mbOK], 0);

  if LoginCount >= LoginRetries then
  begin
    MessageDlg ('Number of retries exceded!', mtInformation, [mbOK], 0);
    Close;
  end;
end;

procedure TFValidateDeleteDlg.edPasswordChange(Sender: TObject);
begin
  btnOK.Enabled := edPassword.Text <> '';
end;

procedure TFValidateDeleteDlg.FormCreate(Sender: TObject);
begin
  LoginRetries := 3;
end;

procedure TFValidateDeleteDlg.FormShow(Sender: TObject);
begin
  LoginCount      := 0;
  edPassword.Text := '';
end;

end.
