{ Invokable interface IAutoUpdater }

unit AutoUpdaterIntf;

interface

uses Soap.InvokeRegistry, System.Types, Soap.XSBuiltIns;

type
  TVersionInfoRequest = class (TRemotable)
  private
    FApplication:   UnicodeString;
    FEnterprise:    UnicodeString;
    FStore:         UnicodeString;
    FMachineNumber: UnicodeString;
    FDBMajor:       integer;
    FDBMiner:       integer;
    FDBRelease:     integer;
    FCurrMajor:     integer;
    FCurrMiner:     integer;
    FCurrRelease:   integer;
  published
    property Application:   UnicodeString read FApplication   write FApplication;
    property Enterprise:    UnicodeString read FEnterprise    write FEnterprise;
    property Store:         UnicodeString read FStore         write FStore;
    property MachineNumber: UnicodeString read FMachineNumber write FMachineNumber;
    property DBMajor:       integer       read FDBMajor       write FDBMajor;
    property DBMiner:       integer       read FDBMiner       write FDBMiner;
    property DBRelease:     integer       read FDBRelease     write FDBRelease;
    property CurrMajor:     integer       read FCurrMajor     write FCurrMajor;
    property CurrMiner:     integer       read FCurrMiner     write FCurrMiner;
    property CurrRelease:   integer       read FCurrRelease   write FCurrRelease;
  end;

  TVersionInfoResponse = class (TRemotable)
  private
    FApplication:    UnicodeString;
    FVersion:        UnicodeString;
    FURL:            UnicodeString;
    FReleaseNotes:   UnicodeString;
    FTargetFileName: UnicodeString;
    FPathToInstall:  UnicodeString;
    FUpdatePriority: UnicodeString;
    FIdleTime:       UnicodeString;
    FInstallerType:  UnicodeString;
  published
    property Application:    UnicodeString read FApplication    write FApplication;
    property Version:        UnicodeString read FVersion        write FVersion;
    property URL:            UnicodeString read FURL            write FURL;
    property ReleaseNotes:   UnicodeString read FReleaseNotes   write FReleaseNotes;
    property TargetFileName: UnicodeString read FTargetFileName write FTargetFileName;
    property PathToInstall:  UnicodeString read FPathToInstall  write FPathToInstall;
    property UpdatePriority: UnicodeString read FUpdatePriority write FUpdatePriority;
    property IdleTime:       UnicodeString read FIdleTime       write FIdleTime;
    property InstallerType:  UnicodeString read FInstallerType  write FInstallerType;
  end;

  { Invokable interfaces must derive from IInvokable }
  IAutoUpdater = interface(IInvokable)
  ['{CABAF895-888B-4244-9710-74C318769D6D}']

    { Methods of Invokable interface must not use the default }
    { calling convention; stdcall is recommended }
    function echoStr(const Value: UnicodeString): UnicodeString; stdcall;
//    function GetUpgradeVersion (const Request: TVersionInfoRequest): TVersionInfoResponse; stdcall;
//    function LogDownloadDone (const ProgramName, Version, DBVersion, Enterprise, Store, MachineNumber: UnicodeString): UnicodeString; stdcall;
//    function LogUpdateDone (const ProgramName, Version, DBVersion, Enterprise, Store, MachineNumber: UnicodeString): UnicodeString; stdcall;
    function GetUpdateInfo (const Info: UnicodeString): UnicodeString; stdcall;
    function ConfirmDownload (const Info: UnicodeString): UnicodeString; stdcall;
    function ConfirmUpdate (const Info: UnicodeString): UnicodeString; stdcall;
  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface(TypeInfo(IAutoUpdater));

end.
