unit uUploadDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, Vcl.Menus, System.Actions, Vcl.ActnList,
  cxButtons, uAddAppDlg, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.ImageList, Vcl.ImgList,
  Vcl.Grids, Vcl.DBGrids, Vcl.Mask, Vcl.DBCtrls, cxDBEdit, cxCheckBox, cxMemo,
  System.IOUtils, REST.Types, REST.Client, Data.Bind.Components,
  Data.Bind.ObjectScope, System.JSON, uProgramInfo, Vcl.ExtCtrls;

type
  TFUploadDlg = class(TForm)
    Application: TLabel;
    cbApplications: TcxLookupComboBox;
    btnNewApp: TcxButton;
    ActionList1: TActionList;
    actNewApp: TAction;
    Label1: TLabel;
    ImageList1: TImageList;
    actNewVersion: TAction;
    tabUpdates: TFDTable;
    dsUpdates: TDataSource;
    tabUpdatesApplication: TStringField;
    tabUpdatesLastest_Ver_Maj: TIntegerField;
    tabUpdatesLastest_Ver_Min: TIntegerField;
    tabUpdatesLastest_Ver_Rel: TIntegerField;
    tabUpdatesMin_DB_Req_Maj: TIntegerField;
    tabUpdatesMin_DB_Req_Min: TIntegerField;
    tabUpdatesMin_DB_Req_Rel: TIntegerField;
    tabUpdatesURL_Download: TStringField;
    tabUpdatesTargetFileName: TStringField;
    tabUpdatesRelease_Notes: TStringField;
    tabUpdatesPath_To_Install: TStringField;
    tabUpdatesPriority_ID: TIntegerField;
    tabUpdatesIdle_Time: TStringField;
    tabUpdatesInstaller_Type: TIntegerField;
    tabUpdatesActive: TBooleanField;
    tabUpdatesVersion: TStringField;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    edDBMajor: TcxDBTextEdit;
    edDBMiner: TcxDBTextEdit;
    edDBRelease: TcxDBTextEdit;
    edPathToInstall: TcxDBTextEdit;
    edPriority: TcxDBLookupComboBox;
    Label6: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    edIdleTime: TcxDBTextEdit;
    cbInstallerType: TcxDBLookupComboBox;
    cbActive: TcxDBCheckBox;
    edReleaseNotes: TcxDBMemo;
    Label16: TLabel;
    edUploadFileName: TEdit;
    btnSelectFile: TcxButton;
    actSelectFile: TAction;
    OpenDlg: TOpenDialog;
    btnUpload: TcxButton;
    edTargetFileName: TcxDBTextEdit;
    edURL: TcxDBTextEdit;
    btnNewVersion: TcxButton;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    Label9: TLabel;
    edCommitMessage: TcxTextEdit;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    cbVersionInfo: TcxComboBox;
    Label10: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    cbEnterprise: TcxDBComboBox;
    cbStore: TcxDBComboBox;
    cbComputerID: TcxDBComboBox;
    tabUpdatesEnterprise: TStringField;
    tabUpdatesStore: TStringField;
    tabUpdatesComputer_ID: TStringField;
    procedure actNewAppExecute(Sender: TObject);
    procedure cbApplicationsPropertiesChange(Sender: TObject);
    procedure tabUpdatesCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure actSelectFileExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure btnUploadClick(Sender: TObject);
    procedure actNewVersionExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbVersionInfoPropertiesChange(Sender: TObject);
    procedure cbComputerIDPropertiesChange(Sender: TObject);
    procedure cbEnterprisePropertiesEditValueChanged(Sender: TObject);
    procedure cbStorePropertiesEditValueChanged(Sender: TObject);
  private
    { Private declarations }
    FForcedVersion: string;
    procedure SetDataSource (DS: TDataSource);
    function GetAppDirectory (App: string): string;
    function CalculateDownloadFile (App, Version: string): string;
    procedure ExtractWorkspaceRepoSlug (RepoURL: string; var Workspace, RepoSlug: string);
    procedure ForceVersion (Version: string);
    procedure LoadEnterprise;
    procedure LoadStore;
    procedure LoadComputerID;
  public
    { Public declarations }
    procedure ExtractVersionSegments (Version: string; var Major, Miner, Release: integer; var OK: boolean);
  end;

var
  FUploadDlg: TFUploadDlg;

function WinExecAndWait32 (AFileName, AParameters: string; Visibility: integer): DWord;


implementation
uses uMainAdmin;

{$R *.dfm}

{Returns -1 if the Exec failed, otherwise returns the process' exit code when the process terminates }
function WinExecAndWait32 (AFileName, AParameters: string; Visibility: integer): DWord;
var
  zCommandLine: array[0..512] of char;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  StrPCopy(zCommandLine, AFileName + ' ' + AParameters);
  FillChar(StartupInfo, SizeOf(StartupInfo), #0);
  StartupInfo.cb          := Sizeof(StartupInfo);
  StartupInfo.dwFlags     := STARTF_USESHOWWINDOW;
  StartupInfo.wShowWindow := Visibility;

  if not CreateProcess(nil,
                       zCommandLine, { pointer to command line string }
                       nil, { pointer to process security attributes }
                       nil, { pointer to thread security attributes }
                       False, { handle inheritance flag }
                       CREATE_NEW_CONSOLE or { creation flags }
                       NORMAL_PRIORITY_CLASS,
                       nil, { pointer to new environment block }
                       nil, { pointer to current directory name }
                       StartupInfo, { pointer to STARTUPINFO }
                       ProcessInfo
                      ) then { pointer to PROCESS_INF }
    result := DWord(-1)
  else
  begin
    WaitforSingleObject(ProcessInfo.hProcess, INFINITE);
    GetExitCodeProcess(ProcessInfo.hProcess, result);
    CloseHandle(ProcessInfo.hProcess);
    CloseHandle(ProcessInfo.hThread);
  end;
end;

procedure TFUploadDlg.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
  FieldsReady: boolean;
begin
  if Showing then
  begin
    FieldsReady           := not tabUpdates.FieldByName('Installer_Type').IsNull;
//    actSelectFile.Enabled := cbApplications.Text <> '';
    btnOK.Enabled         := (tabUpdates.State in [dsEdit, dsInsert]) and FieldsReady;

    btnUpload.Enabled     := actSelectFile.Enabled and (edUploadFileName.Text <> '') and FieldsReady;
  end;
end;

procedure TFUploadDlg.actNewAppExecute(Sender: TObject);
begin
  if FAddAppDlg.ShowModal = mrOk then
  begin
    cbApplications.EditValue := FMainAdmin.tabApplications.FieldByName('Application').AsString;

    if not FAddAppDlg.FileVer.IsEmpty then
      ForceVersion(FAddAppDlg.FileVer);

    if not FAddAppDlg.FileName.IsEmpty then
      edUploadFileName.Text := FAddAppDlg.FileName;
  end;
end;

procedure TFUploadDlg.ExtractVersionSegments (Version: string; var Major, Miner, Release: integer; var OK: boolean);
var
  A: TArray<string>;
begin
  A  := Version.Split(['.']);
  OK := (Length (A) = 3) and TryStrToInt (A [0], Major) and TryStrToInt (A [1], Miner) and TryStrToInt (A [2], Release)
end;

procedure TFUploadDlg.actNewVersionExecute(Sender: TObject);
var
  v: string;
  i: integer;
  Valid, Quit: boolean;
  Maj, Min, Rel: integer;
  VerDir: string;
begin
  v := '';
  repeat
    Valid := False;
    Quit  := not InputQuery ('Add new version', 'Version number (Maj.Min.Rel)', v);

    if not Quit then
    begin
      ExtractVersionSegments(v, Maj, Min, Rel, Valid);
      if not Valid then
        MessageDlg ('The version is written in invalid form', mtError, [mbOK], 0);
    end;
  until Quit or Valid;

  if Valid then
  begin
    tabUpdates.Cancel;
    tabUpdates.Append;
    tabUpdates.FieldByName('Application').AsString      := cbApplications.Text;
    tabUpdates.FieldByName('Lastest_Ver_Maj').AsInteger := Maj;
    tabUpdates.FieldByName('Lastest_Ver_Min').AsInteger := Min;
    tabUpdates.FieldByName('Lastest_Ver_Rel').AsInteger := Rel;
    tabUpdates.FieldByName('Active').AsBoolean          := True;
    tabUpdates.Post;
    cbApplicationsPropertiesChange(Self);

    v      := Format ('%d.%d.%d', [Maj, Min, Rel]);
    VerDir := FMainAdmin.edRootDirectory.Text;
    if not VerDir.EndsWith ('\') then
      VerDir := VerDir + '\';

    VerDir := Format ('%s\%s\%s', [VerDir, GetAppDirectory(cbApplications.Text), v]);
    ForceDirectories(VerDir);

    ForceVersion(v);
  end;
end;

procedure TFUploadDlg.SetDataSource (DS: TDataSource);
var
  Foreground: TColor;
begin
  if Assigned (DS) then
    Foreground := clWindow
  else
    Foreground := clBtnFace;

  edDBMajor.DataBinding.DataSource        := DS;
  edDBMiner.DataBinding.DataSource        := DS;
  edDBRelease.DataBinding.DataSource      := DS;
  edPathToInstall.DataBinding.DataSource  := DS;
  edPriority.DataBinding.DataSource       := DS;
  edIdleTime.DataBinding.DataSource       := DS;
  cbInstallerType.DataBinding.DataSource  := DS;
  cbActive.DataBinding.DataSource         := DS;
  edReleaseNotes.DataBinding.DataSource   := DS;
  edTargetFileName.DataBinding.DataSource := DS;
  edURL.DataBinding.DataSource            := DS;

  edDBMajor.Style.Color        := Foreground;
  edDBMiner.Style.Color        := Foreground;
  edDBRelease.Style.Color      := Foreground;
  edPathToInstall.Style.Color  := Foreground;
  edPriority.Style.Color       := Foreground;
  edIdleTime.Style.Color       := Foreground;
  cbInstallerType.Style.Color  := Foreground;
  edReleaseNotes.Style.Color   := Foreground;
  edTargetFileName.Style.Color := Foreground;
end;

procedure TFUploadDlg.actSelectFileExecute(Sender: TObject);
var
  FileInfo: TCEVersionInfo;
  FileVer,
  FileName: string;
  OK:       boolean;
  Maj,
  Min,
  Rel:      integer;
begin
  if OpenDlg.Execute (Handle) then
  begin
    //Verifies if application is registered
    FileName := ExtractFileName (OpenDlg.FileName);
    if not FMainAdmin.AppIsRegistered (FileName) then
    begin
      if MessageDlg (Format ('App ''%s'' is not registered, do you want create it now?', [FileName]), mtConfirmation, mbYesNo, 0) = mrYes then
      begin
        FMainAdmin.tabApplications.Append;
        FMainAdmin.tabApplications.FieldByName('Application').AsString     := FileName;
        FMainAdmin.tabApplications.FieldByName('App_Directory').AsString   := TPath.GetFileNameWithoutExtension (FileName);
        FMainAdmin.tabApplications.FieldByName('Update_Type_ID').AsInteger := 3; //LastestRelease
        FMainAdmin.tabApplications.FieldByName('Priority_ID').AsInteger    := 0; //On Program Exit
        FMainAdmin.tabApplications.Post;
        cbApplicationsPropertiesChange (Self);
        cbApplications.EditValue := FMainAdmin.tabApplications.FieldByName('Application').AsString;
      end
      else
      begin
        edUploadFileName.Text := OpenDlg.FileName;
        exit;
      end;
    end;

    FileInfo              := TCEVersionInfo.Create(Self);
    edUploadFileName.Text := OpenDlg.FileName;
    FileVer               := '';
    try
      //Get file version
      FileInfo.GetInfo(OpenDlg.FileName);
      OK := TryStrToInt(FileInfo.MajorVersion, Maj) and
            TryStrToInt(FileInfo.MinorVersion, Min) and
            TryStrToInt(FileInfo.Release,      Rel);

      FileVer := '';
      if OK then
        FileVer := Format ('%s.%s.%s', [FileInfo.MajorVersion, FileInfo.MinorVersion, FileInfo.Release]);

      //The file is equal to application program but file version is distinct to selected version
      if OK and (UpperCase (cbApplications.Text) = UpperCase (ExtractFileName (OpenDlg.FileName))) and
         (cbVersionInfo.Text <> FileVer) then
      begin
        OK := False;

        //If there are changes, ask if you want to save them or discard them
        if tabUpdates.State in [dsInsert, dsEdit] then
        begin
          case MessageDlg (Format ('The file version [%s] is different from the selected one, do you want to save the changes and use the file version? Click Yes to save and use the file version, No to not save and use the file version, and Cancel to keep the selected version.', [FileVer]), mtConfirmation, mbYesNoCancel, 0) of
            mrYes: begin
              OK := True;
              tabUpdates.Post;
            end;
            mrNo: begin
              OK := True;
              tabUpdates.Cancel;
            end;
          end;
        end
        else if (tabUpdates.State = dsBrowse) and
                (MessageDlg (Format ('The file version [%s] is different from the selected one, do you want to use the file version? Press OK to use the file version and Cancel to keep the selected version.', [FileVer]), mtConfirmation, mbOKCancel, 0) = mrOK) then
        begin
          OK := True;
        end;

        //If the version exists, select it, otherwise create it
        if OK then
        begin
          if not tabUpdates.FindKey ([cbApplications.Text, Maj, Min, Rel]) then
          begin
            tabUpdates.Append;
            tabUpdates.FieldByName('Application').AsString      := cbApplications.Text;
            tabUpdates.FieldByName('Lastest_Ver_Maj').AsInteger := Maj;
            tabUpdates.FieldByName('Lastest_Ver_Min').AsInteger := Min;
            tabUpdates.FieldByName('Lastest_Ver_Rel').AsInteger := Rel;
            tabUpdates.FieldByName('Active').AsBoolean          := True;
            tabUpdates.FieldByName('TargetFileName').AsString   := ExtractFileName(OpenDlg.FileName);
            tabUpdates.FieldByName('Installer_Type').AsInteger  := 1; // Replacement
            tabUpdates.Post;
            cbApplicationsPropertiesChange(Self);
          end;
          ForceVersion(FileVer);
        end;

        tabUpdates.Edit;
        tabUpdates.FieldByName ('TargetFileName').AsString := ExtractFileName (edUploadFileName.Text);
      end
      else
      begin
        if tabUpdates.State = dsBrowse then
          tabUpdates.Edit;
      end;

      tabUpdates.FieldByName ('TargetFileName').AsString := ExtractFileName (edUploadFileName.Text);
    finally
      FileInfo.Free;
      edUploadFileName.Text := OpenDlg.FileName;
    end;
  end;
end;

procedure TFUploadDlg.btnOKClick(Sender: TObject);
begin
  if tabUpdates.State in [dsInsert, dsEdit] then
    tabUpdates.Post;

  ModalResult := mrOK;
end;

function TFUploadDlg.GetAppDirectory (App: string): string;
var
  Qry: TFDQuery;
begin
  result := '';
  Qry := TFDQuery.Create(Self);
  try
    Qry.Connection        := FMainAdmin.Conn;
    Qry.FetchOptions.Mode := fmAll;
    Qry.SQL.Clear;
    Qry.SQL.Add('Select App_Directory From Applications Where Upper (Application) = Upper (:App)');
    Qry.ParamByName('App').AsString := cbApplications.Text;
    Qry.Open;

    if not Qry.Eof and not Qry.FieldByName ('App_Directory').IsNull then
      result := Qry.FieldByName ('App_Directory').AsString;
  finally
    Qry.Free;
  end;
end;

procedure TFUploadDlg.btnUploadClick(Sender: TObject);
var
  Dir, Git, Msg, Root, RepoURL, AppDir, CalcFile,
  Workspace, RepoSlug, RestURL, VersionDir,
  DownloadURL: string;
  JsonValue: TJSONValue;
begin
  AppDir := GetAppDirectory(cbApplications.Text);
  if AppDir.IsEmpty then
  begin
    MessageDlg ('App directory is empty, must have value', mtError, [mbOK], 0);
    exit;
  end;

  Root := FMainAdmin.edRootDirectory.Text;
  if Root.EndsWith ('\') then
    Root := Root.Substring(1, Root.Length - 1);

  RepoURL := FMainAdmin.edRepositoryURL.Text;
  if RepoURL.EndsWith ('/') then
    RepoURL := RepoURL.Substring(1, RepoURL.Length - 1);

  VersionDir := Format ('%d.%d.%d',
                        [tabUpdates.FieldByName('Lastest_Ver_Maj').AsInteger,
                         tabUpdates.FieldByName('Lastest_Ver_Min').AsInteger,
                         tabUpdates.FieldByName('Lastest_Ver_Rel').AsInteger
                        ]);

  Dir := Format ('%s\%s\%s', [Root, AppDir, VersionDir]);
  Msg := edCommitMessage.Text;
  Msg := Msg.Replace(' ', '_', [rfReplaceAll]).Replace ('''', '', [rfReplaceAll]).Replace ('"', '', [rfReplaceAll]);

  if Msg.IsEmpty then
    Git := Format ('"%s\gitBatNoWait.bat"', [Root])
  else
    Git := Format ('"%s\gitBatNoWait.bat %s"', [Root, Msg]);
  ForceDirectories(Dir);

  //Copy the selected file to the version directory if is needed
  CalcFile := UpperCase (CalculateDownloadFile(cbApplications.Text, cbVersionInfo.Text));
  if CalcFile <> UpperCase (edUploadFileName.Text) then
    CopyFile(PWideChar (edUploadFileName.Text), PWideChar (Dir + '\' + ExtractFileName (edUploadFileName.Text)), False);

  //Performes push to git
  WinExecAndWait32('CMD', Format ('/c %s', [Git]), SW_HIDE);

  //Calculate the rest utl
  ExtractWorkspaceRepoSlug(FMainAdmin.edRepositoryURL.Text, Workspace, RepoSlug);
  RestURL := Format ('https://api.bitbucket.org/2.0/repositories/%s/%s/src/master/%s/%s/%s',
                     [WorkSpace, RepoSlug, AppDir, VersionDir, ExtractFileName (edUploadFileName.Text)]);

  //Get the download url
  RESTClient1.BaseURL := RestURL;
  DownloadURL         := '';
  try
    RESTRequest1.Execute;
    JsonValue := TJSONObject.ParseJSONValue(RESTResponse1.Content);
    try
      DownloadURL := JsonValue.GetValue<string>('links.self.href');
    finally
      JsonValue.Free;
    end;
  except
    on E: Exception do
    begin
      MessageDlg (Format ('Could not verify that the file was uploaded successfully. Error msg: [%s]', [E.Message]), mtError, [mbOK], 0);
      tabUpdates.Cancel;
      exit;
    end;
  end;

  if tabUpdates.State = dsBrowse then
    tabUpdates.Edit;

  tabUpdates.FieldByName('URL_Download').AsString := DownloadURL;

  //Save data
  tabUpdates.Post;

  MessageDlg ('Version uploaded successful', mtInformation, [mbOK], 0);

  btnOK.Enabled := True;
end;

procedure TFUploadDlg.cbApplicationsPropertiesChange(Sender: TObject);
begin
  SetDataSource (nil);
  try
    tabUpdates.DisableControls;
    tabUpdates.Open;
    tabUpdates.Refresh;
    tabUpdates.Filtered := False;
    tabUpdates.Filter   := Format ('Upper (Application)=''%s''', [UpperCase (cbApplications.Text)]);
    tabUpdates.Filtered := True;

    tabUpdates.First;
    cbVersionInfo.Properties.Items.Clear;
    cbVersionInfo.Text := '';
    while not tabUpdates.eof do
    begin
      cbVersionInfo.Properties.Items.Add(tabUpdatesVersion.AsString);

      tabUpdates.Next;
    end;

    tabUpdates.First;
    cbVersionInfo.Text := '';
  finally
    tabUpdates.EnableControls;
  end;
end;

procedure TFUploadDlg.cbComputerIDPropertiesChange(Sender: TObject);
begin
  LoadComputerID;
end;

procedure TFUploadDlg.cbEnterprisePropertiesEditValueChanged(Sender: TObject);
begin
  LoadStore;
  LoadComputerID;
end;

procedure TFUploadDlg.cbStorePropertiesEditValueChanged(Sender: TObject);
begin
  LoadComputerID;
end;

function TFUploadDlg.CalculateDownloadFile (App, Version: string): string;
var
  Root, AppDir: string;
begin
  AppDir := GetAppDirectory(App);
  Root   := FMainAdmin.edRootDirectory.Text;
  if Root.EndsWith ('\') then
    Root := Root.Substring(1, Root.Length - 1);

  result := Format ('%s\%s\%s\%s', [Root, AppDir, Version, App])
end;

procedure TFUploadDlg.ExtractWorkspaceRepoSlug (RepoURL: string; var Workspace, RepoSlug: string);
var
  A: TArray <string>;
begin
  Workspace := '';
  RepoSlug  := '';

  if RepoURL.IsEmpty then exit;

  if RepoURL.EndsWith ('/') then
    Delete (RepoURL, Length (RepoURL), 1);

  A := RepoURL.Split(['/']);
  if Length (A) > 2 then
  begin
    WorkSpace := A [Length (A) - 2];
    RepoSlug  := A [Length (A) - 1];
  end;
end;


procedure TFUploadDlg.cbVersionInfoPropertiesChange(Sender: TObject);
var
  Valid: boolean;
  Maj, Min, Rel: integer;
begin
  SetDataSource (nil);
  if (cbApplications.Text <> '') and (cbVersionInfo.EditText <> '') then
  begin
    ExtractVersionSegments(cbVersionInfo.EditText, Maj, Min, Rel, Valid);

    if Valid then
    begin
      tabUpdates.FindKey([cbApplications.Text, Maj, Min, Rel]);
      SetDataSource (dsUpdates);

      edUploadFileName.Text := CalculateDownloadFile(cbApplications.Text, cbVersionInfo.Text);
      edCommitMessage.Text  := '';
    end;
  end;
end;

procedure TFUploadDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if tabUpdates.State in [dsInsert, dsEdit] then
    tabUpdates.Cancel;
end;

procedure TFUploadDlg.FormCreate(Sender: TObject);
begin
  FForcedVersion := '';
end;

procedure TFUploadDlg.FormShow(Sender: TObject);
begin
  btnOK.Enabled         := False;
  edUploadFileName.Text := '';
  cbApplicationsPropertiesChange (Self);
  LoadEnterprise;
  LoadComputerID;
end;

procedure TFUploadDlg.tabUpdatesCalcFields(DataSet: TDataSet);
begin
  DataSet.FieldByName('Version').AsString :=
    DataSet.FieldByName('Lastest_Ver_Maj').AsString + '.' +
    DataSet.FieldByName('Lastest_Ver_Min').AsString + '.' +
    DataSet.FieldByName('Lastest_Ver_Rel').AsString;
end;

procedure TFUploadDlg.ForceVersion (Version: string);
begin
  cbVersionInfo.EditValue := Version;
  cbVersionInfoPropertiesChange(cbVersionInfo);
end;

procedure TFUploadDlg.LoadEnterprise;
var
  Qry: TFDQuery;
begin
   cbEnterprise.Properties.Items.Clear;
   Qry := FMainAdmin.NewQuery;
  try
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('Select Distinct Upper (Enterprise) Enterprise');
    Qry.SQL.Add('  From Status_Machine_Updates');
//    Qry.SQL.Add(' Where Not Enterprise Like ''%.%.%.%''');
    Qry.Open;

    while not Qry.Eof do
    begin
      cbEnterprise.Properties.Items.Add(Qry.FieldByName('Enterprise').AsString);

      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFUploadDlg.LoadStore;
var
  Qry: TFDQuery;
begin
  cbStore.Properties.Items.Clear;
  if VarIsNull (cbEnterprise.EditValue) or VarIsEmpty (cbEnterprise.EditValue) then exit;

  Qry := FMainAdmin.NewQuery;
  try
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('Select Distinct Upper (Store) Store');
    Qry.SQL.Add('  From Status_Machine_Updates');
    Qry.SQL.Add(' Where Upper (Enterprise) = Upper (:Enterprise)');
    Qry.ParamByName('Enterprise').AsString := cbEnterprise.EditValue;
    Qry.Open;

    while not Qry.Eof do
    begin
      cbStore.Properties.Items.Add(Qry.FieldByName('Store').AsString);

      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFUploadDlg.LoadComputerID;
var
  Qry: TFDQuery;
begin
  cbComputerID.Properties.Items.Clear;
  Qry := FMainAdmin.NewQuery;
  try
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('Select Distinct Upper (Computer_ID) Computer_ID');
    Qry.SQL.Add('  From Status_Machine_Updates');
    Qry.SQL.Add(' Where Computer_ID Is Not Null');

    if not VarIsNull (cbEnterprise.EditValue) and not VarIsEmpty (cbEnterprise.EditValue) then
    begin
      Qry.SQL.Add('   And Upper (Enterprise) = Upper (:Enterprise)');
      Qry.ParamByName('Enterprise').AsString := cbEnterprise.EditValue;

      if not VarIsNull (cbStore.EditValue) and not VarIsEmpty (cbStore.EditValue) then
      begin
        Qry.SQL.Add('   And Upper (Store) = Upper (:Store)');
        Qry.ParamByName('Store').AsString := cbStore.EditValue;
      end;
    end;

    Qry.Open;
    while not Qry.Eof do
    begin
      cbComputerID.Properties.Items.Add(Qry.FieldByName('Computer_ID').AsString);

      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
end;

end.
